﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class AmbientAudioAdjuster : MonoBehaviour {

	protected AudioSource _audioSource;

	[SerializeField]
	// The points that decide the attenuation
	private Transform _zonePointsParent;
	private Vector3[] _zonePoints;

	// Attenuated volume is at max here
	[SerializeField]
	private float _minDistance;

	// Attenuated volume is at min here
	[SerializeField]
	private float _maxDistance;

	// Volume at min distance
	[SerializeField]
	private float _maxVolume = 1.0f;

	// Volume at max distance
	[SerializeField]
	private float _minVolume = 0.0f;


	private Transform _player;

	public void Start() {
		_audioSource = GetComponent<AudioSource>();
		_audioSource.spatialize = true;
		_audioSource.spatialBlend = 0.25f;

		_zonePoints = new Vector3[_zonePointsParent.childCount];
		for (int i = 0; i < _zonePoints.Length; i++) {
			_zonePoints[i] = _zonePointsParent.GetChild(i).position;
		}

		_player = transform.parent;
		transform.parent = null;
		transform.position = _player.position;
	}


	public void Update() {
		float smallestDist = (_player.position - _zonePoints[0]).magnitude;
		int nearest = 0;
		int secondnearest = 0;
		for (int i = 1; i < _zonePoints.Length; i++) {
			float dist = (_player.position - _zonePoints[i]).magnitude;
			//Debug.Log(_player.position + ", " + _zonePoints[i]);
			if (dist < smallestDist) {
				smallestDist = dist;
				secondnearest = nearest;
				nearest = i;
			}
		}

		transform.position = (_zonePoints[nearest] + _zonePoints[secondnearest]) / 2f;

		// If we're beyond the max distance, volume = min, if we're within the min distance, volume = max
		if (smallestDist > _maxDistance || smallestDist < _minDistance) {
			_audioSource.volume = smallestDist > _maxDistance ? _minVolume : _maxVolume;
			//Debug.Log(transform.position + ", " + _audioSource.volume);
		} else {
			// If we're within the min and max, simply remap our distance to our volume
			_audioSource.volume = smallestDist.Remap(_minDistance, _maxDistance, _maxVolume, _minVolume);
		}

		//Debug.Log("New Vol: " + _audioSource.volume);
	}
}
