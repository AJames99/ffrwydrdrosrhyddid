﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitcher : MonoBehaviour {
	[SerializeField]
	private Material _moonSkyBox;
	private Material _moonSkyBoxCopy;

	[SerializeField]
	private Light _moonLight;

	[SerializeField]
	private ReflectionProbe reflectionProbe;

	private float ambIntensity_default = 0.0f;
	private float moonBrightness_default = 0.0f;
	private float skyboxExposure_default = 0.0f;

	private float reflProbeInt_default = 0.0f;

	private int dir = 0;

	// Start is called before the first frame update
	void Awake() {
		ambIntensity_default = RenderSettings.ambientIntensity;
		moonBrightness_default = _moonLight.intensity;
		skyboxExposure_default = _moonSkyBox.GetFloat("_Exposure");
		_moonSkyBoxCopy = new Material(_moonSkyBox);
		RenderSettings.skybox = _moonSkyBoxCopy;

		reflProbeInt_default = reflectionProbe.intensity;
		/*_moonSkyBoxCopy.SetFloat("_Exposure", skyboxExposure_default);
		_moonLight.intensity = moonBrightness_default);
		RenderSettings.ambientIntensity = ambIntensity_default;*/
	}

	float timePassed = 0.0f;
	const float lerpMaxTime = 15.0f;
	float timePassed_lerp = lerpMaxTime;
	bool switched;
	private void Update() {
		if (timePassed < 5.0f) {
			timePassed += Time.deltaTime;
		} else if (!switched) {
			SwitchLights(false); // ENABLE!
			switched = true;
		}

		if (timePassed_lerp < lerpMaxTime) {
			timePassed_lerp += Time.deltaTime;
			Lerp(dir, timePassed_lerp / lerpMaxTime);
		}
	}


	// +1/-1
	void Lerp(int dir, float lerp) {

		float fac = (dir == -1 ? 1 - lerp : lerp) + 0.15f;

		_moonSkyBoxCopy.SetFloat("_Exposure", skyboxExposure_default * fac);
		_moonLight.intensity = moonBrightness_default * fac;
		RenderSettings.ambientIntensity = ambIntensity_default * fac;
		//RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;

		reflectionProbe.intensity = reflProbeInt_default * fac;
		reflectionProbe.RenderProbe();
		//Debug.Log("Lerp: " + lerp + ", dir: " + dir);
	}
	void SwitchLights(bool lights) {
		if (lights) {
			dir = 1;
			timePassed_lerp = 0.0f;
		} else {
			dir = -1;
			timePassed_lerp = 0.0f;
		}
	}
}
