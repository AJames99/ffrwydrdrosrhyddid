﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;



public interface PlayerEquippableItem {
	void Equip(FpsController.PlayerSocket socket);
	void Unequip(FpsController.PlayerSocket socket);
}

/// <summary>
/// Q3-based first person controller
/// </summary>
public class FpsController : MonoBehaviour, IGrabber {
	#region Drag Drop
	[SerializeField]
	private Transform _camTransform;
	private Transform _mainCamera;

	// Collision resolving is done with respect to this volume
	[SerializeField]
	private CapsuleCollider _collisionVolume;

	// Collision will not happen with these layers
	// One of them has to be this controller's own layer
	[SerializeField]
	private LayerMask _excludedLayers;

	/*[SerializeField]
	private Footsteps _footsteps;*/

	/*[SerializeField]
	private GrapplingHook _hook;*/

	[SerializeField]
	private bool _debugInfo;

	[SerializeField]
	private List<Transform> _groundedRayPositions;

	[SerializeField]
	private Animator _legsAnimator;
	private RuntimeAnimatorController _defaultLegsAnimator;

	[SerializeField]
	private double _slopePenetrationIgnoreLimit = 0.015;

	[SerializeField]
	private bool _allowDownwardSlide = true;

	[SerializeField]
	private Transform _spawnPoint;

	[SerializeField]
	private bool _allowBhopGroundAcceleration = true; /// <summary>
													  /// When we hold space to bunnyhop, should we get 1 frame's worth of ground acceleration? (Will cause sharp direction changes if holding A/D  upon bouncing)
													  /// </summary>
													  /// 
	[SerializeField]
	private bool _allowCrouch = true;

	[SerializeField]
	private float _crouchSlowFactor = 0.35f;

	[SerializeField]
	private bool _allowSlideOnCrouch = true;

	[SerializeField]
	private bool _allowAcceleratingSlide = false;

	[SerializeField]
	private bool _allowAirCrouching = false;

	[SerializeField]
	private BounceEffectScript _feetEffects = null;


	/// <summary>
	/// If true, holding space will cause a continual jumping. If false, the player must re-press space after each jump, like in CPMA.
	/// </summary>
	[SerializeField]
	private bool bAllowQPLStyleJump = true;

	[SerializeField]
	private Boots _equippedBoots;

	[SerializeField]
	private GameObject[] _weapons;
	private GameObject _activeWeapon;

	private bool _movementLocked = false;


	[Header("FoV Settings")]
	[SerializeField]
	private bool _enableDynamicFoV = false;
	[SerializeField]
	[Range(25.0f, 120.0f)]
	private float _minFov = 75f;
	[SerializeField]
	[Range(25.0f, 120.0f)]
	private float _maxFov = 90f;
	[SerializeField]
	[Range(1.0f, 100.0f)]
	private float _fovChangeRate = 15.0f;
	[SerializeField]
	[Range(2.0f, 50.0f)]
	private float _minSpeed_Fov = 5.0f;
	[SerializeField]
	[Range(2.0f, 50.0f)]
	private float _maxSpeed_Fov = 20.0f;

	[SerializeField]
	private Boots[] _bootsList;
	private int bootsIter = 0;

	#endregion

	#region Movement Parameters

	// The controller can collide with colliders within this radius
	private const float Radius = 2f;

	// Ad-hoc approach to make the controller accelerate faster
	//private const float GroundAccelerationCoeff = 500.0f;
	//private const float GroundAccelerationCoeff = 150.0f; // TF2 version
	// WAS 150, now 250
	private const float GroundAccelerationCoeff = 500.0f; // TF2 version

	// How fast the controller accelerates while it's not grounded
	//private const float AirAccelCoeff = 1f;
	//private const float AirAccelCoeff = 3f; // TF2 Version

	// Air deceleration occurs when the player gives an input that's not aligned with the current velocity
	// I think TF2's air decel is the same as accel
	//private const float AirDecelCoeff = 1.5f;	

	private const float SlideAccelCoeff = 8f;
	private const float SlideDecelCoeff = 18f;

	// Along a dimension, we can't go faster than this
	// This dimension is relative to the controller, not global
	// Meaning that "max speend along X" means "max speed along 'right side' of the controller"
	//private float MaxSpeedAlongOneDimension = 8f;
	//private const float MaxSpeedDefault = 8f;
	private const float MaxSpeedDefault = 10f; // Upped to 10 from 8

	// How fast the controller decelerates on the grounded
	// Was previously 9, decreased to 3f
	private const float Friction = 4.5f; // TF2 version
	private const float SlidingFriction = 0.15f; // TF2 version

	// Stop if under this speed
	private const float FrictionSpeedThresholdStop = 0.5f;

	// Use extra friction multiplier if under this speed
	private const float FrictionSpeedThresholdRoughen = 3f;

	// Push force given when jumping
	// - Used also to determine whether or not to ground us when sliding across a surface.
	//private const float JumpStrength = 8f;
	private const float JumpStrength = 10f;

	// yeah...
	private const float GravityAmount = 24f;


	// How precise the controller can change direction while not grounded 
	//private const float AirControlPrecision = 32f; // 16

	// Should we increase air control/direction change when moving forward only?
	//private const bool bForwardAirControlAddition = false; // true

	// When moving only forward, increase air control dramatically
	private const float AirControlAdditionForward = 2f; //8

	// Sliding acceleration (if allowed) is greater when doing W + A/D, and max when only A/D
	private const float SlideControlAdditionForward = 2f;

	// Prevents the projection of velocity onto the ground normal plane that stops us bouncing down slopes etc. Used to allow the player to be launched vertically when grounded.
	private bool beingLaunched = false;

	private bool isCrouched = false;
	private bool isSliding = false;
	private bool isRamping = false; // Causes sliding state when going up ramps
	private float defaultHeight = 2.0f;
	private float defaultCentre = 0.0f;
	private float minimumSlideSpeed = 6.0f;

	// Is the player inputting a slide/crouch?
	private bool wantsToCrouch = false;

	private Boots activeBoots;

	private RollerskatesBoots _skatesScript = null;
	#endregion

	#region Fields
	private AnimCallback ArmsAnimCallback;

	// Caching this always a good practice
	// TODO: Not anymore, as Unity caches it for us.
	private Transform _transform;

	// The real velocity of this controller
	private Vector3 _velocity;
	public Vector3 GetVelocity() { return _velocity; }

	// Raw input taken with GetAxisRaw()
	private Vector3 _moveInput;

	// Vertical look
	private float _pitch = 0; // We keep track of this stat since we want to clamp it
	private const float Sensitivity = 150;

	private readonly Collider[] _overlappingColliders = new Collider[25]; // More may be needed in future?
	private Transform _ghostJumpRayPosition; // Removed for now - ruins downhill sliding
											 // Old stat was (0, -0.5, -0.75)

	// Some information to persist
	private bool _isGroundedInPrevFrame;
	private bool _isGonnaJump;
	private Vector3 _wishDirDebug;

	private Vector3 _defaultCamPos;

	private PlayerSocket playerSocket; //Keeps track of stats and attributes that affect things in here.

	//private const float _doubleJumpTimeMax = 0.4f;//0.55f; // If a 2nd jump occurs within Xseconds of the first, give an upward boost (no conversion)
	private const float _rampJumpTimeMax = 0.3f; // If a 2nd jump occurs within X seconds, convert some horizontal to vertical speed
	private const float _doubleJumpDownTimeMin = 1.0f;
	private float _timeSinceLastJump = 0.0f;

	// Was the last jump a ramp/double jump? If so, don't supply the down-jump boost.
	private bool _lastjumpwasramp = false;

	//private Vector3 cameraRotOffset = new Vector3(0.0f, 180.0f, 0.0f);

	// Grabbing objects:
	private GrabbableObject _nickedObject;
	private bool _hasObject;
	// How long must <grab> be held to grab an object (otherwise, "use"/interact)
	private const float MaxGrabTimer = 0.6f;
	private float _grabTimer = 0.0f;
	private bool _lastPressWasGrab = false;
	//...

	private Camera camera;

	private const float _MaxRoll = 6f;
	private const float _camRollLerpRate = 3.0f;
	private float _currentRot = 0.0f;

	/*private bool isGrounded;
	Vector3 groundNormal;*/

	#endregion

	public class PlayerSocket : ItemSocket {

		FpsController fpsController;

		private PlayerEquippableItem itemEquipped = null;

		public PlayerSocket(FpsController fpsController) {
			this.fpsController = fpsController;
		}

		public void InstallItem(PlayerEquippableItem item) {
			if (item is Boots) {
				Boots boots = (Boots)item;

				if (itemEquipped != null) {
					UninstallItem();
				}

				foreach (Boots bootObj in fpsController._bootsList) {
					bootObj.gameObject.SetActive(false);
				}
				boots.gameObject.SetActive(true);
			}

			itemEquipped = item;
			item.Equip(this);

			fpsController._feetEffects.SetBounceEnabled(BounceParticle.resultantvalue);
			fpsController._feetEffects.enableQuakeVoice = QuakeVoice.resultantvalue;
			fpsController.isRamping = CanSlide.resultantvalue;
			fpsController.isSliding = CanSlide.resultantvalue;

			if (UseSkateAnims.resultantvalue) {
				fpsController._legsAnimator.runtimeAnimatorController = ((RollerskatesBoots)item).skatesAnimatorController;
				fpsController._skatesScript = ((RollerskatesBoots)item);
				Debug.Log("Using skates anims: " + ((RollerskatesBoots)item).skatesAnimatorController.name);
			}/* else {
				fpsController._legsAnimator.runtimeAnimatorController = fpsController._defaultLegsAnimator;
				fpsController._skatesScript = null;
			}*/
		}

		public void UninstallItem() {
			itemEquipped.Unequip(this);
			fpsController._feetEffects.SetBounceEnabled(BounceParticle.resultantvalue);
			fpsController._feetEffects.enableQuakeVoice = QuakeVoice.resultantvalue;
			itemEquipped = null;

			if (!UseSkateAnims.resultantvalue) {
				fpsController._legsAnimator.runtimeAnimatorController = fpsController._defaultLegsAnimator;
				fpsController._skatesScript = null;
			}
		}

		public SocketStatBool CanCrouch = new SocketStatBool(0, stackTypes.additive);

		//TF2 version 3f, CPMA version 6f (+3)
		// Was previously 3, now 7 by default
		public SocketStatFloat AirDecelCoeff = new SocketStatFloat(16, stackTypes.additive);
		// 
		public SocketStatFloat AirAccelCoeff = new SocketStatFloat(3, stackTypes.additive);

		// Should we increase air control massively when only inputting W?
		public SocketStatBool ForwardAirControlAddition = new SocketStatBool(1, stackTypes.additive);

		public SocketStatFloat AirControlPrecision = new SocketStatFloat(16f, stackTypes.additive);

		//public SocketStatBool BounceParticle = new SocketStatBool(1, stackTypes.additive);
		public SocketStatBool BounceParticle = new SocketStatBool(0, stackTypes.additive);

		public SocketStatBool QuakeVoice = new SocketStatBool(0, stackTypes.additive);

		public SocketStatFloat GroundAccelCoeff = new SocketStatFloat(0f, stackTypes.additive);

		public SocketStatFloat FrictionCoeff = new SocketStatFloat(0f, stackTypes.additive);

		public SocketStatFloat MaxSpeed = new SocketStatFloat(0f, stackTypes.additive);

		public SocketStatBool CanQuakeRampBounce = new SocketStatBool(0, stackTypes.additive);

		//public SocketStatBool CanSlide = new SocketStatBool(1, stackTypes.additive);
		public SocketStatBool CanSlide = new SocketStatBool(0, stackTypes.additive);

		public SocketStatBool UseVanillaQuakeAccel = new SocketStatBool(0, stackTypes.additive);

		public SocketStatFloat DecelerationMultiplier = new SocketStatFloat(1f, stackTypes.additive);

		public SocketStatBool GravityOnGround = new SocketStatBool(0, stackTypes.additive);

		public SocketStatBool UseSkateAnims = new SocketStatBool(0, stackTypes.additive);

		//public SocketStatFloat FrictionCoeffWhileSlow = new SocketStatFloat(1f, stackTypes.multiplicative);
		public SocketStatFloat FrictionCoeffWhileSlow = new SocketStatFloat(2f, stackTypes.multiplicative);
	}

	private void Start() {
		transform.position = _spawnPoint.position;
		//Application.targetFrameRate = 60; // My laptop is shitty and burn itself to death if not for this
		_transform = transform;
		_ghostJumpRayPosition = _groundedRayPositions.Last();
		defaultHeight = _collisionVolume.height;
		defaultCentre = _collisionVolume.center.y;
		_defaultCamPos = _camTransform.localPosition;

		playerSocket = new PlayerSocket(this);

		// Add the serialised values to the modifier stack
		playerSocket.CanCrouch.BaseValue = _allowCrouch ? 1 : 0;
		playerSocket.GroundAccelCoeff.BaseValue = GroundAccelerationCoeff;
		playerSocket.FrictionCoeff.BaseValue = Friction;
		playerSocket.MaxSpeed.BaseValue = MaxSpeedDefault;

		_defaultLegsAnimator = _legsAnimator.runtimeAnimatorController;

		_bootsList[0].gameObject.SetActive(true);

		//SwapWeapon(0);
	}

	private void Awake() {
		ArmsAnimCallback = this.gameObject.GetComponentInChildren<AnimCallback>();
		//camera = _camTransform.GetComponent<Camera>();
		camera = this.GetComponentInChildren<Camera>(false);
	}

	// Only for debug drawing
	private void OnGUI() {
		if (!_debugInfo) {
			return;
		}

		// Print current horizontal speed
		var ups = _velocity;
		ups.y = 0;
		GUI.Box(new Rect(Screen.width / 2f - 50, Screen.height / 2f + 50, 100, 40),
			(Mathf.Round(ups.magnitude * 100) / 100).ToString());

		// Draw horizontal speed as a line
		var mid = new Vector2(Screen.width / 2, Screen.height / 2); // Should remain integer division, otherwise GUI drawing gets screwed up
		var v = _camTransform.InverseTransformDirectionHorizontal(_velocity) * _velocity.WithY(0).magnitude * 10f;
		if (v.WithY(0).magnitude > 0.0001) {
			Drawing.DrawLine(mid, mid + Vector2.up * -v.z + Vector2.right * v.x, Color.red, 3f);
		}

		// Draw input direction
		var w = _camTransform.InverseTransformDirectionHorizontal(_wishDirDebug) * 100;
		if (w.magnitude > 0.001) {
			Drawing.DrawLine(mid, mid + Vector2.up * -w.z + Vector2.right * w.x, Color.blue, 2f);
		}
	}

	private void LerpCameraRoll() {
		float targetRot = Vector3.Dot(Vector3.right, _moveInput) * _MaxRoll;

		float diff = targetRot - _currentRot;
		// If the difference isn't negligible...
		if (Mathf.Abs(diff) >= 0.01f) {
			// If the difference is smaller than the rate of change, just set it to the target
			if (Mathf.Abs(diff) <= _camRollLerpRate * Time.deltaTime) {
				_currentRot = targetRot;
			} else {
				_currentRot += _camRollLerpRate * Time.deltaTime * diff;
			}
		}
	}

	private void Update() {
		Cursor.lockState = CursorLockMode.Locked; // Keep doing this. We don't want cursor anywhere just yet

		// Reset player - for testing
		if (Input.GetKeyDown(KeyCode.L)) {
			Gravity.Set(Vector3.down);
			_transform.position = _spawnPoint.position;
			_velocity = Vector3.forward;
			//_hook.ResetHook();
		}



		var dt = Time.deltaTime;

		// We use GetAxisRaw, since we need it to feel as responsive as possible
		_moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;

		// Don't allow jumps while ramping, only true player-input sliding
		if (!_movementLocked) {
			if (Input.GetKeyDown(KeyCode.Space) && !_isGonnaJump) {
				isCrouched = false;
				SetCrouch(false);
				_isGonnaJump = true;
			} else if (Input.GetKeyUp(KeyCode.Space)) {
				_isGonnaJump = false;
			}
		}

		if (Input.GetKeyDown(KeyCode.B)) {
			bootsIter++;
			if (bootsIter > (_bootsList.Length - 1)) {
				bootsIter = 0;
			}

			switch (bootsIter) {
				case 0:
					playerSocket.InstallItem(_bootsList[0].GetComponent<Boots>());
					Debug.Log("Equipped boots");
					break;
				case 1:
					playerSocket.InstallItem(_bootsList[1].GetComponent<RollerskatesBoots>());
					Debug.Log("Equipped rollers");
					break;
				case 2:
					playerSocket.InstallItem(_bootsList[2].GetComponent<QuakeBoots>());
					Debug.Log("Equipped quakers");
					break;
				default:
					Debug.LogError("Error: Index of Boots-" + bootsIter);
					break;
			}
			//Boots quakeBoots = new QuakeBoots();
			//Boots Speedrunners = new SpeedrunnersBoots();
			//playerSocket.InstallItem(_bootsList[1].GetComponent<RollerskatesBoots>());
		}

		/*if (Input.GetKeyDown(KeyCode.Alpha1)) {
			SwapWeapon(0);
		} else if (Input.GetKeyDown(KeyCode.Alpha2)) {
			SwapWeapon(1);
		}*/


		// Mouse look
		_pitch += Input.GetAxis("Mouse Y") * -Sensitivity * dt;
		_pitch = Mathf.Clamp(_pitch, -60, 80);
		if (!_movementLocked) {
			_camTransform.localRotation = Quaternion.Euler(Vector3.right * _pitch);
			_transform.rotation *= Quaternion.Euler(Input.GetAxis("Mouse X") * Sensitivity * dt * Vector3.up);
			LerpCameraRoll();

			_camTransform.Rotate(_camTransform.forward, -_currentRot, Space.World);
		}

		if (Input.GetKeyUp(KeyCode.G)) {

			// To stop us immediately interacting with stuff we've dropped
			if (!_lastPressWasGrab) {
				if (_hasObject) {
					ThrowObject();
				} else {
					Interact();
				}
				_grabTimer = 0.0f;
			} else {
				_lastPressWasGrab = false;
			}

		}
		if (Input.GetKey(KeyCode.G)) {
			_grabTimer += Time.deltaTime;
			if (_grabTimer > MaxGrabTimer && !_lastPressWasGrab) {
				if (_hasObject) {
					ThrowObject();
					_lastPressWasGrab = true;
					// TODO: Hand anim
				} else {
					TryGrabObject();
					_lastPressWasGrab = true;
					// TODO: Hand anim
				}
				_grabTimer = 0.0f;
			}
		}

		//_hook.Draw();

		// MOVEMENT

		// The player is attempting to go in this direction
		Vector3 wishDir = _movementLocked ? Vector3.zero : _camTransform.TransformDirectionHorizontal(_moveInput);
		_wishDirDebug = wishDir.ToHorizontal();

		Vector3 groundNormal;
		bool isGrounded = IsGrounded(out groundNormal);
		isGrounded = beingLaunched ? false : isGrounded;

		_feetEffects.ExternalUpdate(_isGonnaJump && !isRamping, isGrounded, isGrounded && !_isGroundedInPrevFrame, _velocity, isSliding && isGrounded, transform);

		if (Input.GetKeyDown(KeyCode.LeftControl) && !wantsToCrouch && playerSocket.CanCrouch.resultantvalue) {
			wantsToCrouch = true;
			isCrouched = isGrounded || _allowAirCrouching;
			_collisionVolume.height = defaultHeight / 2;
			_collisionVolume.center = new Vector3(_collisionVolume.center.x, defaultCentre - (defaultHeight / 4), _collisionVolume.center.z);
			SetCrouch(true);
		} else if (Input.GetKeyUp(KeyCode.LeftControl)) {
			wantsToCrouch = false;
			isCrouched = false;
			_collisionVolume.height = defaultHeight;
			_collisionVolume.center = new Vector3(_collisionVolume.center.x, defaultCentre, _collisionVolume.center.z);
			SetCrouch(false);
		}

		if (isGrounded && !_movementLocked) {

			isSliding = isRamping ? true : (wantsToCrouch && _velocity.magnitude > minimumSlideSpeed && _allowSlideOnCrouch);

			// Don't apply friction if just landed or about to jump (essentially, bhopping)
			if (_isGroundedInPrevFrame && !_isGonnaJump && !beingLaunched) {
				ApplyFriction(ref _velocity, dt, wishDir);
			}

			if (isSliding) {
				if (!isRamping) {
					var coeff = Vector3.Dot(_velocity, wishDir) > 0 ? SlideAccelCoeff : SlideDecelCoeff;
					Accelerate(ref _velocity, wishDir, coeff, dt);
					if (_allowAcceleratingSlide) {
						ApplySlideControl(ref _velocity, wishDir, dt);
					}
				}
				_velocity += Gravity.Down * (GravityAmount * dt);

			} else if (!_isGonnaJump || _allowBhopGroundAcceleration) {
				Accelerate(ref _velocity, wishDir, (isCrouched) ? playerSocket.GroundAccelCoeff.resultantvalue : playerSocket.GroundAccelCoeff.resultantvalue * _crouchSlowFactor, dt);
			}
			// Crop up horizontal velocity component - JULY9 - REMOVED THIS AS IT PREVENTS LAUNCHING
			if (!beingLaunched) {
				_velocity = Vector3.ProjectOnPlane(_velocity, groundNormal);
			}

			if (!_isGroundedInPrevFrame && !_isGonnaJump) {
				_lastjumpwasramp = false;
			}

			// JUMP
			if (_isGonnaJump && !isRamping && !_movementLocked) {


				// How strong is our jump? If we're jumping up a slope we can do a Quake Ramp Jump
				if (playerSocket.CanQuakeRampBounce.resultantvalue) {
					// Ramp jump converts some horizontal to vertical speed
					if (_timeSinceLastJump <= _rampJumpTimeMax) {
						_velocity = new Vector3(_velocity.x * 0.8f, _velocity.y, _velocity.z * 0.8f);
						_velocity += -Gravity.Down * JumpStrength * 1.15f;
						/*// Double jump gives a vertical boost relative to horizontal if within 400ms
						if (_timeSinceLastJump <= _doubleJumpTimeMax) {
							_velocity += (-Gravity.Down * JumpStrength * (_velocity.ToHorizontal().magnitude / 60f));
						}*/

						_lastjumpwasramp = true;

					} else if (_timeSinceLastJump >= _doubleJumpDownTimeMin && !_lastjumpwasramp) {
						// If we're jumping down a slope (longer time between jumps), we boost horizontally in our move dir
						// Increase horizontal speed

						// Clamp downward velocity to 0, otherwise slopes get buggy :(
						_velocity = _velocity.WithY(Mathf.Max(_velocity.y, 0f));
						_velocity += -Gravity.Down * JumpStrength;
						_velocity += Vector3.ProjectOnPlane(_velocity, groundNormal).normalized * 3.0f;

						_lastjumpwasramp = false;
					} else {
						_velocity += -Gravity.Down * JumpStrength;
						_lastjumpwasramp = false;
					}
				} else {
					_velocity += -Gravity.Down * JumpStrength;
				}

				// NEW add velocity that goes down the ramp like in Quake
				// Is 0 if the ground normal is just flat, increases to a max of 2.5 ups at 90° (which is impossible to hit really)
				_velocity += groundNormal * 2.5f * (1 - Mathf.Abs(Vector3.Dot(groundNormal, Vector3.up)));
				//Debug.Log(groundNormal + ", " + Vector3.up + ": " + Vector3.Dot(groundNormal, Vector3.up)/* * 2.5f * Mathf.Abs(Vector3.Dot(groundNormal, Vector3.up))*/);
				Debug.Log(groundNormal * 2.5f * (1 - Mathf.Abs(Vector3.Dot(groundNormal, Vector3.up))));

				//_velocity += -Gravity.Down * ((playerSocket.CanQuakeRampBounce.resultantvalue && _timeSinceLastJump <= _doubleJumpTimeMax) ? JumpStrength * 2 : JumpStrength);
				ArmsAnimCallback.Jump();
				_legsAnimator.SetTrigger("Jump");
				//wantsToCrouch = false;


				if (_timeSinceLastJump > 0.05f) {
					_timeSinceLastJump = 0.0f;
				}

				//Q3CPMA style hold space until jump input
				//QPL style is hold space to jump endlessly
				if (!bAllowQPLStyleJump) {
					_isGonnaJump = false;
				}
			}

			// If we're allowed to apply gravity while grounded
			// Only really applies if we're on a slope, and only pushes us down the slope, otherwise we jiggle through the floor
			if (playerSocket.GravityOnGround.resultantvalue) {
				_velocity += ((Gravity.Down + groundNormal) * (GravityAmount * dt));
			}

		} else {
			if (_timeSinceLastJump <= 10f) {
				_timeSinceLastJump += dt;
			}
			if (isSliding) {
				isSliding = false;
			}
			if (!(wantsToCrouch && _allowAirCrouching)) {
				isCrouched = false;
			}
			// If the input doesn't have the same facing with the current velocity
			// then slow down instead of speeding up (and use appropriate coefficient)
			var coeff = Vector3.Dot(_velocity, wishDir) > 0 ? playerSocket.AirAccelCoeff.resultantvalue : playerSocket.AirDecelCoeff.resultantvalue;

			Accelerate(ref _velocity, wishDir, coeff, dt);

			if (Mathf.Abs(_moveInput.z) > 0.0001) {  // Pure side velocity doesn't allow air control
				ApplyAirControl(ref _velocity, wishDir, dt);
			}

			_velocity += Gravity.Down * (GravityAmount * dt);
		}

		var displacement = _velocity * dt;

		// If we're moving too fast, make sure we don't hollow through any collider
		if (displacement.magnitude > _collisionVolume.radius) {
			ClampDisplacement(ref _velocity, ref displacement, _transform.position);
		}

		_transform.position += displacement;

		var collisionDisplacement = ResolveCollisions(ref _velocity);
		_transform.position += collisionDisplacement;


		if (beingLaunched) {
			beingLaunched = false;
		}
		ArmsAnimCallback.SetGrounded(isGrounded);
		//_handAnimators.SetBool("isGrounded", isGrounded);
		_legsAnimator.SetBool("isGrounded", isGrounded);

		ArmsAnimCallback.SetMoveSpeed(Mathf.Min(_velocity.ToHorizontal().magnitude, 14.0f), 14.0f);
		//_handAnimators.SetFloat("moveSpeed", Mathf.Min(_velocity.ToHorizontal().magnitude, 8.0f));
		_legsAnimator.SetFloat("moveSpeed", Mathf.Min(_velocity.ToHorizontal().magnitude, 14.0f));

		_legsAnimator.SetFloat("moveSpeed_X", Vector3.Dot(transform.right, _velocity.ToHorizontal().normalized));
		_legsAnimator.SetFloat("moveSpeed_Y", Vector3.Dot(transform.forward, _velocity.ToHorizontal().normalized));

		if (playerSocket.UseSkateAnims.resultantvalue) {
			_legsAnimator.SetFloat("moveSpeed", Mathf.Min(_velocity.ToHorizontal().magnitude, 25.0f));
			_legsAnimator.SetBool("isAccelerating", wishDir.magnitude > 0.05f && isGrounded);
			_skatesScript.SetSpeedAccel(Mathf.Min(_velocity.ToHorizontal().magnitude, 15f) / 10f, /*wishDir.magnitude > 0.05f,*/ isGrounded, _isGroundedInPrevFrame);
		}

		// NEW for skates
		if (playerSocket.UseSkateAnims.resultantvalue) {
			Vector3 velDir = _velocity.normalized;
			float skatesAngle = 0f;

			// TOP HALF
			if (Vector3.Dot(velDir, _transform.forward) > 0) {
				skatesAngle = Vector3.Dot(velDir, _transform.right);
			}
			// BOTTOM HALF
			else {
				skatesAngle = Vector3.Dot(velDir, -_transform.right);
			}

			skatesAngle = (skatesAngle + 1) / 2f;

			_legsAnimator.SetFloat("skatesAngle", skatesAngle);
		}


		ArmsAnimCallback.SetSliding(isSliding);
		//_handAnimators.SetBool("isSliding", isSliding);
		_legsAnimator.SetBool("isSliding", isSliding);

		if (_enableDynamicFoV) {
			float targetFoV;
			float speed = _velocity.ToHorizontal().magnitude;
			if (speed <= _minSpeed_Fov) {
				targetFoV = _minFov;
			} else if (speed >= _maxSpeed_Fov) {
				targetFoV = _maxFov;
			} else {
				targetFoV = speed.Remap(_minSpeed_Fov, _maxSpeed_Fov, _minFov, _maxFov);
			}

			float diff = targetFoV - camera.fieldOfView;
			// If the difference isn't negligible...
			if (Mathf.Abs(diff) >= 0.01f) {
				// If the difference is smaller than the rate of change, just set it to the target
				if (Mathf.Abs(diff) <= _fovChangeRate * Time.deltaTime) {
					camera.fieldOfView = targetFoV;
				} else {
					if (Mathf.Sign(diff) > 0) {
						camera.fieldOfView += _fovChangeRate * Time.deltaTime;
					} else {
						camera.fieldOfView -= _fovChangeRate * Time.deltaTime;
					}
				}
			}
		}

		_isGroundedInPrevFrame = isGrounded;

	}

	/*private void FixedUpdate() {
		var collisionDisplacement = ResolveCollisions(ref _velocity);
		_transform.position += collisionDisplacement;
	}*/

	/*private void FixedUpdate() {
		/*isGrounded = IsGrounded(out groundNormal);

		///
		if (isGrounded) {
			if (_isGonnaJump && !isRamping && !_movementLocked) {

				// How strong is our jump? If we're jumping up a slope we can do a Quake Ramp Jump
				if (playerSocket.CanQuakeRampBounce.resultantvalue && _timeSinceLastJump <= _doubleJumpTimeMax) {
					// Stunt horizontal velocity (i.e. convert to vertical)
					_velocity = new Vector3(_velocity.x, _velocity.y, _velocity.z) * 0.8f;
					_velocity += -Gravity.Down * JumpStrength * 2;
				}
				// If we're jumping down a slope (longer time between jumps), we boost horizontally in our move dir
				else if (playerSocket.CanQuakeRampBounce.resultantvalue && _timeSinceLastJump >= _doubleJumpTimeMax * 2) {
					// Increase horizontal speed
					_velocity += -Gravity.Down * JumpStrength;
					_velocity += Vector3.ProjectOnPlane(_velocity, groundNormal).normalized * 6.0f;
				} else {
					_velocity += -Gravity.Down * JumpStrength;
				}
				//_velocity += -Gravity.Down * ((playerSocket.CanQuakeRampBounce.resultantvalue && _timeSinceLastJump <= _doubleJumpTimeMax) ? JumpStrength * 2 : JumpStrength);
				ArmsAnimCallback.Jump();
				_legsAnimator.SetTrigger("Jump");
				//wantsToCrouch = false;

				if (playerSocket.CanQuakeRampBounce.resultantvalue) {
					_timeSinceLastJump = 0.0f;
				}
			}
		}

	var displacement = _velocity * Time.fixedDeltaTime;

		// If we're moving too fast, make sure we don't hollow through any collider
		if (displacement.magnitude > _collisionVolume.radius) {
			ClampDisplacement(ref _velocity, ref displacement, _transform.position);
}

_transform.position += displacement;

		var collisionDisplacement = ResolveCollisions(ref _velocity);

_transform.position += collisionDisplacement;
		//_isGroundedInPrevFrame = isGrounded;
	}*/

	//private bool useVanillaQuakeAccel = true;
	private void Accelerate(ref Vector3 playerVelocity, Vector3 accelDir, float accelCoeff, float dt) {
		// How much speed we already have in the direction we want to speed up

		float projSpeed = 0f;
		if (playerSocket.UseVanillaQuakeAccel.resultantvalue) {
			// Better allowance of deceleration
			/*projSpeed = Mathf.Min((Vector3.Dot(playerVelocity.normalized, accelDir.normalized) - 0.5f) * 2, 1);
			if (projSpeed < 0) {
				projSpeed = (-1 / projSpeed);
			//Braking ability //Added a +0.5f to hugely reduce braking
		}

		projSpeed *= playerVelocity.magnitude; */



			// NEW test
			projSpeed = 0f;
			float dot = Vector3.Dot(accelDir, playerVelocity.normalized);

			//Debug.Log("BEFORE: " + dot);

			// Map from 1, 0.5, 0 to 1, 0, 1
			if (dot > 0.5f) {
				dot = dot.Remap(0.5f, 1f, 0f, 1f);
			} else if (dot > 0) {
				dot = dot.Remap(0.5f, 0f, 0f, 1f);
			}/* else if (dot < 0) {
				dot = 0f; // Always allow backward acceleration
			}*/

			//Debug.Log("AFTER: " + dot);

			projSpeed = dot * playerVelocity.magnitude;


		} else {
			// CPMA Style - Acceleration in direction of vel = 0, at 90° = 1, at 180° (Braking) = 2
			projSpeed = Vector3.Dot(playerVelocity, accelDir);
		}


		// How much speed we need to add (in that direction) to reach max speed
		var addSpeed = playerSocket.MaxSpeed.resultantvalue - projSpeed;
		if (addSpeed <= 0) {
			return;
		}

		// How much we are gonna increase our speed
		// maxSpeed * dt => the real deal. a = v / t
		// accelCoeff => ad hoc approach to make it feel better
		var accelAmount = accelCoeff * playerSocket.MaxSpeed.resultantvalue * dt;

		// If we are accelerating more than in a way that we exceed maxSpeedInOneDimension, crop it to max
		if (accelAmount > addSpeed) {
			accelAmount = addSpeed;
		}

		// NEW! parameter in boots (should only affect rollerskates for now)
		if (Vector3.Dot(playerVelocity.normalized, accelDir.normalized) < 0) {
			accelAmount *= playerSocket.DecelerationMultiplier.resultantvalue;
			//Debug.Log(accelAmount + ", " + playerSocket.DecelerationMultiplier.resultantvalue);
		}/* else {
			Debug.Log("Accelerating: " + accelDir);
		}*/

		playerVelocity += accelDir * accelAmount;
	}

	private void ApplyFriction(ref Vector3 playerVelocity, float dt, Vector3 wishdir) {
		var speed = playerVelocity.magnitude;
		if (speed <= 0.00001) {
			return;
		}

		float dot = (wishdir.magnitude > 0.1f) ? Mathf.Max(0f, Vector3.Dot(wishdir, playerVelocity)) : 1f;

		var downLimit = Mathf.Max(speed, FrictionSpeedThresholdStop); // Don't drop below threshold.
																	  // If we're crouching and going beyond the minimum slide limit and we allow sliding, use sliding friction

		float fric = isSliding ? SlidingFriction : playerSocket.FrictionCoeff.resultantvalue;
		if (speed < FrictionSpeedThresholdRoughen) {
			//Debug.Log("FRIC WAS: " + fric + ", NOW" + fric * playerSocket.FrictionCoeffWhileSlow.resultantvalue);
			fric *= playerSocket.FrictionCoeffWhileSlow.resultantvalue;
		}

		var dropAmount = speed - (downLimit * fric * dt * dot);
		if (dropAmount < 0) {
			dropAmount = 0;
		}

		playerVelocity *= (dropAmount / speed); // Reduce the velocity by a certain percent
	}

	private void ApplyAirControl(ref Vector3 playerVelocity, Vector3 accelDir, float dt) {
		// This only happens in the horizontal plane
		// TODO: Verify that these work with various gravity values
		var playerDirHorz = playerVelocity.ToHorizontal().normalized;
		var playerSpeedHorz = playerVelocity.ToHorizontal().magnitude;

		var dot = Vector3.Dot(playerDirHorz, accelDir);
		if (dot > 0) {
			var k = playerSocket.AirControlPrecision.resultantvalue * dot * dot * dt;

			// Like CPMA, increased direction change rate when we only hold W
			// If we want pure forward movement, we have much more air control
			var isPureForward = Mathf.Abs(_moveInput.x) < 0.0001 && Mathf.Abs(_moveInput.z) > 0;
			if (isPureForward && playerSocket.ForwardAirControlAddition.resultantvalue) {
				k *= AirControlAdditionForward;
			}

			// A little bit closer to accelDir
			playerDirHorz = playerDirHorz * playerSpeedHorz + accelDir * k;
			playerDirHorz.Normalize();

			// Assign new direction, without touching the vertical speed
			playerVelocity = (playerDirHorz * playerSpeedHorz).ToHorizontal() + Gravity.Up * playerVelocity.VerticalComponent();
		}

	}

	private void ApplySlideControl(ref Vector3 playerVelocity, Vector3 accelDir, float dt) {
		// This only happens in the horizontal plane
		var playerDirHorz = playerVelocity.ToHorizontal().normalized;
		var playerSpeedHorz = playerVelocity.ToHorizontal().magnitude;

		var dot = Vector3.Dot(playerDirHorz, accelDir);
		if (dot > 0) {
			//16f was air control precision
			var k = 1f * dot * dot * dt;

			// (Mae'n fel Slash o Quake: Champions)
			// If we want pure forward movement, we have much more air control
			/*var isPureSideways = Mathf.Abs(_moveInput.z) < 0.0001f && Mathf.Abs(_moveInput.x) > 0f;
			if (isPureSideways) {
				k *= SlideControlAdditionForward;
			}*/

			// A little bit closer to accelDir
			playerDirHorz = playerDirHorz * playerSpeedHorz + accelDir * k;
			playerDirHorz.Normalize();

			// Assign new direction, without touching the vertical speed
			playerVelocity = (playerDirHorz * playerSpeedHorz).ToHorizontal() + Gravity.Up * playerVelocity.VerticalComponent();
		}

	}

	// Calculates the displacement required in order not to be in a world collider
	private Vector3 ResolveCollisions(ref Vector3 playerVelocity) {
		// Get nearby colliders
		Physics.OverlapSphereNonAlloc(_transform.position, Radius + 0.1f,
			_overlappingColliders, ~_excludedLayers, QueryTriggerInteraction.Ignore);

		var totalDisplacement = Vector3.zero;
		var checkedColliderIndices = new HashSet<int>();

		// If the player is intersecting with that environment collider, separate them
		for (var i = 0; i < _overlappingColliders.Length; i++) {
			// Two player colliders shouldn't resolve collision with the same environment collider
			if (checkedColliderIndices.Contains(i)) {
				continue;
			}

			var envColl = _overlappingColliders[i];

			// Skip empty slots
			if (envColl == null || envColl.isTrigger) {
				continue;
			}

			Vector3 collisionNormal;
			float collisionDistance;
			if (Physics.ComputePenetration(
				_collisionVolume, _collisionVolume.transform.position, _collisionVolume.transform.rotation,
				envColl, envColl.transform.position, envColl.transform.rotation,
				out collisionNormal, out collisionDistance)) {
				// Ignore very small penetrations
				// Required for standing still on slopes
				// ... still far from perfect though
				if (collisionDistance < _slopePenetrationIgnoreLimit) {
					continue;
				}

				checkedColliderIndices.Add(i);

				// Shift out of the collider
				totalDisplacement += collisionNormal * collisionDistance;

				// Clip down the velocity component which is in the direction of penetration
				playerVelocity -= Vector3.Project(playerVelocity, collisionNormal);
			}
		}

		// It's better to be in a clean state in the next resolve call
		for (var i = 0; i < _overlappingColliders.Length; i++) {
			_overlappingColliders[i] = null;
		}

		return totalDisplacement;
	}

	// If one of the rays hit, we're considered to be grounded
	private bool IsGrounded(out Vector3 groundNormal) {
		// If vertical speed (w.r.t. gravity direction) is greater than our jump velocity, don't ground us.
		// This allows for sliding up ramps when moving fast
		// If we allow downward slide, the player can slide when their vertical velocity is downward
		float verticalSpeed = (_allowDownwardSlide) ? Mathf.Abs(_velocity.VerticalComponent()) : _velocity.VerticalComponent();
		bool isGrounded = false;
		groundNormal = -Gravity.Down;

		if (verticalSpeed > JumpStrength + 0.05 && playerSocket.CanSlide.resultantvalue) {
			//isGrounded = false; //- now removed, when sliding up a ramp we'll be in the slide state from now on
			isRamping = true;
		} else {
			isRamping = false;
		}
		foreach (var t in _groundedRayPositions) {
			// The last one is reserved for ghost jumps
			// Don't check that one if already on the ground
			// Ghost jump is typically behind the player and below so that we can check if we've recently jumped etc.
			if (t == _ghostJumpRayPosition && isGrounded) {
				continue;
			}

			RaycastHit hit;
			if (Physics.Raycast(t.position, Gravity.Down, out hit, 0.51f, ~_excludedLayers, QueryTriggerInteraction.Ignore)) {
				groundNormal = hit.normal;
				isGrounded = true;
			}
		}
		//}

		return isGrounded;
	}

	// If there's something between the current position and the next, clamp displacement
	private void ClampDisplacement(ref Vector3 playerVelocity, ref Vector3 displacement, Vector3 playerPosition) {
		RaycastHit hit;
		if (Physics.Raycast(playerPosition, playerVelocity.normalized, out hit, displacement.magnitude, ~_excludedLayers)) {
			displacement = hit.point - playerPosition;
		}
	}

	// Handy when testing
	public void ResetAt(Transform t) {
		_transform.position = t.position + Vector3.up * 0.5f;
		//_camTransform.position = _defaultCamPos;
		_velocity = t.TransformDirection(Vector3.forward);
	}

	public void AddForce(Vector3 force, bool explosion = false) {
		_velocity += force;
		beingLaunched = true;
		if (explosion) {
			if (_feetEffects) {
				_feetEffects.ResetSmoke(true);
				_feetEffects.ResetSmoke(false);
			}
		}
	}


	private void SetCrouch(bool crouch) {
		/*if (crouch) {
			//_camTransform.localPosition = _defaultCamPos + new Vector3(0.0f, -0.5f, 0.0f); // REDUNDANT - camera transf node is now a child of leg torso bone
			//MaxSpeedAlongOneDimension = MaxSpeedDefault * _crouchSlowFactor;
			playerSocket.MaxSpeed.BaseValue = MaxSpeedDefault * _crouchSlowFactor;
		} else {
			//_camTransform.localPosition = _defaultCamPos + new Vector3(0.0f, 0.0f, 0.0f);
			playerSocket.MaxSpeed.BaseValue = MaxSpeedDefault;
		}*/

		playerSocket.MaxSpeed.BaseValue = MaxSpeedDefault * (crouch ? _crouchSlowFactor : 1f);

		_legsAnimator.SetBool("isCrouched", crouch);
	}

	/*private void SwapWeapon(int i) {
		foreach (GameObject weapon in _weapons) {
			weapon.SetActive(false);
		}
		if (0 <= i && i <= _weapons.Length) {
			_weapons[i].SetActive(true);
			_activeWeapon = _weapons[i];
		}
	}*/

	private void TryGrabObject() {
		Ray ray = new Ray(camera.transform.position, /*_camTransform*/camera.transform.forward);
		Debug.DrawRay(ray.origin, ray.direction * 5.0f, Color.yellow, 5.0f);

		RaycastHit[] hits = new RaycastHit[3];
		if (Physics.RaycastNonAlloc(ray, hits, 5.0f, ~_excludedLayers, QueryTriggerInteraction.Ignore) > 0) {
			foreach (RaycastHit hit in hits) {
				if (hit.transform == null) {
					continue;
				}

				GrabbableObject grabObj = hit.collider.gameObject.GetComponent<GrabbableObject>();
				if (grabObj) {
					_nickedObject = grabObj;
					//_nickedObject.SetActive(false);
					grabObj.Carrier = camera.transform;
					grabObj.FpsCont = this;
					grabObj.grabber = this;
					_hasObject = true;
					return;
				}
			}
		}
	}

	private void ThrowObject() {
		// Query forward to see if we can throw it (enough space)
		// If not, play a sound/voice clip "Does dim digon o le"
		if (_nickedObject != null) {
			/*_nickedObject.transform.position = _camTransform.position + (_camTransform.forward * 3.0f);
			_nickedObject.transform.LookAt(_camTransform.position + _camTransform.forward, Vector3.up);
			_nickedObject.SetActive(true);
			Rigidbody rb = _nickedObject.GetComponent<Rigidbody>();
			if (rb) {
				rb.velocity = Vector3.zero;
				rb.AddForce(_camTransform.forward * 25.0f, ForceMode.Impulse);
			}
			*/

			_nickedObject.Carrier = null;
			_nickedObject.Throw(_camTransform.forward, 10.0f);
			_nickedObject = null;
			_hasObject = false;
		}
	}

	private void Interact() {
		//Ray ray = new Ray(_camTransform.position, _camTransform.forward);
		Ray ray = new Ray(camera.transform.position, camera.transform.forward);
		RaycastHit[] hits = new RaycastHit[3];

		if (Physics.RaycastNonAlloc(ray, hits, 7.0f, ~_excludedLayers, QueryTriggerInteraction.Ignore) > 0) {
			foreach (RaycastHit hit in hits) {
				if (hit.transform == null) {
					continue;
				}
				//UsableObject obj = hit.collider.gameObject.GetComponent<UsableObject>();
				UsableObject obj = hit.collider.gameObject.GetComponent<UsableObject>();
				if (obj != null) {
					obj.Use(this);
					continue;
				}
			}
		}
	}

	public void GiveAmmo(FlareShellScript.FlareTypes type, int amount) {
		((FlareGunAnimCallback)ArmsAnimCallback).ModifyAmmo(amount, type);
	}

	public void PouncedOn(Vector3 sourcePos) {
		_movementLocked = true;
		((FlareGunAnimCallback)ArmsAnimCallback).PouncedOn();
		_legsAnimator.SetBool("Pouncing", true);

		Debug.Log("SourcePos: " + sourcePos + ", _camTransform pos: " + _camTransform.position + ", player transform pos:" + transform.position);

		//Rotate horizontally to look at da spider
		//_camTransform.LookAt(new Vector3(sourcePos.x, _camTransform.position.y, sourcePos.z), Vector3.up);
		transform.LookAt(new Vector3(sourcePos.x, transform.position.y, sourcePos.z), Vector3.up);
		_camTransform.LookAt(new Vector3(sourcePos.x, transform.position.y, sourcePos.z), Vector3.up);

		_velocity = Vector3.zero.WithY(_velocity.y);
		_isGonnaJump = false;
	}

	/*
	public void Mauled(Vector3 sourcePos) {
		_movementLocked = true;
		_legsAnimator.SetBool("Mauling", true);

		Debug.Log("SourcePos: " + sourcePos + ", _camTransform pos: " + _camTransform.position + ", player transform pos:" + transform.position);

		//Rotate horizontally to look at da spider
		//_camTransform.LookAt(new Vector3(sourcePos.x, _camTransform.position.y, sourcePos.z), Vector3.up);
		//transform.LookAt(new Vector3(sourcePos.x, transform.position.y, sourcePos.z), Vector3.up);
		//_camTransform.LookAt(new Vector3(sourcePos.x, transform.position.y, sourcePos.z), Vector3.up);

		// New
		transform.position = sourcePos.WithY(transform.position.y);

		_velocity = Vector3.zero.WithY(_velocity.y);
		_isGonnaJump = false;
	}
	*/
	public void Snap() {
		_nickedObject = null;
		_hasObject = false;
	}
}
