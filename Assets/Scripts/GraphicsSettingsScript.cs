﻿#define HZ240
// HZ30, HZ60, HZ144, HZ240
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphicsSettingsScript : MonoBehaviour {

#if HZ30
	readonly int FRAMERATE = 30;
#elif HZ60
	readonly int FRAMERATE = 60;
#elif HZ144
readonly int FRAMERATE = 144;
#elif HZ240
	readonly int FRAMERATE = 240;
#endif

	// Start is called before the first frame update
	void Start() {
		QualitySettings.vSyncCount = 0;
		Application.targetFrameRate = FRAMERATE;
		Debug.Log("HZ:" + Application.targetFrameRate + ", VSYNC: " + QualitySettings.vSyncCount);
	}
}
