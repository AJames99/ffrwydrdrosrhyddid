﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilScript : MonoBehaviour {

	[SerializeField]
	private Texture2D _smallSingle;

	[SerializeField]
	private Texture2D _medSingle;

	[SerializeField]
	private Texture2D _smallOneSide;

	[SerializeField]
	private Texture2D _medOneSide;

	[SerializeField]
	private Texture2D _largeOneSide;

	[SerializeField]
	private Texture2D _fullOneSide;

	[SerializeField]
	private Texture2D _fullOmni;

	// How large is this puddle
	private const uint NONE = 0;
	private const uint SMALL = 1;
	private const uint MEDIUM = 2;
	private const uint LARGE = 3;
	private const uint FULL = 4;

	private uint size = SMALL;

	// 0 = invisible, 0.25 = small, 0.5 = medium, 0.75 large, 1.0 = full
	float sizeProportion = 0.25f;

	// Keeps log of all the oil script quads in the world
	private static List<OilScript> oilTileList;

	// When checking directionst to expand to, how far off the ground can a tile be?
	// If the distance is too large, don't expand in this direction
	const float _maxAcceptableHeight = 0.1f;

	static Vector2[] directions = { new Vector2(1.0f, 0.0f), new Vector2(-1.0f, 0.0f), new Vector2(0.0f, 1.0f), new Vector2(0.0f, -1.0f) };

	const float scale = 1.0f;

	// Start is called before the first frame update
	void Start() {
		transform.localScale = new Vector3(scale, scale, scale);
	}

	// Update is called once per frame
	void FixedUpdate() {
		SetSizeProportion(sizeProportion += 0.25f * Time.deltaTime);

		if (size == FULL) {
			Vector2 dir = FindValidExpansion();

		}
	}

	Vector2 FindValidExpansion() {
		foreach (Vector2 dir in directions) {
			if (QueryExpandDirection(dir)) {
				return dir;
			}
		}
		return Vector2.zero;
	}

	bool QueryExpandDirection(Vector2 direction) {
		//transform + (direction * scale);
		//TOOD: Raycast down
		return false;
	}

	void SetSizeProportion(float sizeProp) {
		sizeProportion = sizeProp;
		// Update the size enum
		size = (uint)Mathf.FloorToInt(sizeProportion * 4);
	}

	void Expand(Vector2 direction) {

	}
}
