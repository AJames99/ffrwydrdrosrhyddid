﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareScript : MonoBehaviour {
	[SerializeField]
	private float _lifeTime; // Total lifetime in seconds - deletes self at end of this

	[SerializeField]
	private ParticleSystem _smokeSystem;
	[SerializeField]
	private float _smokeExpirationTime; // in Seconds


	[SerializeField]
	private ParticleSystem _flareSystem;
	[SerializeField]
	private float _flareExpirationTime; // in Seconds

	[SerializeField]
	private bool _shouldLightFloatInAir = false;

	[SerializeField]
	private Light _flareLight;
	[SerializeField]
	private float _lightExpirationTime; // in Seconds  - CANNOT BE MODIFIED WHILE RUNNIN!
	[SerializeField]
	private float _lightFlickerAmount = 0.05f; // As a proportion of total brightness i.e. probably stick to 0-1 range

	[SerializeField]
	private float _flareBurnCoefficient = 1.0f; // Affects how well the flare burns things

	[SerializeField]
	private float _impactDamage = 10.0f; // Affects the damage caused when impacting an enemy/object

	[SerializeField]
	private GameObject _lensFlareObject;
	private Material _lensFlare;


	private bool _hasLensFlare = false;
	private float defaultLensFlareBrightness = 1.0f;
	private float defaultLensFlareScaling = 1.0f;
	private float brightness = 0.0f;

	private const string _brightnessHandle = "FlareBrightness";
	private const string _scalingHandle = "FlareScaling";

	private float timeAlive = 0.0f;

	private float defaultBrightness;

	bool _flareDead = false, _smokeDead = false, _lightDead = false;

	private static Transform[] flaresInWorld = new Transform[10];

	// A function/graph f(x) is used to actuate the light intensity over time.
	// The resultant brightness is calculated by: f(t) + a, where "t" is total time alive and "a" is a random 0-1 value multiplied by _lightFlickerAmount

	// When each of the particle systems expires, their emission to be halted and their parents set to null (detaching them)
	// they should be set to destroy themselves when all particles are gone

	protected void Start() {
		DetectableSoundScript DSS = gameObject.AddComponent<DetectableSoundScript>();
		DSS.CreateDetectableMovingSound(0.45f, 0.5f);
	}

	void Awake() {
		defaultBrightness = _flareLight.intensity;
		T = _lightExpirationTime;
		if (_lensFlareObject) {
			_lensFlare = _lensFlareObject.GetComponent<MeshRenderer>().material;
			_hasLensFlare = true;
			defaultLensFlareBrightness = _lensFlare.GetFloat(_brightnessHandle);
			defaultLensFlareScaling = _lensFlare.GetFloat(_scalingHandle);
		}

		AddToFlaresInWorld(this.transform);

		if (_shouldLightFloatInAir) {
			Rigidbody rb = GetComponent<Rigidbody>();
			rb.freezeRotation = true;
			transform.rotation = Quaternion.identity;
			rb.rotation = Quaternion.identity;
			Vector3 offset = new Vector3(0.0f, 15.0f, 0.0f);
			_flareLight.transform.position += offset;
			_lensFlareObject.transform.position -= offset;
		}
	}

	public void SetFlareRotation(Quaternion quat) {
		if (!_shouldLightFloatInAir) {
			transform.rotation = quat;
		}
	}

	private static void AddToFlaresInWorld(Transform flare) {
		for (int i = 0; i < flaresInWorld.Length; i++) {
			if (flaresInWorld[i] == null) {
				flaresInWorld[i] = flare;
				//Debug.Log("Flare successfully added to static array");
				return;
			}
		}
	}

	private static void RemoveFromFlaresInWorld(Transform flare) {
		for (int i = 0; i < flaresInWorld.Length; i++) {
			if (flaresInWorld[i] == flare) {
				flaresInWorld[i] = null;
				//Debug.Log("Flare successfully removed from static array");
				return;
			}
		}
	}

	public static bool GetFlaresInWorld(out Transform[] flares) {
		int j = 0;
		for (int i = 0; i < flaresInWorld.Length; i++) {
			if (flaresInWorld[i] != null) {
				j++;
			}
		}
		flares = flaresInWorld;
		return j > 0;
	}

	// Update is called once per frame
	void Update() {
		timeAlive += Time.deltaTime;

		if (timeAlive > _lifeTime) {
			Destroy(this.gameObject);
			RemoveFromFlaresInWorld(this.transform);
			//Debug.Log("Object Destroyed!");
		}

		if (!_flareDead) {
			if (timeAlive > _flareExpirationTime) {
				_flareSystem.Stop(true, ParticleSystemStopBehavior.StopEmitting);
				_flareDead = true;
				//Debug.Log("Flare FX Destroyed!");
			}
		}

		if (!_smokeDead) {
			if (timeAlive > _smokeExpirationTime) {
				_smokeSystem.Stop(true, ParticleSystemStopBehavior.StopEmitting);
				_smokeDead = true;
				//Debug.Log("Smoke FX Destroyed!");
			}
		}

		if (!_lightDead) {
			if (timeAlive > _lightExpirationTime) {
				Destroy(_flareLight);
				_lightDead = true;
				//Debug.Log("Light Destroyed!");
			}

			brightness = flareBrightness(timeAlive, _lightFlickerAmount);
			_flareLight.intensity = defaultBrightness * brightness;
			if (_hasLensFlare) {
				_lensFlare.SetFloat(_brightnessHandle, defaultLensFlareBrightness * (brightness));
				_lensFlare.SetFloat(_scalingHandle, defaultLensFlareScaling * (brightness));
			}
		}
	}

	private void OnCollisionEnter(Collision collision) {
		Vector3 impact = collision.relativeVelocity;
		//if (impact.magnitude > 2.0f) {
		// Might need to do HasComponent? if this is breaking on nulls n stuff
		FireScript fs = collision.transform.GetComponent<FireScript>();
		if (fs) {
			//fs.IncreaseTemperature(5.0f * _flareBurnCoefficient);
			fs.Ignite();
		}
		//}
	}

	//Linear decrease: y = (-x/timemax) + 1
	//Sinusoidal decrease: y = sin(-x/timemax) + 1
	//Exponential decrease: y = ((-x^p)/timemax) + 1
	//Sigmoid babyyy hahaa: y = (1 / (1 + e^(9x-5))) - 0.025
	//Playground Slide curve: y = ( (3x/(6^3x) * 10 ) -0.2

	/* God's Perfect Function
	 * legend: T = max lifespan, K = starting curve steepness, J = ending curve steepness
	 * Starting curve is used up until it reaches 1, then it plateaus until T/2, from which it uses the ending curve
	 * 
	 * 
	*		FOR X < T/K
	*			y = ( -((Kx-T)^4) / T^4 ) + 1
	*		FOR T/K < X < T/J
	*			y = 1
	*		FOR X > T/J
	*			y = ( -((Jx-T)^2) / T^2 ) + 1
	* 
	*	If computation is too expensive, lower 4th power to 2nd
	* 
	*/

	// CLAMP RESULT YA YELLOW BELLIED COWARD

	const float K = 14f;
	const float J = 2f;
	private float T = 0.0f; // set in Awake
	float noise = 0.0f;
	float flareBrightness(float x, float n) {
		Random.InitState(System.DateTime.Now.Millisecond);
		noise = Random.value * n;
		if (x < T / K) {
			return ((-(Mathf.Pow((K * x) - T, 4)) / Mathf.Pow(T, 4)) + 1) + noise;
		} else if (x > T / J) {
			return ((-(Mathf.Pow((J * x) - T, 2)) / Mathf.Pow(T, 2)) + 1) + noise;
		} else {
			return 1 + noise;
		}
	}
}
