﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientAudioAdjuster_MultiClip : AmbientAudioAdjuster {

	[SerializeField]
	private float[] _randomInterval = new float[2];

	[SerializeField]
	private AudioClip[] _audioClips;

	private float _timer = 0.0f;
	private float _timeMax;
	// Start is called before the first frame update
	new void Start() {
		base.Start();
		_audioSource.loop = false;
		_timeMax = Random.value.Remap(0f, 1f, _randomInterval[0], _randomInterval[1]);
	}

	// Update is called once per frame
	new void Update() {
		base.Update();
		if ((_timer += Time.deltaTime) >= _timeMax) {
			if (_audioSource.volume > 0) {
				_audioSource.PlayOneShot(_audioClips[(int)(Random.value * (_audioClips.Length - 1))]);
				//Debug.Log("Played wave @volume:" + _audioSource.volume);
			}
			_timeMax = Random.value.Remap(0f, 1f, _randomInterval[0], _randomInterval[1]);
			_timer = 0.0f;
		}
	}
}
