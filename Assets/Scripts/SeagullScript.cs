﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeagullScript : MonoBehaviour {
	Animator _seagullAnimator;
	bool _valid = false;

	bool _airborne = false;
	bool _tryingToLand = false;

	// How often to query for landings while airborne
	const float LandQueryDefault = 10.0f;
	float LandQueryCooldown = LandQueryDefault;
	float landQueryTimer = 0.0f;
	// The probability of choosing to land
	const float LandQueryChance = 0.5f;
	//...

	// How often to roll for taking off when sitting
	const float TakeOffQueryCooldown = 5.0f;
	float takeOffQueryTimer = 0.0f;
	// Chance of taking off
	const float TakeOffChance = 0.1f;

	// Points where seagulls can dock
	private static List<Transform> _seagullPoints = new List<Transform>();
	private static List<bool> _seagullPointsOccupation = new List<bool>();

	private Transform _landingTarget = null;

	private Vector3 _velocity = new Vector3();

	private float _orbitRadius = 100.0f;
	private const float _orbitHeightDefault = 50.0f;
	private float _orbitHeight = _orbitHeightDefault;
	private float _orbitAngle = 0.0f;

	private int _mySeagullPoint = -1;

	[SerializeField]
	private AudioClip[] seagullSquawkClips;
	[SerializeField]
	private AudioClip[] seagullAmbClips;
	private AudioSource audioSource;

	private FireScript _fireScript;

	private DetectableSoundScript DSS;

	private void Start() {
		Random.InitState(System.DateTime.Now.Millisecond);

		// Find all the points where a seagull may land
		if (_seagullPoints.Count <= 0) {
			GameObject[] points = GameObject.FindGameObjectsWithTag("SeagullPoint");
			foreach (GameObject point in points) {
				_seagullPoints.Add(point.transform);
				_seagullPointsOccupation.Add(false);
			}
		}

		Random.InitState(System.DateTime.Now.Millisecond);
		audioSource = GetComponent<AudioSource>();

		_fireScript = GetComponent<FireScript>();

		if (!DSS) {
			DSS = gameObject.AddComponent<DetectableSoundScript>();
		}
	}
	void Awake() {
		_orbitHeight += transform.position.y;
		_seagullAnimator = GetComponentInChildren<Animator>();
		if (_seagullAnimator) {
			_valid = true;
			//Debug.Log("Seagull valid!");
		} else {
			Debug.LogError("Seagull not valid");
		}
		LandQueryCooldown = LandQueryDefault + (Random.value * 7.0f);
	}

	Vector3 newPos = new Vector3();
	// Update is called once per frame
	void Update() {
		if (_airborne) {
			/*_orbitAngle += 10.0f * Time.deltaTime;
			if (_orbitAngle > 360.0f) {
				_orbitAngle -= 360.0f;
			}*/

			if (!_tryingToLand) {
				landQueryTimer += Time.deltaTime;
				if (landQueryTimer >= LandQueryCooldown) {
					if (QueryLand()) {
						// Is it occupied? If not, target it, otherwise stay in air.
						int i = Mathf.RoundToInt(Random.value * (_seagullPoints.Count - 1));
						if (!_seagullPointsOccupation[i]) {
							_landingTarget = _seagullPoints[i];
							_tryingToLand = true;
							_mySeagullPoint = i;
							_seagullPointsOccupation[i] = true;
						}
					}
					landQueryTimer = 0.0f;
				}

				transform.Rotate(Vector3.up, 55.0f * (1.2f - (transform.position.y / _orbitHeight)) * Time.deltaTime);
				_velocity = transform.forward * 7.0f * ((transform.position.y / _orbitHeight) + 0.1f);
				newPos = transform.position;
				transform.LookAt(transform.position + _velocity);

				if (transform.position.y > _orbitHeight) {
					newPos.y = _orbitHeight;
					_velocity.y = 0.0f;
				} else {
					_velocity.y = 20.0f * (1.0f - (transform.position.y / _orbitHeight));
				}


				newPos += _velocity * Time.deltaTime;
				transform.position = newPos;
			} else {
				transform.LookAt(_landingTarget, Vector3.up);
				_velocity = transform.forward * 15.0f;
				if ((_landingTarget.position - transform.position).magnitude < 15.0f * Time.deltaTime) {
					Land();
				}
				newPos += _velocity * Time.deltaTime;
				transform.position = newPos;
			}
		} else {
			takeOffQueryTimer += Time.deltaTime;
			if (takeOffQueryTimer > TakeOffQueryCooldown) {
				if (QueryTakeoff()) {
					_seagullAnimator.SetTrigger("TakeOff");
				}

				takeOffQueryTimer = 0.0f;
			}
		}
	}

	public void Scare() {
		_seagullAnimator.SetTrigger("TakeOff");
		audioSource.clip = seagullSquawkClips[Mathf.RoundToInt(Random.value * (seagullSquawkClips.Length - 1))];
		audioSource.Play();
		DSS.CreateDetectableMovingSound(1.0f, 1.5f);
		//SQUAWK
	}

	// Sends boost and resets the point we're sat on
	public void TakeOff() {
		// Impulse upward
		_airborne = true;
		//Debug.Log("Taking off!");

		// This spot is now free for other seagulls! :)
		if (_mySeagullPoint >= 0) {
			_seagullPointsOccupation[_mySeagullPoint] = false;
		}
	}

	private bool QueryLand() {
		// Randomise the land cooldown
		LandQueryCooldown = LandQueryDefault + (Random.value * 7.0f);

		if (Random.value <= LandQueryChance) {
			//Debug.Log("Gonna Land!");
			return true;
		} else {
			audioSource.clip = seagullAmbClips[Mathf.RoundToInt(Random.value * (seagullAmbClips.Length - 1))];
			audioSource.Play();
		}
		return false;
	}

	private bool QueryTakeoff() {
		//Random.InitState(System.DateTime.Now.Millisecond);
		if (Random.value <= TakeOffChance) {
			//Debug.Log("Gonna take off!!");
			//Debug.Log("Gonna take off!!");
			return true;
		}
		//Debug.Log("Takeoff roll failed!");
		return false;
	}

	private void Land() {
		_tryingToLand = false;
		_airborne = false;
		_velocity = Vector3.zero;
		transform.position = _landingTarget.position;
		transform.LookAt(transform.position + new Vector3(Random.insideUnitCircle.x, 0.0f, Random.insideUnitCircle.y));
		_seagullAnimator.ResetTrigger("TakeOff");
		_seagullAnimator.SetTrigger("Land");
		//Debug.Log("Landed!!");
	}

	private void OnTriggerEnter(Collider other) {
		if ((other.CompareTag("Player") || other.CompareTag("PlayerProjectile")) && !_airborne && !_tryingToLand) {
			Scare();
			//Debug.Log("Scared!!");
			if (_fireScript && other.CompareTag("PlayerProjectile")) {
				_fireScript.Ignite();
			}
		}
	}
}
