﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluidAttraction : MonoBehaviour {
	// Start is called before the first frame update
	Rigidbody[] metaBalls;

	// How far the attraction will apply, outside of the radius, no attraction occurs
	[SerializeField]
	float MAXDIST = 0.75f;

	// Strength of the attraction
	[SerializeField]
	float ATTRACTION = 15.0f;

	[SerializeField]
	bool _useSquare = false;

	// Repulse if within this value
	[SerializeField]
	float MINDIST = 0.4f;

	// If >0, multiply attraction by mass / baselinemass
	[SerializeField]
	float _BaselineMass = 4f;

	[SerializeField]
	bool _DivideByMass = false;

	void Awake() {
		metaBalls = GetComponentsInChildren<Rigidbody>();
		MAXDIST *= transform.localScale.x;
		MINDIST *= transform.localScale.x;
	}

	void FixedUpdate() {
		for (int i = 0; i < metaBalls.Length; i++) {
			for (int j = 0; j < metaBalls.Length; j++) {
				// Don't calc attraction for self or previously calculated balls (prevents doubling)
				if (j <= i) {
					continue;
				}
				Vector3 directionAtoB = metaBalls[i].position - metaBalls[j].position;

				float dist = _useSquare ? directionAtoB.sqrMagnitude : directionAtoB.magnitude;
				if (dist > MAXDIST) {
					// Break the bond
					continue;
				}

				float multiplierA;
				float multiplierB;
				if (dist < MINDIST) {
					multiplierA = (dist - MINDIST) / MINDIST;
				} else {
					multiplierA = (dist - MINDIST) / (MAXDIST * (1 - (MINDIST / MAXDIST)));
				}
				if (_BaselineMass > 0.0f) {
					// Magnify attraction based on the averaged mass of the two particles
					multiplierA *= (metaBalls[i].mass + metaBalls[j].mass) / (_BaselineMass * 2);
				}
				multiplierB = multiplierA;
				// Do we decrease the forces by mass?
				if (_DivideByMass) {
					multiplierA /= metaBalls[i].mass;
					multiplierB /= metaBalls[j].mass;
				}
				metaBalls[i].AddForce(-directionAtoB.normalized * ATTRACTION * multiplierA, ForceMode.Force);
				metaBalls[j].AddForce(directionAtoB.normalized * ATTRACTION * multiplierB, ForceMode.Force);
			}
		}
	}
}
