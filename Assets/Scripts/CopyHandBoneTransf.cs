﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyHandBoneTransf : MonoBehaviour {
	[SerializeField]
	Transform handBone;

	// Update is called once per frame
	void Update() {
		this.transform.SetPositionAndRotation(handBone.position, handBone.rotation);
	}
}
