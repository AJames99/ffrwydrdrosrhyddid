﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Footsteps : MonoBehaviour {
	private const float DistancePerStep = 3f;

	[SerializeField]
	private AudioSource _audioSource;
	[SerializeField]
	private AudioSource _audioSourceScrape;

	[SerializeField]
	private AudioClip[] _stepClips;

	[SerializeField]
	private AudioClip[] _jumpClips;

	[SerializeField]
	private AudioClip[] _bounceClips;

	[SerializeField]
	private float _bounceThreshold = 10f;

	[SerializeField]
	private AudioClip _landClip;

	[SerializeField]
	private AudioClip _slideClip;

	private bool _shouldBounceParticle = false;

	private List<AudioClip> _shuffledWalkClips;
	private List<AudioClip> _shuffledJumpClips;
	private List<AudioClip> _shuffledBounceClips;

	private Vector3 _prevPos;
	private float _distanceCovered;

	private DetectableSoundScript DSS;

	private void Start() {
		DSS = gameObject.AddComponent<DetectableSoundScript>();

		_shuffledWalkClips = new List<AudioClip>(_stepClips);
		_shuffledJumpClips = new List<AudioClip>(_jumpClips);
		_shuffledBounceClips = new List<AudioClip>(_bounceClips);

		_audioSourceScrape.clip = _slideClip;
		_audioSourceScrape.loop = true;
	}

	public void ExternalUpdate(bool isGonnaJump, bool isGrounded, bool isLandedThisFrame, float speed, bool isSliding) {
		if (isSliding && !_audioSourceScrape.isPlaying) {
			// If we're already playing; don't restart
			_audioSourceScrape.Play();
		} else if (!isSliding && _audioSourceScrape.isPlaying) {
			_audioSourceScrape.Stop();
		}
		if (_audioSource.clip == _slideClip && _audioSource.isPlaying) {
			_audioSource.Stop();
		}
		if (_distanceCovered > DistancePerStep) {
			_distanceCovered = 0;
			_audioSource.PlayOneShot(_shuffledWalkClips[0]);
			_shuffledWalkClips.Shuffle();
		}

		if (isGonnaJump && isGrounded) {
			if (speed > _bounceThreshold) {
				AudioSource.PlayClipAtPoint(_shuffledBounceClips[0], GetComponent<Transform>().position, 0.3f);
				_shuffledBounceClips.Shuffle();
			} else {
				_audioSource.PlayOneShot(_shuffledJumpClips[0]);
				_shuffledJumpClips.Shuffle();
			}
			DSS.CreateDetectableSound(transform.position, 3f, 0.1f);
		}

		if (isLandedThisFrame && !isGonnaJump) {
			_audioSource.PlayOneShot(_landClip);
			DSS.CreateDetectableSound(transform.position, 4f, 0.1f);
		}

		if (isGrounded && !isSliding) {
			_distanceCovered += Vector3.Distance(_prevPos.WithY(0), transform.position.WithY(0));
		}

		_prevPos = transform.position;
	}
}
