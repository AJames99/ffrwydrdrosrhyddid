﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpiderScript : MonoBehaviour, IDamageable {

	private float health = 100;

	static private Transform _player;
	private NavMeshAgent _navMeshAgent;
	private Animator _spiderAnimator;

	//[HideInInspector]
	private bool _canSeePlayer;

	[SerializeField]
	LayerMask playerLayer = 0;

	[SerializeField]
	private float _detectionReset = 12f; //12 down to 5 is detection
	[SerializeField]
	private float _suspicionReset = 5f; // 5 down to 0 is suspicious
	private const float _idleReset = 0f; // once 0 is hit, go to idle!
	private float _resetTimer = 0f;


	// How long to wait before changing target position when Idle/Wandering
	[SerializeField]
	private float _idleChangePosTime = 5f;
	private float _wanderTimeMax;
	[SerializeField]
	private float _wanderRadius = 5.0f;
	private float _wanderTimer;

	private const uint IDLE = 0;
	private const uint SUSPICIOUS = 1;
	private const uint ALERT = 2;
	private uint _alertness;// = IDLE;

	private Transform _target = null;
	private Vector3 _targetPosition; // Used for situations where the spider should only know of the position at the point of detection, rather than constantly

	private Queue<int> _objectMemory = new Queue<int>(10);
	private const float _forgetTime = 25.0f;
	private float _memoryTimer = 0.0f;

	[SerializeField]
	private LookerScript[] _detectors;


	// Used for looking for the player...
	[SerializeField]
	[Range(1f, 0f)]
	private float _visionAngleCosine = 0.5f; // The maximum angle cosine between facing direction and player that we can see at (a field of view)
	[SerializeField]
	[Range(1f, 80f)]
	private float _visionDistanceAlert = 15.0f;
	[SerializeField]
	[Range(1f, 80f)]
	private float _visionDistanceSuspicion = 30.0f;


	[SerializeField]
	private float idleWanderSpeed = 5.0f;
	private const float idleWanderSlowMaxDistance = 3.0f; //at what distance-to-idle-target (i.e. wander point) do we begin to decrease speed?
	[SerializeField]
	private float suspiciousWanderSpeed = 7.0f;
	[SerializeField]
	private float detectionRunSpeed = 20.0f;

	private const float MaxAnimRunSpeed = 8.0f; // Any movement speed above this will not increase the animation rate

	private const float _visionPoll = 0.5f;
	private float _visionTimer = 0.0f;
	//...

	// Static to stop the player from being pounced by multiple enemies
	private static bool _movementLocked = false;

	[Header("Spider Sounds")]
	[SerializeField]
	private AudioClip _bodyImpactClip;
	[SerializeField]
	private AudioClip _skullSmackClip;
	[SerializeField]
	private AudioClip _skinBreakClip;
	[SerializeField]
	private AudioClip _fleshPierce;
	[SerializeField]
	private AudioClip _biteClip;
	[SerializeField]
	private AudioClip[] _clawClips;
	[SerializeField]
	private AudioClip[] _warningClips;
	[SerializeField]
	private AudioClip _leapClip;
	[SerializeField]
	private AudioClip[] _painClips;
	[SerializeField]
	private AudioClip _dieClip;
	[SerializeField]
	AudioClip[] _footstepSounds;
	AudioSource _footstepAudioSource;
	AudioSource _otherAudioSource;

	StingerAudioManager stingerSound;

	//[SerializeField]
	//private NavMeshSurface navMesh;

	private NavMeshPath _path;

	private const float _pounceDist = 3.5f;

	private Vector3 _slidePos;
	private const float _slidePosRate = 5f;
	private bool _pouncing = false;

	private FireScript _fs;
	private bool _hasFireScript = false;

	private const float PainSoundTimeMax = 5.0f;
	private float _painSoundTime = PainSoundTimeMax;
	private float _paintSoundTimer = 0.0f;

	private bool bDead = false;

	private void Awake() {
		bDead = false;
		_navMeshAgent = GetComponent<NavMeshAgent>();
		_spiderAnimator = GetComponent<Animator>();
		ChangeAlertness(IDLE);
		_target = null;

		if (!_player) {
			_player = null;
			//_player = GameObject.FindGameObjectWithTag("Player").transform;
			GameObject[] playertaggeds = GameObject.FindGameObjectsWithTag("Player");

			foreach (GameObject obj in playertaggeds) {
				if (obj.GetComponent<FpsController>()) {
					_player = obj.transform;
					continue;
				}
			}
		}

		Random.InitState(System.DateTime.Now.Millisecond);
		_wanderTimeMax = _idleChangePosTime;

		_path = new NavMeshPath();

		_footstepAudioSource = GetComponents<AudioSource>()[0];
		_otherAudioSource = GetComponents<AudioSource>()[1];

		stingerSound = _player.GetComponentInChildren<StingerAudioManager>();

		_fs = GetComponent<FireScript>();
		_hasFireScript = _fs;
	}

	public void SendStimulus(LookerScript.detectionTypes detectionType, uint degree, Transform target = null) {
		if (!bDead) {
			if (detectionType == LookerScript.detectionTypes.PLAYER) {
				// If the detection degree is greater than what we are at now, increase it to the input
				if (_alertness <= degree) {
					ChangeAlertness(degree);
					// DEBUG
					//Debug.Log(_alertness >= ALERT ? "PLAYER SENSED!!!" : "What's that moving over there?");
					_target = _player;
				}

			} else if (detectionType == LookerScript.detectionTypes.OBJECT) {
				if (!_objectMemory.Contains(target.GetInstanceID())) {
					if (_alertness <= degree) {
						// Objects cannot put us into the "Alert" state
						// Alert degree instead functions as us recognising/memorising the object
						// and resolving our "suspicion" about the object
						ChangeAlertness((uint)Mathf.Min(degree, SUSPICIOUS));
						_target = target;
					}

					//Debug.Log("OBJECTSTIMULUS: " + detectionType + ", " + degree + ", ALERT: " + _alertness);

					// Once we've approached the object close enough, add it to our memory, keeping the memory at 10 objects
					if (degree >= ALERT) {
						_objectMemory.Enqueue(target.GetInstanceID());
						Debug.Log("Object memorised! " + target.GetInstanceID());
						while (_objectMemory.Count > 10) {
							_objectMemory.Dequeue();
							Debug.Log("Shortened memory to 10");
						}
					}/* else {
					Debug.Log("What's that object over there? " + target.GetInstanceID());
				}*/
				}/* else {
				Debug.Log("I remember that object! " + target.GetInstanceID());
			}*/
			}

			// Sample the target's position once. (Used for suspicious/idle modes to avoid perfect tracking)
			// UPDATE: Using proper navmesh sampling to find the nearest point to our target, allowing for tracking of very high/distant objects.
			if (_target) {
				//_targetPosition = _target.position;
				GetNearestPointToTarget(_target.position, out _targetPosition);
			}

			_navMeshAgent.destination = _targetPosition;
		}
	}

	private bool GetNearestPointToTarget(Vector3 sourcePos, out Vector3 result) {
		NavMeshHit navHit;
		if (NavMesh.SamplePosition(sourcePos, out navHit, 50f, NavMesh.AllAreas)) {
			result = navHit.position;
			return true;
		} else {
			result = sourcePos;
			return false;
		}
	}

	private void ChangeAlertness(uint alertness) {
		ForceAlertness(alertness);

		switch (_alertness) {
			case ALERT:
				_resetTimer = Mathf.Max(_resetTimer, _detectionReset);
				//_navMeshAgent.speed = detectionRunSpeed;
				break;
			case SUSPICIOUS:
				_resetTimer = Mathf.Max(_resetTimer, _suspicionReset);
				//_navMeshAgent.speed = suspiciousWanderSpeed;
				break;
			default:
				_target = null;
				_resetTimer = Mathf.Max(_resetTimer, _idleReset);
				//_navMeshAgent.speed = idleWanderSpeed;
				break;
		}
	}
	private void ForceAlertness(uint alertness) {
		_alertness = alertness;

		switch (_alertness) {
			case ALERT:
				ModifySpeed(detectionRunSpeed);
				break;
			case SUSPICIOUS:
				ModifySpeed(suspiciousWanderSpeed);
				break;
			default:
				_target = null;
				ModifySpeed(idleWanderSpeed);
				break;
		}
	}

	private void ModifySpeed(float speed) {
		if (!_movementLocked) {
			//Debug.Log("Speed modified to " + speed);
			_navMeshAgent.speed = speed;
		}
	}

	void Update() {
		if (!bDead) {
			if (_hasFireScript) {
				if (_fs._isBurning) {
					//Damage(12f * Time.deltaTime);
					Damage(10f * Time.deltaTime);
					_paintSoundTimer += Time.deltaTime;
					if (_paintSoundTimer > _painSoundTime) {
						PlaySound("Pain");
						_painSoundTime = PainSoundTimeMax + (Random.value * 2.0f);
						_paintSoundTimer = 0.0f;
					}
				}
			}

			if (_alertness > IDLE) {
				// Transition down the alertness states
				_resetTimer -= Time.deltaTime;
				//Debug.Log(_resetTimer);

				// If we're alert and our timer goes below the suspicion-alert boundary, go to Suspicious
				if (_alertness >= ALERT) {
					if (_resetTimer < _suspicionReset) {
						Debug.Log("Alertness degraded to Suspicious");
						ForceAlertness(SUSPICIOUS);
						_target = null;
					} else {
						//Actively track our target if we're alert
						_navMeshAgent.destination = _target.position;
					}
				}
				// If we're suspicious and our timer goes to 0 (idle reset), go to Idle
				else {
					if (_resetTimer < _idleReset) {
						Debug.Log("Alertness degraded to Idle");
						ForceAlertness(IDLE);
						_target = null;
					}
				}
			} else {
				if ((_wanderTimer += Time.deltaTime) >= _wanderTimeMax) {
					_targetPosition = RandomNavSphere(transform.position, _wanderRadius * ((Random.value + 0.5f) * 3f), NavMesh.AllAreas); //-1
					_navMeshAgent.destination = _targetPosition;
					_wanderTimer = 0f;
					//Debug.Log("Wandering somewhere new! " + newPos);
					_wanderTimeMax = _idleChangePosTime + ((Random.value - 0.5f) * 4f);

					CalculatePath(_targetPosition);
				}

				//Slow down as we approach the target (makes legs look less silly)
				//if (!_target) {
				float dist = (_navMeshAgent.destination - transform.position).magnitude;
				ModifySpeed(dist > idleWanderSlowMaxDistance ? idleWanderSpeed : idleWanderSpeed * (dist / idleWanderSlowMaxDistance));
				//}
			}

			if (_objectMemory.Count > 0) {
				if ((_memoryTimer += Time.deltaTime) >= _forgetTime) {
					if (_objectMemory.Count > 0) {
						Debug.Log("I forgot about " + _objectMemory.Dequeue() + "...");
					}
					_memoryTimer = 0.0f;
				}
			}

			_visionTimer += Time.deltaTime;
			if (_visionTimer >= _visionPoll) {
				_visionTimer = 0.0f;
				PollVision();
			}

			// Move toward target constantly if we're Alerted by it
			if (_target && _alertness >= ALERT) {
				_navMeshAgent.destination = _target.position;
				if ((_player.position - transform.position).magnitude <= _pounceDist && !_movementLocked && !_pouncing) {
					PounceOnPlayer();
				}
			}

			_spiderAnimator.SetFloat("moveSpeed", Mathf.Min(MaxAnimRunSpeed, _navMeshAgent.velocity.magnitude));
		}
	}

	private void CalculatePath(Vector3 objective) {
		NavMesh.CalculatePath(transform.position, objective, NavMesh.AllAreas, _path);
		for (int i = 0; i < _path.corners.Length - 1; i++) {
			Debug.DrawLine(_path.corners[i], _path.corners[i + 1], Color.red, 5.0f);
		}
	}

	public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
		Vector3 randDirection = Random.insideUnitSphere * dist;
		randDirection += origin;

		NavMeshHit navHit;
		NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

		return navHit.position;
	}

	private void PounceOnPlayer() {

		StingerAudioManager.SAM.JumpScare();

		_navMeshAgent.speed = 0;
		_movementLocked = true;
		_spiderAnimator.SetBool("Pouncing", true);

		// Move the spider to the same height
		//transform.position = transform.position.WithY(_player.position.y);
		// Move the player to the spider's height
		//_player.position = _player.position.WithY(transform.position.y);
		// Move us a fixed dist from the player
		transform.position = transform.position + (transform.position - _player.position).normalized * _pounceDist;

		if (!_player) {
			Debug.LogError("PLAYER REF DOES NOT EXIST");
		} else if (!_player.GetComponent<FpsController>()) {
			Debug.LogError("FPS CONTROLLER COMPONENT REF DOES NOT EXIST");
		}

		_player.GetComponent<FpsController>().PouncedOn(transform.position);

		//transform.position = transform.position.WithY(_player.position.y);

		//Debug.Log("Spider measured player Y as " + _player.position.y);

		// Look at player
		transform.LookAt(_player, Vector3.up);
	}

	private void PollVision() {
		Debug.DrawRay(transform.position, transform.forward, Color.red, 2.0f);

		Vector3 dir = _player.position - transform.position;

		// If the player is within the maximum vision range...
		if (dir.magnitude < _visionDistanceSuspicion) {
			//Debug.Log("Player in vision radius!");
			//... and he's within the view angle...
			if (Vector3.Dot(transform.forward.normalized, dir.normalized) >= _visionAngleCosine) {
				//Debug.Log("Player in vision angle");
				// ... and nothing obstructs the ray from Spider to Player...
				if (!Physics.Raycast(transform.position, dir.normalized, dir.magnitude, ~playerLayer, QueryTriggerInteraction.Ignore)) {
					// Send a vision stimulus of the appropriate strength
					SendStimulus(LookerScript.detectionTypes.PLAYER, dir.magnitude < _visionDistanceAlert ? ALERT : SUSPICIOUS);
					//Debug.Log("Player sighted!: " + (dir.magnitude < _visionDistanceAlert ? "ALERT" : "SUSPICIOUS"));
				}
			}
		}

		//Simply check for the nearest flare. No view angle or raycasts, sorta unnecessary.

		Transform[] flares;
		if (FlareScript.GetFlaresInWorld(out flares)) {
			if (flares[0]) {
				float nearestFlareDist = (flares[0].position - transform.position).magnitude;
				int nearestFlare = 0;
				for (int i = 1; i < flares.Length; i++) {
					if (flares[i]) {
						if ((flares[i].position - transform.position).magnitude < nearestFlareDist) {
							nearestFlareDist = (flares[i].position - transform.position).magnitude;
							nearestFlare = i;
						}
					}
				}

				if (nearestFlareDist < _visionDistanceSuspicion) {
					SendStimulus(LookerScript.detectionTypes.OBJECT, SUSPICIOUS, flares[nearestFlare]);
					//Debug.Log("I see a flare! " + flares[nearestFlare].position);
				}
			}
		}
	}

	public void FootStep() {
		_footstepAudioSource.PlayOneShot(_footstepSounds[Mathf.RoundToInt(Random.value * (_footstepSounds.Length - 1))], Random.value);
	}

	public void PlaySound(string snd) {
		switch (snd) {
			case "BodyImpact":
				_otherAudioSource.PlayOneShot(_bodyImpactClip, 0.55f);
				break;
			case "SkullSmack":
				_otherAudioSource.PlayOneShot(_skullSmackClip, 5.0f);
				break;
			case "Bite":
				_otherAudioSource.PlayOneShot(_fleshPierce, 0.65f);
				_otherAudioSource.PlayOneShot(_skinBreakClip, 0.65f);
				_otherAudioSource.PlayOneShot(_biteClip, 0.5f);
				break;
			case "Claw":
				_otherAudioSource.PlayOneShot(_clawClips[Mathf.RoundToInt(Random.value * (_clawClips.Length - 1))], 0.5f);
				break;
			case "Warning":
				_otherAudioSource.PlayOneShot(_warningClips[Mathf.RoundToInt(Random.value * (_warningClips.Length - 1))], 0.65f);
				break;
			case "Leap":
				_otherAudioSource.PlayOneShot(_leapClip, 2.5f);
				break;
			case "Pain":
				_otherAudioSource.PlayOneShot(_painClips[Mathf.RoundToInt(Random.value * (_painClips.Length - 1))], 0.65f);
				break;
			case "Die":
				_otherAudioSource.PlayOneShot(_dieClip, 1.0f);
				break;
		}
	}

	public void Damage(float dmg) {
		if (!bDead) {
			health -= dmg;
			if (health <= 0) {
				_spiderAnimator.SetBool("Dead", true);
				PlaySound("Die");
				bDead = true;
				_navMeshAgent.speed = 0.0f;
				//_player.GetComponentInChildren<StingerAudioManager>().EnemyRemoved(transform);
				StingerAudioManager.SAM.EnemyRemoved(transform);
			}
		}
	}

	public void Heal(float hp) {
		health += hp;
	}

	public float GetHealth() {
		return health;
	}

	public void SetHealth(float hp) {
		health = hp;
	}
}
