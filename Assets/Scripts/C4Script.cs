﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C4Script : MonoBehaviour {
	[SerializeField]
	private GameObject explosion;

	private bool stuck = false;


	public void Explode() {
		Vector3 location = this.GetComponent<Transform>().position;


		Instantiate(explosion, location, Quaternion.identity);

		Destroy(this.gameObject);
	}

	public void OnCollisionEnter(Collision collision) {
		if (!stuck) {
			Transform transf = this.gameObject.GetComponent<Transform>();
			Transform other;
			if (collision.gameObject.TryGetComponent<Transform>(out other)) {
				//MoveToSurface(ref transf, ref collision);
				RotateToSurfaceNormal(ref transf, ref other, ref collision);
				MoveToSurface(ref transf, ref collision);
				transf.SetParent(other);
				//Debug.Log("C4 Stuck!");
				Rigidbody rb;
				if (this.gameObject.TryGetComponent<Rigidbody>(out rb)) {
					Destroy(rb); // Remove the rigidbody so we are stuck to the thing we hit and it become a child of it
				}
				stuck = true;
			}
		}
	}

	// Should work with sphere collider on self and any collider on other.
	private void RotateToSurfaceNormal(ref Transform transf, ref Transform other, ref Collision collision) {
		List<ContactPoint> contacts = new List<ContactPoint>();
		int count = collision.GetContacts(contacts);
		Vector3 averageNormal = Vector3.zero;
		Vector3 averagePos = Vector3.zero;

		int i;
		for (i = 0; i < count; i++) {
			var contact = contacts[i];
			averageNormal += contact.normal;
			averagePos += contact.point;
		}
		averagePos /= i;
		averageNormal /= i;

		transf.LookAt(averagePos + averageNormal);
		//transf.rotation = Quaternion.FromToRotation(transf.rotation * Vector3.forward, averageNormal);
		//transf.Rotate(axisX, angleX);
		//transf.Rotate(axisY, angleY);

	}

	private void MoveToSurface(ref Transform transf, ref Collision collision) {
		// Stick it closer to the surface
		transf.position = collision.GetContact(0).point + (collision.GetContact(0).normal * 0.1f);
	}
}
