﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookerScript : MonoBehaviour {

	public SpiderScript guard;
	private SphereCollider sphereDetector;

	private float _pollRate = 1.0f;
	private float _pollCurrent = 0.0f;

	public enum detectionTypes {
		PLAYER,
		OBJECT
	}

	// The higher the value, the more stimulating/resolving the detection will be
	[SerializeField]
	[Range(1, 3)]
	private uint detectionDegree = 1;


	private void Awake() {
		guard = GetComponentInParent<SpiderScript>();
		sphereDetector = GetComponent<SphereCollider>();
	}

	private void Update() {
		if ((_pollCurrent += Time.deltaTime) > _pollRate) {
			sphereDetector.enabled = true;
			_pollCurrent = 0f;
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") { //might need to be other.gameObject.tag?
			guard.SendStimulus(detectionTypes.PLAYER, detectionDegree);
			sphereDetector.enabled = false;
		} else if (other.tag == "PlayerProjectile") {
			guard.SendStimulus(detectionTypes.OBJECT, detectionDegree, other.transform);
			sphereDetector.enabled = false;
		} else if (other.tag == "Flammable") {
			if (other.gameObject.GetComponent<FireScript>()._isBurning) {
				guard.SendStimulus(detectionTypes.OBJECT, detectionDegree, other.transform);
				sphereDetector.enabled = false;
			}
		}
	}
}
