﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This simply forwards the animation events on the Flare Gun skeleton to the Flare Gun Arms' script
public class FlareGunHandler : MonoBehaviour {

	FlareGunAnimCallback flareGunScript;
	public void Awake() {
		flareGunScript = GetComponentInParent<FlareGunAnimCallback>();
	}
	public void SetHammerHit() {
		if (flareGunScript) {
			flareGunScript.SetHammerHit();
		}
	}

	public void SetHammer(int val) {
		if (flareGunScript) {
			flareGunScript.SetHammer(val);
		}
	}

	public void TriggerPulled(int trigger) {
		if (flareGunScript) {
			flareGunScript.TriggerPulled(trigger == 1);
		}
	}
}
