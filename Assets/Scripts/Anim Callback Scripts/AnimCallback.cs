﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimCallback : MonoBehaviour {

	//This contains function labels for generic things that all weapons/arms should be able to do, and are triggered by other scripts like the FPS Controller
	public abstract void Jump();

	public abstract void SetGrounded(bool grounded);
	public abstract void SetMoveSpeed(float movespeed, float max);
	public abstract void SetSliding(bool sliding);
}
