﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareGunAnimCallback : AnimCallback {

	[SerializeField]
	// Used to get the transform of the loose bone at the point at which the loose flare mesh is disabled and the spent flare prefab instantiated in its place
	private GameObject LooseFlareBone;

	[SerializeField]
	// Used to toggle the chambered round mesh visibility
	private SkinnedMeshRenderer ChamberedRoundMesh;

	[SerializeField]
	// Used to toggle the chambered round mesh visibility
	private MeshRenderer LooseRoundMesh;
	[SerializeField]
	// Used to toggle the chambered round mesh visibility
	private MeshRenderer LooseRoundMeshDouble;
	[SerializeField]
	// Used to toggle the chambered round mesh visibility
	private MeshRenderer LooseRoundMeshTriple;

	[SerializeField]
	// The prefab with physics that represents a flare round, used when discarding rounds to instance one in the world
	private GameObject FlareRoundPrefab;
	/*[SerializeField]
	// The prefab with physics that represents a flare round, used when discarding rounds to instance one in the world
	private GameObject FlareRoundSpentPrefab;*/

	[SerializeField]
	// The prefabs that represent fired flares
	private GameObject[] FlareShotPrefabs;

	public MatsList fm;
	//	[SerializeField]
	//	private Material[] FlareRoundMaterials;

	[SerializeField]
	// The prefab that represents a fired flare
	private Transform FlareShotPoint;

	[SerializeField]
	private float _flareShotForce = 30.0f;

	[Header("Audio")]

	[SerializeField]
	private AudioSource _audioSource;

	[SerializeField]
	private AudioClip[] _fireClips;

	[SerializeField]
	private AudioClip _insertClip;

	[SerializeField]
	private AudioClip _removeClip;

	[SerializeField]
	private AudioClip _openClip;

	[SerializeField]
	private AudioClip _closeClip;

	[SerializeField]
	private AudioClip _cockClip;

	[SerializeField]
	private AudioClip _hitClip;

	[SerializeField]
	private AudioClip _flickClip;

	[SerializeField]
	// The hand animator must be instructed with variables and triggers
	private Animator _handAnimator;

	// The flaregun animator must be instructed with variables and triggers
	[SerializeField]
	private Animator _flareGunAnimator;

	[SerializeField]
	private Transform _camera;
	private Vector3 cameraOriginalPos;
	private Vector3 cameraDefaultRot = new Vector3(0f, 180f, 0f);

	[SerializeField]
	private Transform _dupeCamera; // The empty that represents the look direction (affected by FpsController)

	[SerializeField]
	private Transform _cameraParent;

	[SerializeField]
	private Transform _cameraBone;
	private Vector3 cameraBoneDefaultPos;

	[SerializeField]
	private ParticleSystem _muzzleSmokeSystem;

	[SerializeField]
	private ParticleSystem _muzzlePoofSystem;

	//private int ammo_available;
	private Dictionary<FlareShellScript.FlareTypes, int> ammo = new Dictionary<FlareShellScript.FlareTypes, int>();
	private FlareShellScript.FlareTypes flareTypeSelected = FlareShellScript.FlareTypes.RedSignal;
	//private int _selectedFlare = 0;
	//private int _chamberedFlare = 0;
	private FlareShellScript.FlareTypes flareTypeChambered = FlareShellScript.FlareTypes.RedSignal;

	private bool previewingFlare = false;

	private bool round_chambered;
	private bool round_is_spent;
	private bool round_held_is_spent = false; // Used for tossiing shells
	private bool hammer_cocked;
	private bool barrel_open;
	private bool thumb_busy = false;
	private bool isKeepingChamberedRound = false;

	private bool _isGrabbing = false;
	public bool isGrabbing {
		set {
			_isGrabbing = value;
			SetGrabbing(value);
		}
		get {
			return _isGrabbing;
		}
	}

	//private bool left_hand_busy = false;

	private bool _inputsLocked;
	private bool _beingPounced = false;

	private float lerp = 0.0f;
	const float lerpRate = 1.0f; // The rate at which the move-sway transition blend moves between 0 and 1

	private float cameraAimParentLerp = 0.0f; //Manages how the arms copy the camera rotation: only copy cameraparent rotation when aiming.
	private float cameraAimParentLerpRate = 3.0f;

	private float recentActionFactor = 1.0f; // Scales between 0 and 1, reset to 0 every time we do an action e.g. opening barrel, to stop movesway from interfering with reload animation

	private DetectableSoundScript DSS;

	//private float _targetArmsRot = 0f;

	//private Vector3 clampedCameraRot = new Vector3();

	public void Start() {

		ammo.Add(FlareShellScript.FlareTypes.RedSignal, 2);
		ammo.Add(FlareShellScript.FlareTypes.WhiteIllum, 1);
		ammo.Add(FlareShellScript.FlareTypes.GreenConc, 0);
		ammo.Add(FlareShellScript.FlareTypes.WhiteSupply, 1);

		//flareTypeChambered = FlareShellScript.FlareTypes.RedSignal;
		SetChamberedRound(0);
		SetRoundChambered(0);
		flareTypeSelected = FlareShellScript.FlareTypes.RedSignal;
		//
		round_held_is_spent = false;

		//SetRoundChambered(1);
		SetHammer(0);
		SetRoundSpent(0);
		SetLooseRound(0);
		//SetChamberedRound(1);
		if (GetComponent<AudioSource>() && !_audioSource) {
			_audioSource = GetComponent<AudioSource>();
		}

		cameraOriginalPos = _camera.localPosition;
		cameraBoneDefaultPos = _cameraBone.localPosition;

		DSS = gameObject.AddComponent<DetectableSoundScript>();
	}

	public override void Jump() {
		//_handAnimator.SetTrigger("Jump");
	}

	public override void SetMoveSpeed(float movespeed, float max) {
		_handAnimator.SetFloat("MoveSpeed", (movespeed / max) * Mathf.Clamp01(recentActionFactor));
	}

	public override void SetSliding(bool sliding) {
		//_handAnimator.SetBool("isSliding", sliding);
	}

	public override void SetGrounded(bool grounded) {
		//_handAnimator.SetBool("isGrounded", grounded);
	}

	public void Awake() {
		//player = GameObject.FindGameObjectWithTag("Player").GetComponent<FpsController>();
		/*_muzzleSmokeSystem.Stop();
		_muzzleSmokeSystem.Clear();
		var em = _muzzleSmokeSystem.emission; em.enabled = false;*/
	}

	public void SetGrabbing(bool grab) {
		_flareGunAnimator.SetBool("isGrabbin", grab);
	}

	public void Update() {
		if (!previewingFlare && !_handAnimator.GetCurrentAnimatorStateInfo(0).IsName("Deselect") && !_handAnimator.GetCurrentAnimatorStateInfo(0).IsName("Select1") && !_handAnimator.GetCurrentAnimatorStateInfo(0).IsName("Select2") && !_handAnimator.GetCurrentAnimatorStateInfo(0).IsName("Select3")) {
			ResetAll();
		}

		// These inputs cannot be locked, as they're independent
		_flareGunAnimator.SetBool("pulling_trigger", Input.GetMouseButton(0));
		_handAnimator.SetBool("pulling_trigger", Input.GetMouseButton(0));
		// triggerPress is set to true when the flare's trigger reaches a point in the anim, and set false here if LMB is released
		if (!Input.GetMouseButton(0)) {
			TriggerPulled(false);
		}

		if (!_beingPounced) {
			_handAnimator.SetBool("aiming", !Input.GetMouseButton(1) || previewingFlare);
		}

		// These inputs are locked in certain scenarios (i.e. firing) to prevent misalignment between the flaregun and the hands
		if (!_inputsLocked && !_beingPounced) {

			//if (!left_hand_busy) {
			if (Input.GetKey(KeyCode.E) && !barrel_open) {
				if (previewingFlare) {
					_handAnimator.SetTrigger("switchoff");
					previewingFlare = false;
				}

				//_flareGunAnimator.SetTrigger("openBarrel");
				_handAnimator.SetTrigger("openBarrel");
				//previewingFlare = false;
				//SetLooseRound(0);
				recentActionFactor = -0.25f;
			}
			if (Input.GetKey(KeyCode.R) && barrel_open) {
				ResetAll();
				if (previewingFlare) {
					_handAnimator.SetTrigger("switchoff");
					previewingFlare = false;
				}

				//_flareGunAnimator.SetTrigger("closeBarrel");
				_handAnimator.SetTrigger("closeBarrel");
				//SetLooseRound(0);
				//previewingFlare = false;
				//recentActionFactor = 0.0f;
				recentActionFactor = 0.0f;
			}
			if (Input.GetKey(KeyCode.Y) && !round_chambered && ammo[flareTypeSelected] > 0 && barrel_open) { // Z?
				ResetAll();
				if (previewingFlare) {
					_handAnimator.SetTrigger("switchoff");
					previewingFlare = false;
				}

				//_flareGunAnimator.SetTrigger("insertRound");
				_handAnimator.SetTrigger("insertRound");

				recentActionFactor = -0.5f;
				flareTypeChambered = flareTypeSelected;
				ChamberedRoundMesh.material = fm.materials[(int)flareTypeSelected];
				LooseRoundMesh.material = fm.materials[(int)flareTypeSelected];
			}
			if (Input.GetKeyDown(KeyCode.V)) {
				if (previewingFlare) {
					ResetAll();
					_handAnimator.SetTrigger("switchoff");
					_handAnimator.SetTrigger("TossShell");
					previewingFlare = false;
					LockInputs(1);
					recentActionFactor = -0.5f;
					LooseRoundMesh.material = fm.materials[(int)flareTypeSelected];
					ModifyAmmo(-1);

				} else if (round_chambered && barrel_open) {
					ResetAll();
					//if (previewingFlare) {
					//_handAnimator.SetTrigger("switchoff");
					//previewingFlare = false;
					//}

					_handAnimator.SetTrigger("removeRound");
					//_flareGunAnimator.SetTrigger("removeRound");
					recentActionFactor = -0.5f;
					ChamberedRoundMesh.material = fm.materials[(int)flareTypeChambered];
					LooseRoundMesh.material = fm.materials[(int)flareTypeChambered];
				}
			}
			//}

			if (Input.GetKeyDown(KeyCode.F) && !hammer_cocked && !thumb_busy) {
				_flareGunAnimator.SetTrigger("cockHammer");
				_handAnimator.SetTrigger("cockHammer");
			}

			// Switch flaretypes
			if (Input.GetKeyDown(KeyCode.Alpha1)) {
				SelectFlare(1);

			} else if (Input.GetKeyDown(KeyCode.Alpha2)) {
				SelectFlare(2);

			} else if (Input.GetKeyDown(KeyCode.Alpha3)) {
				SelectFlare(3);
			} else if (Input.GetKeyDown(KeyCode.Alpha4)) {
				SelectFlare(4);
			}
		} else if (isKeepingChamberedRound) {
			if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.Y) || Input.GetKeyDown(KeyCode.V) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0)) {
				ResetAll();
				_handAnimator.SetTrigger("switchoff");
				_handAnimator.SetTrigger("TossShell");
				previewingFlare = false;
				LockInputs(1);
				recentActionFactor = -0.5f;
			} else if (Input.GetKeyDown(KeyCode.G)) {
				ResetAll();
				_handAnimator.SetTrigger("switchoff");
				previewingFlare = false;
				LockInputs(0);
				recentActionFactor = -0.5f;
			}
		}

		_camera.localPosition = cameraOriginalPos + _cameraBone.localPosition - cameraBoneDefaultPos; // Get any animated movement of the camera from the camera bone on the rig
		_camera.localRotation = Quaternion.Euler(cameraDefaultRot - _cameraBone.localRotation.eulerAngles + _dupeCamera.localRotation.eulerAngles); // Composite rotation from camera bone and FPSController managed camera rot

		if (!_beingPounced) {
			// To match arm rotation to the camera rotation selectively
			_cameraParent.localRotation = LerpArmsRotation(!Input.GetMouseButton(1), Time.deltaTime);
			// For the arm swinging etc.
			LerpMovementSway(Time.deltaTime);
		}

	}

	private void ResetAll() {
		_handAnimator.ResetTrigger("openBarrel");
		_handAnimator.ResetTrigger("closeBarrel");
		_handAnimator.ResetTrigger("insertRound");
		_handAnimator.ResetTrigger("removeRound");

		/*_flareGunAnimator.ResetTrigger("openBarrel");
		_flareGunAnimator.ResetTrigger("closeBarrel");		
		_flareGunAnimator.ResetTrigger("insertRound");		
		_flareGunAnimator.ResetTrigger("removeRound");*/

	}

	private void ResetFGTriggers() {
		_flareGunAnimator.ResetTrigger("openBarrel");
		_flareGunAnimator.ResetTrigger("closeBarrel");
		_flareGunAnimator.ResetTrigger("insertRound");
		_flareGunAnimator.ResetTrigger("removeRound");
	}

	const float actionLerpRate = 0.5f; // rate per second of 0-1 recovery from action factor being set to 0
	private void LerpMovementSway(float dt) {
		// Increase if moving, decrease if not moving
		// Decrease at 5x the rate we increase
		/*lerp += lerpRate * (moving ? 1 : -5) * dt;
		lerp = Mathf.Clamp(lerp, -0.5f, 0.8f);
		return lerp;*/
		recentActionFactor += actionLerpRate * dt;
		recentActionFactor = Mathf.Min(recentActionFactor, 1.0f);
	}

	private Quaternion LerpArmsRotation(bool aiming, float dt) {
		// Smoothly moves between following the camera's pitch and not, depending on whether or not we're aiming
		cameraAimParentLerp += cameraAimParentLerpRate * (aiming ? 2f : -2f) * dt;
		cameraAimParentLerp = Mathf.Clamp01(cameraAimParentLerp);

		Quaternion rot = Quaternion.Slerp(Quaternion.identity, _dupeCamera.localRotation, cameraAimParentLerp);
		return rot;
	}

	public void SetAmmo(int a) {
		//ammo_available = ammo;
		ammo[flareTypeSelected] = a;

		_flareGunAnimator.SetInteger("ammo_available", a);
		_handAnimator.SetInteger("ammo_available", a);
	}

	public void SetAmmo(int a, FlareShellScript.FlareTypes overrideType) {
		if (overrideType != flareTypeSelected) {
			ammo[overrideType] = a;
		} else {
			SetAmmo(a);
		}
	}

	public void Fire() {
		recentActionFactor = -0.2f;
		SetRoundSpent(1);
		previewingFlare = false;
		// FIRE PROJECTILE
		InstanceFlare();
		MakeSound("Fire");
		_muzzleSmokeSystem.Play();
		_muzzlePoofSystem.Play();
		var em = _muzzleSmokeSystem.emission; em.enabled = true;
	}

	public void ModifyAmmo(int change) {
		SetAmmo(ammo[flareTypeSelected] + change);
	}

	public void ModifyAmmo(int change, FlareShellScript.FlareTypes overrideType) {
		Debug.Log("Type " + overrideType + " modified by " + change);
		SetAmmo(ammo[overrideType] + change, overrideType);
	}

	public void SetHammer(int change) {
		hammer_cocked = change == 1;
		_flareGunAnimator.SetBool("hammer_cocked", hammer_cocked);
		_handAnimator.SetBool("hammer_cocked", hammer_cocked);
	}

	public void InstanceRound() {
		if (!isKeepingChamberedRound) {
			if (round_is_spent) {
				SetLooseRound(0);
				// SPAWN ROUND AT BONE
				Transform ShellTransform = LooseFlareBone.transform;

				// If spent, use the "empty" round!
				//GameObject ShellInstance = round_is_spent ? (GameObject)(Instantiate(FlareRoundSpentPrefab)) : (GameObject)(Instantiate(FlareRoundPrefab));
				GameObject ShellInstance = Instantiate(FlareRoundPrefab);
				FlareShellScript fss = ShellInstance.GetComponent<FlareShellScript>();
				fss.spent = round_is_spent;
				fss.type = flareTypeChambered;

				Transform ShellInstanceTransform = ShellInstance.transform;
				//C4Instance.GetComponent<Transform>().localScale = ShellTransform.lossyScale;
				ShellInstanceTransform.position = ShellTransform.position;
				ShellInstanceTransform.rotation = ShellTransform.rotation;
				ShellInstanceTransform.Rotate(new Vector3(90.0f, 0.0f, 0.0f));

				Vector3 vel = -Vector3.up;
				vel = this.transform.rotation * vel;
				ShellInstance.GetComponent<Rigidbody>().AddForce(vel, ForceMode.VelocityChange);

				//Random.InitState(System.DateTime.Now.Millisecond);
				float torque = Random.Range(10, 20);
				ShellInstance.GetComponent<Rigidbody>().maxAngularVelocity = 20;
				ShellInstance.GetComponent<Rigidbody>().AddTorque(ShellInstance.transform.forward * torque, ForceMode.Impulse);
			} else {
				ModifyAmmo(1, flareTypeChambered);
				Debug.Log("Pocketed chambered round!");
			}
		}
	}

	public void TossRound() {
		isKeepingChamberedRound = false;
		SetLooseRound(0);
		Transform ShellTransform = LooseFlareBone.transform;
		// If spent, use the "empty" round!
		GameObject ShellInstance = Instantiate(FlareRoundPrefab);
		FlareShellScript fss = ShellInstance.GetComponent<FlareShellScript>();
		fss.spent = round_held_is_spent;
		fss.type = flareTypeSelected;

		Transform ShellInstanceTransform = ShellInstance.transform;
		ShellInstanceTransform.position = ShellTransform.position;
		ShellInstanceTransform.rotation = ShellTransform.rotation;
		ShellInstanceTransform.Rotate(new Vector3(90.0f, 0.0f, 0.0f));

		Vector3 vel = (transform.forward * 5f) + (transform.up * 3.5f);
		//vel = this.GetComponent<Transform>().rotation * vel;
		ShellInstance.GetComponent<Rigidbody>().AddForce(vel, ForceMode.VelocityChange);

		//Random.InitState(System.DateTime.Now.Millisecond);
		float torque = Random.Range(10, 50);
		ShellInstance.GetComponent<Rigidbody>().AddTorque(Random.onUnitSphere * torque, ForceMode.Impulse);

		MakeSound("Flick");
	}

	public void InstanceFlare() {
		//ker-choooooOOOoooo~
		GameObject FlareShotInstance = (GameObject)(Instantiate(FlareShotPrefabs[(int)flareTypeChambered]));
		Transform FlareShotTransform = FlareShotInstance.transform;
		FlareShotTransform.position = FlareShotPoint.position;
		//FlareShotTransform.rotation = FlareShotPoint.rotation;
		//FlareShotTransform.Rotate(new Vector3(90.0f, 0.0f, 0.0f));



		if (flareTypeChambered == FlareShellScript.FlareTypes.GreenConc) {
			FlareShotTransform.rotation = FlareShotPoint.rotation;
		} else {
			FlareScript fs = FlareShotInstance.GetComponent<FlareScript>();
			fs.SetFlareRotation(FlareShotPoint.transform.rotation);
		}

		Vector3 vel = FlareShotPoint.up * _flareShotForce;
		//vel = this.GetComponent<Transform>().rotation * vel;
		FlareShotInstance.GetComponent<Rigidbody>().AddForce(vel, ForceMode.Impulse);

	}

	public void SetRoundChambered(int val) {
		round_chambered = val == 1;
		_flareGunAnimator.SetBool("round_chambered", round_chambered);
		_handAnimator.SetBool("round_chambered", round_chambered);
	}

	public void SetRoundSpent(int val) {
		round_is_spent = val == 1;
		_flareGunAnimator.SetBool("round_is_spent", round_is_spent);
		_handAnimator.SetBool("round_is_spent", round_is_spent);
	}

	// Firing animation now starts early - when hammer begins to move
	public void SetHammerHit() {
		if (!barrel_open && !round_is_spent && round_chambered) {
			_flareGunAnimator.SetTrigger("hammerHit");
			_handAnimator.SetTrigger("hammerHit");
		}
		SetHammer(0);
		MakeSound("Hammer Hit");
	}

	bool lastSelectWasEmpty = false;

	public void SelectFlare(int i) {
		bool isSwitch = false;
		if (FlareShotPrefabs[i - 1] || i == 0) {
			// Switch between flare types while already viewing shells:
			isSwitch = (flareTypeSelected != (FlareShellScript.FlareTypes)i - 1) && !(lastSelectWasEmpty && previewingFlare);


			//previewingFlare = previewingFlare && isSwitch;

			isSwitch = isSwitch && previewingFlare;


			// Do not change if i == 0
			flareTypeSelected = i == 0 ? flareTypeSelected : (FlareShellScript.FlareTypes)i - 1;


			// Do the switchoff animation before bringing up a different bunch of flares
			if (isSwitch) {
				_handAnimator.SetTrigger("switchoff");
			} else /*if (ammo[flareTypeSelected] > 0) */{
				previewingFlare = !previewingFlare; // Not necessary with a 0-select animation!
			}

			lastSelectWasEmpty = false;



			string prefix = isSwitch ? "switch" : "select";
			//if (isSwitch || !previewingFlare) {

			switch (ammo[flareTypeSelected]) {
				case 0:
					// select 0
					//previewingFlare = false;
					//_handAnimator.SetTrigger("switchoff");
					_handAnimator.SetTrigger(prefix + "0");
					lastSelectWasEmpty = true;
					break;
				case 1:
					_handAnimator.SetTrigger(prefix + "1");
					break;
				case 2:
					_handAnimator.SetTrigger(prefix + "2");
					break;
				case 3:
					_handAnimator.SetTrigger(prefix + "3");
					break;
				default:
					_handAnimator.SetTrigger(prefix + "3");
					break;
			}
			//}
		}

		//Debug.Log("PF: " + previewingFlare);

		_flareGunAnimator.SetInteger("ammo_available", ammo[flareTypeSelected]);
		_handAnimator.SetInteger("ammo_available", ammo[flareTypeSelected]);
	}

	public void SetBarrelOpen(int val) {
		barrel_open = val == 1;
		_handAnimator.SetBool("barrel_open", barrel_open);

		// NEW
		previewingFlare = false;
	}

	public void SetPreviewingFlare(int i) {
		previewingFlare = i > 0;
	}

	public void SetSwitch(int i) {
		_handAnimator.SetTrigger("switch" + (i == 0 ? "off" : i.ToString()));
		previewingFlare = i > 0;
	}

	public void TriggerPulled(bool trigger) {
		//if (hammer_cocked) {
		_flareGunAnimator.SetBool("triggerPress", trigger);
		_handAnimator.SetBool("triggerPress", trigger);
		//}
	}

	public void SetChamberedRound(int val) {
		if (val == 1) {
			ChamberedRoundMesh.gameObject.SetActive(true);
		} else {
			ChamberedRoundMesh.gameObject.SetActive(false);
		}
	}

	public void CheckKeepChamberedRound() {
		if (Input.GetKey(KeyCode.V)) {
			//#####################
			//previewingFlare = !previewingFlare;
			previewingFlare = true;
			_handAnimator.SetTrigger("select1");
			SetLooseRound(0);
			LooseRoundMesh.material = fm.materials[(int)flareTypeChambered];
			LooseRoundMesh.gameObject.SetActive(true);
			isKeepingChamberedRound = true;
			LockInputs(1);
		} else {
			SetLooseRound(1);
			isKeepingChamberedRound = false;
		}
		///####################
	}


	public void SetLooseRound(int val) {

		// Prevent this if we're using the shell from the chamber
		if (!isKeepingChamberedRound) {
			//Update the materials if needed
			switch (val) {
				case 1:
					LooseRoundMesh.material = fm.materials[(int)flareTypeSelected];
					break;
				case 2:
					LooseRoundMeshDouble.material = fm.materials[(int)flareTypeSelected];
					break;
				case 3:
					LooseRoundMeshTriple.material = fm.materials[(int)flareTypeSelected];
					break;
			}

			LooseRoundMesh.gameObject.SetActive(val == 1);
			LooseRoundMeshDouble.gameObject.SetActive(val == 2);
			LooseRoundMeshTriple.gameObject.SetActive(val == 3);
		} else {
			// NEW
			LooseRoundMesh.gameObject.SetActive(val == 1);
			LooseRoundMeshDouble.gameObject.SetActive(false);
			LooseRoundMeshTriple.gameObject.SetActive(false);
		}
	}

	public void MakeSound(string str) {
		switch (str) {
			case "Insert":
				PlaySound(_insertClip);
				DSS.CreateDetectableMovingSound(0.2f, 0.2f);
				break;
			case "Remove":
				PlaySound(_removeClip);
				DSS.CreateDetectableMovingSound(0.2f, 0.2f);
				break;
			case "Open":
				PlaySound(_openClip);
				DSS.CreateDetectableMovingSound(0.2f, 0.2f);
				break;
			case "Close":
				PlaySound(_closeClip);
				DSS.CreateDetectableMovingSound(0.2f, 0.2f);
				break;
			case "Fire":
				DSS.CreateDetectableSound(transform.position + (Random.insideUnitSphere * 10.0f).WithY(0f), 3.0f, 0.1f);
				float vol;
				switch (flareTypeChambered) {
					case FlareShellScript.FlareTypes.RedSignal:
						vol = 0.3f;
						break;
					case FlareShellScript.FlareTypes.WhiteIllum:
						vol = 0.5f;
						break;
					case FlareShellScript.FlareTypes.GreenConc:
						vol = 0.35f;
						break;
					case FlareShellScript.FlareTypes.WhiteSupply:
						vol = 0.35f;
						break;
					default:
						vol = 1.0f;
						break;
				}
				_audioSource.PlayOneShot(_fireClips[(int)flareTypeChambered], vol);
				break;
			case "Cock":
				PlaySound(_cockClip);
				DSS.CreateDetectableMovingSound(0.1f, 0.1f);
				break;
			case "Hammer Hit":
				PlaySound(_hitClip);
				DSS.CreateDetectableMovingSound(0.1f, 0.2f);
				break;
			case "Flick":
				PlaySound(_flickClip);
				break;
			default:
				break;
		}
	}

	private void PlaySound(AudioClip clip) {
		_audioSource.PlayOneShot(clip);
	}
	private void PlaySound(AudioClip[] clips) {
		_audioSource.PlayOneShot(clips[0]);
		clips.Shuffle();
	}

	private void LockInputs(int bLock) {
		_inputsLocked = bLock == 1;
		//Debug.Log("Inputs Locked: " + _inputsLocked);
	}

	public void PouncedOn() {
		_inputsLocked = true;
		_handAnimator.SetBool("Pouncing", true);
		_beingPounced = true;
		SetMoveSpeed(0f, 1f);
	}

	private void SetThumb(int thumb) {
		thumb_busy = thumb == 1;
	}

	// For triggering the gun's animations
	public void CloseBarrelGun() {
		ResetFGTriggers();
		_flareGunAnimator.SetTrigger("closeBarrel");
	}
	public void OpenBarrelGun() {
		ResetFGTriggers();
		_flareGunAnimator.SetTrigger("openBarrel");
	}
	public void InsertRoundGun() {
		ResetFGTriggers();
		_flareGunAnimator.SetTrigger("insertRound");
	}
	public void RemoveRoundGun() {
		ResetFGTriggers();
		_flareGunAnimator.SetTrigger("removeRound");
	}
}
