﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C4AnimCallback : AnimCallback {

	[SerializeField]
	private GameObject C4Bone;

	[SerializeField]
	private GameObject C4Placeholder;

	[SerializeField]
	private GameObject C4Prefab;

	//[SerializeField]
	private FpsController player; // Needed to get player velocity

	[Range(0f, 1f)]
	[SerializeField]
	private float _playerVelocityInheritance = 0.15f; // Amount of player velocity to inherit (full is realistic but feels inconsistent when the player changes direction a lot)

	[SerializeField]
	private Vector2 randomSpinForceRange = new Vector2(-8.0f, -15.0f);

	[SerializeField]
	private AudioSource _audioSource;

	[SerializeField]
	private AudioClip[] _throwClips;

	[SerializeField]
	private AudioClip _detClip;

	[SerializeField]
	private AudioClip _buttonPressClip;

	[SerializeField]
	private AudioClip _buttonReleaseClip;

	[SerializeField]
	private Animator _handAnimators;

	[SerializeField]
	private Animator _C4Animator;


	public override void Jump() {
		_handAnimators.SetTrigger("Jump");
	}

	public override void SetMoveSpeed(float movespeed, float max) {
		_handAnimators.SetFloat("moveSpeed", movespeed);
	}

	public override void SetSliding(bool sliding) {
		_handAnimators.SetBool("isSliding", sliding);
	}

	public override void SetGrounded(bool grounded) {
		_handAnimators.SetBool("isGrounded", grounded);
	}

	public void Awake() {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<FpsController>();
	}

	public void Update() {
		if (Input.GetMouseButtonDown(0)) {
			_handAnimators.SetTrigger("Input_Throw");
		}
		if (Input.GetMouseButtonDown(1)) {
			_handAnimators.SetTrigger("Input_Det");
			_C4Animator.SetTrigger("Input_Det");
		}
	}


	public void SetC4(int val) {
		if (val == 1) {
			Transform C4Transform = C4Bone.GetComponent<Transform>().transform;
			GameObject C4Instance = (GameObject)(Instantiate(C4Prefab));
			C4Instance.GetComponent<Transform>().localScale = C4Transform.lossyScale;
			C4Instance.GetComponent<Transform>().position = C4Transform.position;
			C4Instance.GetComponent<Transform>().rotation = C4Transform.rotation;

			Vector3 vel = Vector3.forward * 10;
			vel = this.GetComponent<Transform>().rotation * vel;
			C4Instance.GetComponent<Rigidbody>().AddForce(vel + player.GetVelocity() * _playerVelocityInheritance, ForceMode.VelocityChange);

			Random.InitState(System.DateTime.Now.Millisecond);
			float torque = Random.Range(randomSpinForceRange.x, randomSpinForceRange.y);
			C4Instance.GetComponent<Rigidbody>().maxAngularVelocity = 20;
			C4Instance.GetComponent<Rigidbody>().AddTorque(C4Instance.GetComponent<Transform>().forward * torque, ForceMode.Impulse);

			C4Placeholder.SetActive(false);
		} else {
			C4Placeholder.SetActive(true);
		}
	}

	public void DetC4() {
		GameObject[] C4s = GameObject.FindGameObjectsWithTag("PlayerC4");
		foreach (GameObject C4 in C4s) {
			C4.GetComponent<C4Script>().Explode();
		}
	}

	public void MakeSound(string str) {
		switch (str) {
			case "Throw":
				PlaySound(_throwClips);
				break;
			case "Detonate":
				PlaySound(_detClip);
				break;
			case "ButtonDown":
				PlaySound(_buttonPressClip);
				break;
			case "ButtonUp":
				PlaySound(_buttonReleaseClip);
				break;
			default:
				break;
		}
	}

	private void PlaySound(AudioClip clip) {
		_audioSource.PlayOneShot(clip);
	}
	private void PlaySound(AudioClip[] clips) {
		_audioSource.PlayOneShot(clips[0]);
		clips.Shuffle();
	}
}
