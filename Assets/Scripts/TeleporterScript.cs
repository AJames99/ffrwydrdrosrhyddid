﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterScript : MonoBehaviour {

	[SerializeField]
	private Transform _teleExit;

	private void OnTriggerEnter(Collider other) {
		if (_teleExit) {
			if (other.gameObject.CompareTag("Player")) {
				other.transform.parent.position = _teleExit.position;
			} else if (other.attachedRigidbody) {
				other.attachedRigidbody.MovePosition(_teleExit.position);
			}
		}
	}
}
