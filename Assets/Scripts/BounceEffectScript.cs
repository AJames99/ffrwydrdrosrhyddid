﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceEffectScript : MonoBehaviour {
	private const float DistancePerStep = 3f;

	[SerializeField]
	private AudioSource _audioSource;

	private AudioSource _audioSourceScrape;

	private AudioSource _audioSourceVoice;

	[SerializeField]
	private AudioClip[] _stepClipsStone;

	[SerializeField]
	private AudioClip[] _stepClipsMud;

	[SerializeField]
	private AudioClip[] _stepClipsWood;

	[SerializeField]
	private AudioClip[] _jumpClipStone;

	[SerializeField]
	private AudioClip[] _jumpClipMud;

	[SerializeField]
	private AudioClip[] _jumpClipWood;

	[SerializeField]
	private AudioClip _jumpQuakeClip;

	[SerializeField]
	private AudioClip[] _bounceClips;

	[SerializeField]
	private float _bounceThreshold = 10f;

	/*[SerializeField]
	private AudioClip _landClip;*/

	[SerializeField]
	private AudioClip _landQuakeClip;

	[SerializeField]
	private AudioClip _slideClip;

	private List<AudioClip> _shuffledWalkClips;
	private List<AudioClip> _shuffledJumpClips;
	private List<AudioClip> _shuffledBounceClips;

	private Vector3 _prevPos;
	private float _distanceCovered;

	[SerializeField]
	private AudioClip[] skatesClips;


	//--------------------
	[SerializeField]
	private GameObject _bounceParticle = null;

	[SerializeField]
	private Transform leftFoot;
	[SerializeField]
	private Transform rightFoot;

	[SerializeField]
	private ParticleSystem _leftSmoke;

	[SerializeField]
	private ParticleSystem _rightSmoke;

	[SerializeField]
	private FpsController _player;

	[SerializeField]
	private bool _enableBounceParticle = true;


	private bool _shouldBounceParticle = false;

	public bool enableQuakeVoice = false;

	private DetectableSoundScript DSS;

	private float _speedY;

	private void Start() {
		_shouldBounceParticle = _bounceParticle && leftFoot && rightFoot && _enableBounceParticle;

		_shuffledWalkClips = new List<AudioClip>(_stepClipsMud);
		_shuffledJumpClips = new List<AudioClip>(_jumpClipMud);
		_shuffledBounceClips = new List<AudioClip>(_bounceClips);

		DSS = gameObject.AddComponent<DetectableSoundScript>();
	}

	private void Awake() {
		_audioSourceScrape = gameObject.AddComponent<AudioSource>();
		_audioSourceScrape.clip = _slideClip;
		_audioSourceScrape.loop = true;
		_audioSourceScrape.volume = 0.25f;

		_audioSourceVoice = gameObject.AddComponent<AudioSource>();
		_audioSourceVoice.clip = _jumpQuakeClip;
		_audioSourceVoice.volume = 0.25f;
	}

	public void ExternalUpdate(bool isGonnaJump, bool isGrounded, bool isLandedThisFrame, Vector3 vel, bool isSliding, Transform playerPos) {
		_speedY = vel.y;

		if (isSliding && !_audioSourceScrape.isPlaying) {
			// If we're already playing; don't restart
			_audioSourceScrape.Play();
		} else if (!isSliding && _audioSourceScrape.isPlaying) {
			_audioSourceScrape.Stop();
		}
		if (_audioSource.clip == _slideClip && _audioSource.isPlaying) {
			_audioSource.Stop();
		}
		/*if (_distanceCovered > DistancePerStep) {
			_distanceCovered = 0;
			_audioSource.PlayOneShot(_shuffledWalkClips[0]);
			_shuffledWalkClips.Shuffle();

			//DSS.CreateDetectableSound(transform.position, 0.3f, 0.1f);
			DSS.CreateDetectableMovingSound(0.3f, 0.1f, true);
		}*/

		if (isGonnaJump && isGrounded) {
			if (vel.magnitude > _bounceThreshold && _shouldBounceParticle) {
				AudioSource.PlayClipAtPoint(_shuffledBounceClips[0], GetComponent<Transform>().position, 0.3f);
				_shuffledBounceClips.Shuffle();
				Bounce(0);
				Bounce(1);

			} else {
				if (enableQuakeVoice) {
					_audioSourceVoice.PlayOneShot(_jumpQuakeClip);
				}
				_audioSource.PlayOneShot(_shuffledJumpClips[0]);
				_shuffledJumpClips.Shuffle();
			}

			//DSS.CreateDetectableSound(transform.position, 0.3f, 0.1f);
			DSS.CreateDetectableMovingSound(0.3f, 0.1f, true);
			_audioSource.PlayOneShot(_shuffledWalkClips[0]);
			_shuffledWalkClips.Shuffle();
		}

		/*if (isLandedThisFrame && !isGonnaJump) {
			_audioSource.PlayOneShot(_shuffledWalkClips[0]);
			_shuffledWalkClips.Shuffle();
			if (enableQuakeVoice) {
				_audioSourceVoice.PlayOneShot(_landQuakeClip);
			}
			//DSS.CreateDetectableSound(transform.position, 0.3f, 0.1f);
			DSS.CreateDetectableMovingSound(0.3f, 0.1f, true);
		}*/

		if (isLandedThisFrame && !isGonnaJump && _speedY < 0) {
			float volMultiplier = Mathf.Min(Mathf.Abs(_speedY) / 15f, 1.5f);
			//Debug.Log("YSPEED:" + _speedY);

			_audioSource.PlayOneShot(_shuffledWalkClips[0], volMultiplier);
			_shuffledWalkClips.Shuffle();
			if (enableQuakeVoice) {
				_audioSourceVoice.PlayOneShot(_landQuakeClip, volMultiplier);
			}
			//DSS.CreateDetectableSound(transform.position, 0.3f, 0.1f);
			DSS.CreateDetectableMovingSound(0.3f * volMultiplier, 0.1f, true);
		}

		/*if (isGrounded && !isSliding) {
			_distanceCovered += Vector3.Distance(_prevPos.WithY(0), playerPos.position.WithY(0));
		}*/

		_prevPos = playerPos.position;
	}


	public void Bounce(int left) {
		if (_shouldBounceParticle && _player.GetVelocity().magnitude > _bounceThreshold) {
			Instantiate(_bounceParticle, left == 1 ? leftFoot.position : rightFoot.position, left == 1 ? leftFoot.rotation : rightFoot.rotation);
			ResetSmoke(left == 1);
		}
	}

	public void SetBounceEnabled(bool val) {
		_enableBounceParticle = val;
		_shouldBounceParticle = _bounceParticle && leftFoot && rightFoot && _enableBounceParticle;
	}
	public bool GetBounceEnabled() {
		return _enableBounceParticle;
	}

	public void Footstep(int isCrouched) {
		//_audioSource.PlayOneShot(_shuffledWalkClips[0]);
		_audioSource.PlayOneShot(_shuffledWalkClips[Mathf.RoundToInt(Random.value * (_shuffledWalkClips.Capacity - 1))], isCrouched > 0 ? 0.2f : 0.8f);
		//_shuffledWalkClips.Shuffle();
		if (isCrouched <= 0) {
			DSS.CreateDetectableMovingSound(0.3f, 0.1f, true);
		}
	}

	// 0 or 1 depending on if it's an away or a step
	public void Skatestep(int up) {
		//_audioSource.PlayOneShot(skatesClips[Mathf.RoundToInt(Random.value * (skatesClips.Length - 1))], 1.0f);
		_audioSource.PlayOneShot(skatesClips[up], 0.35f);
	}

	public void ResetSmoke(bool left) {
		if (left) {
			_leftSmoke.gameObject.SetActive(true);
			_leftSmoke.time = 0;
		} else {
			_rightSmoke.gameObject.SetActive(true);
			_rightSmoke.time = 0;
		}
	}
}
