﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientateUpwards : MonoBehaviour {
	//private Vector3 contactNormal;
	private static Vector3 upDirection = Vector3.up;
	//public Quaternion rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
	[HideInInspector]
	public Quaternion rotation = new Quaternion(1.0f, 0.0f, 0.0f, 0.0f);

	// how many colliders are we collidin with? used to check grounding
	int c = 0;

	private const float _maxYSquash = 3.0f;
	private const float _minYSquash = 1.0f;
	[HideInInspector]
	public float yScale = _minYSquash;
	private const float _squashRate = 0.1f;
	private float _targetSquash = _minYSquash;
	private bool _onGround = false;


	private float diff;
	private void FixedUpdate() {
		if (_onGround) {
			_targetSquash = _maxYSquash;
		} else {
			_targetSquash = _minYSquash;
		}

		diff = _targetSquash - yScale;
		if (Mathf.Abs(diff) > 0.01f) {
			if (Mathf.Abs(diff) <= _squashRate) {
				yScale = _targetSquash;
			} else {
				if (Mathf.Sign(diff) > 0) {
					yScale += _squashRate;
				} else {
					yScale -= _squashRate;
				}
			}
		}
	}

	void OnCollisionEnter(Collision collision) {
		if (!collision.collider.CompareTag("Water")) {
			/*contactNormal = Vector3.zero;
			foreach (ContactPoint contact in collision.contacts)
			{
				//Debug.DrawRay(contact.point, contact.normal, Color.red, 5.0f);
				contactNormal += contact.normal;
			}
			contactNormal /= collision.contacts.Length;
			Debug.DrawRay(transform.position, contactNormal, Color.red, 5.0f);*/
			//  Debug.DrawRay(transform.position, (collision.contacts[0].normal).normalized, Color.red, 5.0f);

			rotation = Quaternion.FromToRotation(upDirection, collision.contacts[0].normal);
			c++;
			_onGround = c > 0;
		}
	}

	private void OnCollisionExit(Collision collision) {
		if (!collision.collider.CompareTag("Water")) {
			c--;
			_onGround = c > 0;
		}
	}
}
