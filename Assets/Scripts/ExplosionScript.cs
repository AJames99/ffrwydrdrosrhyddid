﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour {
	[SerializeField] //TODO: only show this if above is true
	private GameObject _particlePrefab = null;

	[SerializeField] //TODO: only show this if above is true
	private GameObject _particlePrefabMidair = null;

	[SerializeField]
	private LayerMask ignoreLayersMidairCheck;

	[SerializeField]
	private float maxLifespan = 0.5f;

	[SerializeField]
	//private float growthRate = 1.0f;
	private float maxRadius = 8.0f;

	[SerializeField]
	private float _pushForce = 10.0f;

	[SerializeField]
	private float _verticalBoostfactor = 1.0f;

	[SerializeField]
	private List<AudioClip> _explosionClips;

	[SerializeField]
	private float playerFactor = 0.02f; // 0.05

	[SerializeField]
	private LayerMask ignoreLayers;

	private float growthRate;

	private float minPushDistClamp = 0.25f; // If an affected object within this distance, clamp the push force to the maximum.


	private void Awake() {
		_explosionClips.Shuffle();

		AudioSource.PlayClipAtPoint(_explosionClips[0], GetComponent<Transform>().position);

		if (_particlePrefab) {
			//Random.InitState(System.DateTime.Now.Millisecond);
			Vector3 location = gameObject.GetComponent<Transform>().position;
			if (Physics.Raycast(location, Vector3.down, 5f, ~ignoreLayersMidairCheck) || !_particlePrefabMidair) {
				GameObject instance = Instantiate(_particlePrefab, location, Quaternion.identity);
				Destroy(instance, 5.0f);
			} else {
				GameObject instance = Instantiate(_particlePrefabMidair, location, Quaternion.identity);
				Destroy(instance, 5.0f);
			}

			//Instantiate(_particlePrefab, gameObject.GetComponent<Transform>().position, Quaternion.identity); //Quaternion.AngleAxis(Random.Range(0f, 360f), Vector3.up)


			// Do collision checks ONCE (at the start), using the resultant maximum 
			Vector3 centre = GetComponent<Transform>().position;
			Collider[] hitColliders = Physics.OverlapSphere(centre, maxRadius, ~ignoreLayers);
			foreach (Collider hit in hitColliders) {
				Rigidbody rb;
				if (hit.gameObject.TryGetComponent<Rigidbody>(out rb)) {
					Vector3 distanceVec = hit.ClosestPoint(centre) - centre;
					Vector3 force = distanceVec.normalized;
					force *= _pushForce * Mathf.Clamp(maxRadius - distanceVec.magnitude, minPushDistClamp, maxRadius);

					rb.AddForce(force, ForceMode.Force);
				} else {
					FpsController fp = hit.GetComponentInParent<FpsController>();
					if (fp) {
						Vector3 distanceVec = hit.ClosestPoint(centre) - centre;
						Vector3 force = distanceVec.normalized;
						force *= _pushForce * Mathf.Clamp(maxRadius - distanceVec.magnitude, minPushDistClamp, maxRadius);
						force.y *= _verticalBoostfactor;

						fp.AddForce(force * playerFactor, true);
					}
				}
			}

			Destroy(this.gameObject, maxLifespan);
		}

	}
}
