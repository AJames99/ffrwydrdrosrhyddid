﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeagullAnimCallback : MonoBehaviour {
	[SerializeField]
	private SeagullScript seagullScript;
	// Start is called before the first frame update
	public void TakeOff() {
		seagullScript.TakeOff();
	}
}
