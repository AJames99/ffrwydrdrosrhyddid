﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbableObject : MonoBehaviour {


	private bool bBeingCarried = false;
	private bool bHasPlayer = false;

	private Transform carrier;
	public Transform Carrier {
		get { return carrier; }
		set {
			carrier = value;
			bBeingCarried = carrier;
			_rb.drag = value ? ForcedDrag : rbDrag;

			// Fix our carry distance to the initial distance!
			if (carrier) {
				fCarryOffset = Mathf.Max((carrier.position - transform.position).magnitude, fMinCarrayOffset);
			}
		}
	}

	private FpsController fpsCont;
	public FpsController FpsCont {
		get { return fpsCont; }
		set {
			fpsCont = value;
			bHasPlayer = fpsCont;
		}
	}

	//[SerializeField]
	private float fCarryOffset = 2.0f;
	private const float fMinCarrayOffset = 3.0f;
	private float fSnapDistance = 2.5f;

	private Rigidbody _rb;

	private float rbDrag = 0.0f;

	private const float ForcedDrag = 15.0f;

	private const float force = 75.0f;

	public IGrabber grabber;

	private void Start() {
		_rb = GetComponent<Rigidbody>();
		rbDrag = _rb.drag;
	}

	private void Update() {
		if (bBeingCarried) {
			Vector3 targetPos = (carrier.position /* + (bHasPlayer ? fpsCont.GetVelocity() * Time.deltaTime : Vector3.zero)*/) + (carrier.forward * fCarryOffset);

			Vector3 delta = targetPos - transform.position;
			Debug.DrawRay(transform.position, delta, Color.blue, 0.2f);



			// Snap
			if (delta.magnitude > fSnapDistance) {
				Carrier = null;
				// Clamp the velocity so we don't accidentally fling it to the moon
				_rb.velocity = _rb.velocity.normalized * Mathf.Min(_rb.velocity.magnitude, 5.0f);
				grabber.Snap();
			}// else if (delta.magnitude > force * Time.deltaTime) {
			 //float force = Mathf.Min(delta.magnitude, 6.0f) * 3.0f;
			 //float force = Mathf.Max(1.0f, fSnapDistance - delta.magnitude) * 15.0f * ((Mathf.Abs(Mathf.Min(0f, Vector3.Dot(delta.normalized, _rb.velocity.normalized))) * 15.0f) + 1.0f);
			 //float force = Mathf.Max(1.0f, fSnapDistance - delta.magnitude) * 15.0f * ((Mathf.Abs(Mathf.Min(0f, Vector3.Dot(delta.normalized, _rb.velocity.normalized))) * 15.0f) + 1.0f);
			delta = delta.normalized * force * delta.sqrMagnitude;

			_rb.MovePosition(_rb.position + (fpsCont.GetVelocity() * Time.deltaTime));
			_rb.AddForce(delta, ForceMode.Acceleration);
			//_rb.AddForce(delta, ForceMode.Force);
		}
	}

	public void Throw(Vector3 dir, float force) {
		_rb.AddForce(dir * force, ForceMode.Impulse);
	}
}

public interface IGrabber {
	void Snap();
}