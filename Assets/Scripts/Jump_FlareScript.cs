﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump_FlareScript : MonoBehaviour {

	private const float _impactDamage = 10.0f;
	private const float _impactForce = 30.0f;
	private const float _explosionForce = 50.0f;

	/*[SerializeField]
	private AudioClip[] _explosionClips;

	private AudioSource _audioSource;*/

	[SerializeField]
	private ParticleSystem _smokeSystem;

	[SerializeField]
	private GameObject _explosionPrefab;

	private bool bExploded = false;

	private float fTimer = 0.0f;
	private const float fExpirationTime = 3.0f;

	private Light light;

	private Rigidbody rb;

	private DetectableSoundScript DSS;

	private void Start() {
		DSS = gameObject.AddComponent<DetectableSoundScript>();
		/*DSS.CreateDetectableMovingSound(10.0f, fExpirationTime);*/
	}

	// Start is called before the first frame update
	void Awake() {
		/*_audioSource = gameObject.AddComponent<AudioSource>();
		_audioSource.spatialBlend = 1.0f;
		_audioSource.spatialize = true;*/
		_smokeSystem.Play();
		light = GetComponentInChildren<Light>();
		rb = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update() {
		if (!bExploded) {
			if ((fTimer += Time.deltaTime) > fExpirationTime) {
				//Destroy(this.gameObject);
				Explode();
			}
		}
	}

	private void Explode() {
		// pew
		Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
		_smokeSystem.Stop();
		bExploded = true;
		Destroy(gameObject, fExpirationTime);
		rb.velocity = Vector3.zero;
		DSS.CreateDetectableSound(transform.position, 3.0f, 0.1f);
		Destroy(light.gameObject);
	}

	private void OnCollisionEnter(Collision collision) {
		if (!bExploded) {
			if (collision.impulse.magnitude > 5.0f) {
				Explode();
			}
		}
	}
}
