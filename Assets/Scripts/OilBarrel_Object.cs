﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilBarrel_Object : MonoBehaviour, UsableObject {

	[SerializeField]
	GameObject _oilPrefab;
	/*[SerializeField]
	GameObject _oilDropPrefab;*/
	[SerializeField]
	Transform _oilPoint;

	const float LAUNCHFORCE = 1.0f;

	MCBlob _oilInstance = null;
	public void Use(FpsController user) {
		Debug.Log("Oil barrel used!");
		if (!_oilInstance) {
			_oilInstance = Instantiate(_oilPrefab, _oilPoint.position, _oilPoint.rotation).GetComponent<MCBlob>();
			//_oilInstance.BlobObjectsLocations[0].GetComponent<Rigidbody>().AddForce(_oilPoint.forward * LAUNCHFORCE, ForceMode.Impulse);
		} else {
			/*GameObject oilDropInstance = Instantiate(_oilDropPrefab);
			oilDropInstance.transform.position = _oilPoint.position;
			List<SphereCollider> list = new List<SphereCollider>(_oilInstance.BlobObjectsLocations);
			list.Add(oilDropInstance.GetComponent<SphereCollider>());
			_oilInstance.BlobObjectsLocations = list.ToArray();
			oilDropInstance.transform.parent = _oilInstance.transform;
			*/
			_oilInstance.Freeze();

		}
	}
}
