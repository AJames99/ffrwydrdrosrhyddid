﻿using System.Collections;
using System.Collections.Generic;
//using UnityEngine;
public enum stackTypes {
	additive,
	multiplicative
}


public class ItemSocket {

}

public abstract class SocketStat {
}
public abstract class SocketStat<T, ReturnType> : SocketStat {

	// Basevalue may be modified post-init, so we need to update the resultant value to match.
	private T _baseValue;
	public T BaseValue {
		get {
			return this._baseValue;
		}
		set {
			this._baseValue = value;
			this.CalculateResultantValue();
		}
	}
	public ReturnType resultantvalue { get; set; }
	protected List<StatModifier<T>> _modifierStack = new List<StatModifier<T>>();

	protected stackTypes _stackType;

	public SocketStat(T baseValue, stackTypes stackType) {
		_baseValue = baseValue;
		_stackType = stackType;
	}

	public void AddModifier(StatModifier<T> modifier) {
		_modifierStack.Add(modifier);
		CalculateResultantValue();
	}

	public bool RemoveModifier(StatModifier<T> modifierToRemove) {
		foreach (StatModifier<T> modifier in _modifierStack) {
			if (modifier == modifierToRemove) {
				_modifierStack.Remove(modifier);
				CalculateResultantValue();
				return true;
			}
		}
		return false;
	}

	public abstract void CalculateResultantValue();
}

public class SocketStatFloat : SocketStat<float, float> {
	public SocketStatFloat(float baseValue, stackTypes stackType) : base(baseValue, stackType) {
		resultantvalue = baseValue;
	}
	public override void CalculateResultantValue() {
		resultantvalue = BaseValue;
		switch (_stackType) {
			case stackTypes.additive:
				foreach (StatModifier<float> modifier in _modifierStack) {
					resultantvalue += modifier.stat;
				}
				break;
			case stackTypes.multiplicative:
				foreach (StatModifier<float> modifier in _modifierStack) {
					resultantvalue *= modifier.stat;
				}
				break;
		}
	}
}

public class SocketStatInt : SocketStat<int, int> {
	public SocketStatInt(int baseValue, stackTypes stackType) : base(baseValue, stackType) {
		resultantvalue = baseValue;
	}
	public override void CalculateResultantValue() {
		resultantvalue = BaseValue;
		switch (_stackType) {
			case stackTypes.additive:
				foreach (StatModifier<int> modifier in _modifierStack) {
					resultantvalue += modifier.stat;
				}
				break;
			case stackTypes.multiplicative:
				foreach (StatModifier<int> modifier in _modifierStack) {
					resultantvalue *= modifier.stat;
				}
				break;
		}
	}
}

// 1 = true, 0 = false. GetValue returns calculated stat == 1. Allows stacking of +1 and -1 to allow e.g. Slide Boots +1 to CanSlide, HeavyArmour -1 to CanSlide, result is False
public class SocketStatBool : SocketStat<int, bool> {
	public SocketStatBool(int baseValue, stackTypes stackType) : base(baseValue, stackType) {
		resultantvalue = baseValue > 0;
	}
	public override void CalculateResultantValue() {

		int counter = BaseValue;
		resultantvalue = false;
		switch (_stackType) {
			case stackTypes.additive:
				foreach (StatModifier<int> modifier in _modifierStack) {
					counter += modifier.stat;
				}
				break;
			case stackTypes.multiplicative:
				foreach (StatModifier<int> modifier in _modifierStack) {
					counter *= modifier.stat;
				}
				break;
		}
		resultantvalue = counter > 0;
	}
}

public class StatModifier<T> {
	public T stat;
	public StatModifier(T stat) {
		this.stat = stat;
	}

	public static bool operator ==(StatModifier<T> B, StatModifier<T> A) {
		return EqualityComparer<T>.Default.Equals(B.stat, A.stat);
	}
	public static bool operator !=(StatModifier<T> B, StatModifier<T> A) {
		return !EqualityComparer<T>.Default.Equals(B.stat, A.stat);
	}
}