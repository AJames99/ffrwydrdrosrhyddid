﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedrunnersBoots : Boots {

	StatModifier<float> decreaseAirDecelCoeff = new StatModifier<float>(-15f);
	StatModifier<float> decreaseAirAccelCoeff = new StatModifier<float>(-2.5f);
	StatModifier<float> decreaseAirControlPrecision = new StatModifier<float>(-10f);
	//StatModifier<float> increaseGroundAccelCoeff = new StatModifier<float>(350f);
	StatModifier<float> increaseGroundFriction = new StatModifier<float>(3f); //+5
	StatModifier<float> increaseMaxSpeed = new StatModifier<float>(5f);
	StatModifier<int> enableSliding = new StatModifier<int>(1);

	public override void Equip(FpsController.PlayerSocket socket) {
		socket.AirDecelCoeff.AddModifier(decreaseAirDecelCoeff);
		socket.AirAccelCoeff.AddModifier(decreaseAirAccelCoeff);
		//socket.GroundAccelCoeff.AddModifier(increaseGroundAccelCoeff);
		socket.FrictionCoeff.AddModifier(increaseGroundFriction);
		socket.AirControlPrecision.AddModifier(decreaseAirControlPrecision);
		socket.MaxSpeed.AddModifier(increaseMaxSpeed);
		socket.CanSlide.AddModifier(enableSliding);
	}

	public override void Unequip(FpsController.PlayerSocket socket) {
		socket.AirDecelCoeff.RemoveModifier(decreaseAirDecelCoeff);
		socket.AirAccelCoeff.RemoveModifier(decreaseAirAccelCoeff);
		//socket.GroundAccelCoeff.RemoveModifier(increaseGroundAccelCoeff);
		socket.FrictionCoeff.RemoveModifier(increaseGroundFriction);
		socket.AirControlPrecision.RemoveModifier(decreaseAirControlPrecision);
		socket.MaxSpeed.RemoveModifier(increaseMaxSpeed);
		socket.CanSlide.RemoveModifier(enableSliding);
	}
}
