﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuakeBoots : Boots {

	StatModifier<int> disableCrouch = new StatModifier<int>(-1); //Stack a -1 onto the crouch enable/disable.
	StatModifier<int> disableForwardAirControlIncrease = new StatModifier<int>(-1);
	StatModifier<float> increaseAirDecelCoeff = new StatModifier<float>(6f);        // + 3 = 9 // was 6  TODO multiplicative
	StatModifier<float> increaseAirAccelCoeff = new StatModifier<float>(3f);
	//StatModifier<float> decreaseAirAccelCoeff = new StatModifier<float>(-0.5f); // was -2
	StatModifier<float> increasedAirControlPrecision = new StatModifier<float>(16f); // + 16 = 32 TODO multiplicative
	StatModifier<int> disableBounceParticles = new StatModifier<int>(-1);
	StatModifier<int> enableQuakeVoice = new StatModifier<int>(1);
	StatModifier<float> decreaseGroundAccelCoeff = new StatModifier<float>(-150f);
	//StatModifier<float> decreaseGroundFriction = new StatModifier<float>(-5f);
	StatModifier<float> decreaseMaxSpeed = new StatModifier<float>(-2f);
	StatModifier<int> allowQuakeRampJump = new StatModifier<int>(1);
	StatModifier<int> disableSliding = new StatModifier<int>(-1);

	public override void Equip(FpsController.PlayerSocket socket) {
		socket.CanCrouch.AddModifier(disableCrouch);
		socket.ForwardAirControlAddition.AddModifier(disableForwardAirControlIncrease);
		socket.AirDecelCoeff.AddModifier(increaseAirDecelCoeff);
		socket.AirAccelCoeff.AddModifier(increaseAirAccelCoeff);
		socket.GroundAccelCoeff.AddModifier(decreaseGroundAccelCoeff);
		//socket.FrictionCoeff.AddModifier(decreaseGroundFriction);
		socket.AirControlPrecision.AddModifier(increasedAirControlPrecision);
		socket.BounceParticle.AddModifier(disableBounceParticles);
		socket.QuakeVoice.AddModifier(enableQuakeVoice);
		socket.MaxSpeed.AddModifier(decreaseMaxSpeed);
		socket.CanQuakeRampBounce.AddModifier(allowQuakeRampJump);
		socket.CanSlide.AddModifier(disableSliding);
	}

	public override void Unequip(FpsController.PlayerSocket socket) {
		socket.CanCrouch.RemoveModifier(disableCrouch);
		socket.ForwardAirControlAddition.RemoveModifier(disableForwardAirControlIncrease);
		socket.AirDecelCoeff.RemoveModifier(increaseAirDecelCoeff);
		socket.AirAccelCoeff.RemoveModifier(increaseAirAccelCoeff);
		socket.GroundAccelCoeff.RemoveModifier(decreaseGroundAccelCoeff);
		//socket.FrictionCoeff.RemoveModifier(decreaseGroundFriction);
		socket.AirControlPrecision.RemoveModifier(increasedAirControlPrecision);
		socket.BounceParticle.RemoveModifier(disableBounceParticles);
		socket.QuakeVoice.RemoveModifier(enableQuakeVoice);
		socket.MaxSpeed.RemoveModifier(decreaseMaxSpeed);
		socket.CanQuakeRampBounce.RemoveModifier(allowQuakeRampJump);
		socket.CanSlide.RemoveModifier(disableSliding);
	}
}
