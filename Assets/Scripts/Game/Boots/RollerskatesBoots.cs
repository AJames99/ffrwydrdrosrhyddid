﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollerskatesBoots : Boots {

	public RuntimeAnimatorController skatesAnimatorController;


	StatModifier<int> disableForwardAirControlIncrease = new StatModifier<int>(-1);
	StatModifier<float> decreaseAirDecelCoeff = new StatModifier<float>(16f);
	StatModifier<float> decreaseAirAccelCoeff = new StatModifier<float>(-3f); //-2
																			  //StatModifier<float> decreasedAirControlPrecision = new StatModifier<float>(-15f);
																			  //StatModifier<int> disableBounceParticles = new StatModifier<int>(-1);
																			  //StatModifier<float> decreaseGroundAccelCoeff = new StatModifier<float>(-490f); // Was -475
	StatModifier<float> decreaseGroundAccelCoeff = new StatModifier<float>(-470f); // Was -490
																				   //StatModifier<float> decreaseGroundFriction = new StatModifier<float>(-4.45f);
																				   //StatModifier<float> decreaseGroundFriction = new StatModifier<float>(-4.45f); // was -4.45
	StatModifier<float> decreaseGroundFriction = new StatModifier<float>(-4.485f);

	StatModifier<float> increaseMaxSpeed = new StatModifier<float>(3f);

	StatModifier<int> disableSliding = new StatModifier<int>(-1);

	StatModifier<int> enableVanillaQuakeStyle = new StatModifier<int>(1);

	StatModifier<int> enableGroundedGravity = new StatModifier<int>(1); // To allow downhill rolling
																		//StatModifier<float> decreaseDecelerationMultiplier = new StatModifier<float>(-0.95f); // To reduce the effect of braking - applies only to inputs that are +90° from our current velocity
	StatModifier<float> decreaseDecelerationMultiplier = new StatModifier<float>(-0.5f); // To reduce the effect of braking - applies only to inputs that are +90° from our current velocity

	StatModifier<int> enableSkateAnims = new StatModifier<int>(1);

	StatModifier<float> increaseFrictionWhenSlow = new StatModifier<float>(128f);

	StatModifier<int> enableDoubleJump = new StatModifier<int>(1);

	[SerializeField]
	private AudioClip landClip;
	[SerializeField]
	private AudioClip jumpClip;
	[SerializeField]
	private AudioSource rollSource;

	private bool canLandSFX = true;

	private void Start() {
		rollSource.Play();
	}

	public override void Equip(FpsController.PlayerSocket socket) {
		socket.ForwardAirControlAddition.AddModifier(disableForwardAirControlIncrease);
		socket.AirDecelCoeff.AddModifier(decreaseAirDecelCoeff);
		socket.AirAccelCoeff.AddModifier(decreaseAirAccelCoeff);
		socket.GroundAccelCoeff.AddModifier(decreaseGroundAccelCoeff);
		socket.FrictionCoeff.AddModifier(decreaseGroundFriction);
		//socket.AirControlPrecision.AddModifier(decreasedAirControlPrecision);
		//socket.BounceParticle.AddModifier(disableBounceParticles);
		socket.MaxSpeed.AddModifier(increaseMaxSpeed);
		socket.CanSlide.AddModifier(disableSliding);
		socket.UseVanillaQuakeAccel.AddModifier(enableVanillaQuakeStyle);
		socket.DecelerationMultiplier.AddModifier(decreaseDecelerationMultiplier);
		socket.GravityOnGround.AddModifier(enableGroundedGravity);
		socket.UseSkateAnims.AddModifier(enableSkateAnims);
		socket.FrictionCoeffWhileSlow.AddModifier(increaseFrictionWhenSlow);
		socket.CanQuakeRampBounce.AddModifier(enableDoubleJump);
		/*rollSource.Play();
		accelSource.Play();*/
	}

	public override void Unequip(FpsController.PlayerSocket socket) {
		socket.ForwardAirControlAddition.RemoveModifier(disableForwardAirControlIncrease);
		socket.AirDecelCoeff.RemoveModifier(decreaseAirDecelCoeff);
		socket.AirAccelCoeff.RemoveModifier(decreaseAirAccelCoeff);
		socket.GroundAccelCoeff.RemoveModifier(decreaseGroundAccelCoeff);
		socket.FrictionCoeff.RemoveModifier(decreaseGroundFriction);
		//socket.AirControlPrecision.RemoveModifier(decreasedAirControlPrecision);
		//socket.BounceParticle.RemoveModifier(disableBounceParticles);
		socket.MaxSpeed.RemoveModifier(increaseMaxSpeed);
		socket.CanSlide.RemoveModifier(disableSliding);
		socket.UseVanillaQuakeAccel.RemoveModifier(enableVanillaQuakeStyle);
		socket.DecelerationMultiplier.RemoveModifier(decreaseDecelerationMultiplier);
		socket.GravityOnGround.RemoveModifier(enableGroundedGravity);
		socket.UseSkateAnims.RemoveModifier(enableSkateAnims);
		socket.FrictionCoeffWhileSlow.RemoveModifier(increaseFrictionWhenSlow);
		socket.CanQuakeRampBounce.RemoveModifier(enableDoubleJump);
		/*rollSource.Stop();
		accelSource.Stop();*/
	}

	public void SetSpeedAccel(float speed/*, bool isAccelerating*/, bool isGrounded, bool isGroundedPreviousFrame) {
		if (isGrounded) {
			rollSource.volume = speed;

		} else {
			rollSource.volume = 0.0f;
		}

		if (isGrounded && !isGroundedPreviousFrame && canLandSFX) {
			AudioSource.PlayClipAtPoint(landClip, transform.position, 0.4f);
			canLandSFX = false;
			Invoke("ResetLand", 0.3f);
		}
	}

	private void ResetLand() {
		canLandSFX = true;
	}
}
