﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Boots : MonoBehaviour, PlayerEquippableItem {

	//public GameObject bootsModel = null;

	public abstract void Equip(FpsController.PlayerSocket socket);

	public abstract void Unequip(FpsController.PlayerSocket socket);

}
