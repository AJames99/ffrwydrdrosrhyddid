﻿public interface IDamageable {
	void Damage(float dmg);
	void Heal(float hp);
	float GetHealth();
	void SetHealth(float health);
}
