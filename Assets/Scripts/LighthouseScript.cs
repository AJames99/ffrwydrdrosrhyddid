﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighthouseScript : MonoBehaviour {

	[SerializeField]
	private float _SpinRate = 10.0f; // Degrees per second

	[SerializeField]
	private Transform _SpotLight;

	// Update is called once per frame
	void Update() {
		if (_SpotLight) {
			_SpotLight.Rotate(new Vector3(0.0f, 1.0f, 0.0f), _SpinRate * Time.deltaTime);
		}
	}
}
