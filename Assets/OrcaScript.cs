﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OrcaScript : MonoBehaviour, IDamageable {

	private float hp = 100f;

	static private Transform _player;
	private NavMeshAgent _navMeshAgent;
	private Animator _orcaAnimator;

	/*[SerializeField]
	LayerMask playerLayer = 0;*/

	[SerializeField]
	private float _detectionReset = 12f; //12 down to 5 is detection
	[SerializeField]
	private float _suspicionReset = 5f; // 5 down to 0 is suspicious
	private const float _idleReset = 0f; // once 0 is hit, go to idle!
	private float _resetTimer = 0f;


	// Time after the last stimulus to go to sleep
	private const float minimumSleepTime = 15.0f;
	private const float maximumSleepTime = 35.0f;
	private float sleepTimeMax = minimumSleepTime;
	private float sleepTimer = 0.0f;

	// How long to wait before changing target position when Idle/Wandering
	[SerializeField]
	private float _idleChangePosTime = 5f;
	private float _wanderTimeMax;
	[SerializeField]
	private float _wanderRadius = 5.0f;
	private float _wanderTimer;

	private const uint IDLE = 0;
	private const uint SUSPICIOUS = 1;
	private const uint ALERT = 2;

	// Will go to sleep and only be woken by an alert level stimulus (2)
	private bool isAsleep = false;

	private uint _alertness;// = IDLE;

	private Transform _target = null;
	private Vector3 _targetPosition; // Used for situations where the spider should only know of the position at the point of detection, rather than constantly

	private Queue<int> _objectMemory = new Queue<int>(10);
	private const float _forgetTime = 25.0f;
	private float _memoryTimer = 0.0f;

	private bool movementLocked = false;

	[SerializeField]
	private float idleWanderSpeed = 5.0f;
	private const float idleWanderSlowMaxDistance = 3.0f; //at what distance-to-idle-target (i.e. wander point) do we begin to decrease speed?
	[SerializeField]
	private float suspiciousWanderSpeed = 7.0f;
	[SerializeField]
	private float detectionRunSpeed = 20.0f;

	private const float MaxAnimRunSpeed = 25.0f; // Any movement speed above this will not increase the animation rate


	private AudioSource audioSource;
	private AudioSource audioSourceCallBark;

	[Header("Orca Sounds")]
	[SerializeField]
	private AudioClip[] _attackClips;
	[SerializeField]
	private AudioClip[] _screamClips;
	[SerializeField]
	private AudioClip[] _painClips;
	[SerializeField]
	private AudioClip _dieClip;
	[SerializeField]
	private AudioClip[] _normalBreathingClips;
	[SerializeField]
	private AudioClip[] _angryAlertBreathingClips;
	[SerializeField]
	AudioClip[] _footstepSounds;

	[SerializeField]
	private AudioClip[] _callClips;
	[SerializeField]
	private AudioClip[] _alertClips;
	[SerializeField]
	private AudioClip[] _clickingClips;

	private NavMeshPath _path;

	private FireScript _fs;
	private bool _hasFireScript = false;

	private const float PainSoundTimeMax = 5.0f;
	private float _painSoundTime = PainSoundTimeMax;
	private float _paintSoundTimer = 0.0f;

	private AudioSource vocalisationSource;

	private bool bVoiceCoolingDown = false;


	//private const float rawSpeedSprintThresold = 8f;
	private const float rawSpeedSprintThreshold = 10f;

	private bool bDead = false;
	public void Damage(float dmg) {
		if ((hp -= dmg) <= 0) {
			bDead = true;
		}
	}

	public float GetHealth() {
		return hp;
	}

	public void Heal(float hp) {
		this.hp += hp;
	}

	public void SetHealth(float health) {
		hp = health;
	}

	private void Awake() {
		bDead = false;
		_navMeshAgent = GetComponent<NavMeshAgent>();
		_orcaAnimator = GetComponent<Animator>();
		ChangeAlertness(IDLE);
		_target = null;

		sleepTimeMax = Random.value.Remap(0f, 1f, minimumSleepTime, maximumSleepTime);

		if (!_player) {
			_player = null;
			//_player = GameObject.FindGameObjectWithTag("Player").transform;
			GameObject[] playertaggeds = GameObject.FindGameObjectsWithTag("Player");

			foreach (GameObject obj in playertaggeds) {
				if (obj.GetComponent<FpsController>()) {
					_player = obj.transform;
					continue;
				}
			}
		}

		Random.InitState(System.DateTime.Now.Millisecond);
		_wanderTimeMax = _idleChangePosTime;

		_path = new NavMeshPath();

		//_footstepAudioSource = GetComponents<AudioSource>()[0];
		//_otherAudioSource = GetComponents<AudioSource>()[1];

		//stingerSound = _player.GetComponentInChildren<StingerAudioManager>();

		_fs = GetComponent<FireScript>();
		_hasFireScript = _fs;

		if (!GetComponent<AudioSource>()) {
			audioSource = gameObject.AddComponent<AudioSource>();
			audioSource.spatialBlend = 1.0f;
			audioSource.spatialize = true;
			audioSource.volume = 0.5f;

			audioSourceCallBark = gameObject.AddComponent<AudioSource>();
			audioSourceCallBark.spatialBlend = 1.0f;
			audioSourceCallBark.spatialize = true;
			audioSourceCallBark.volume = 1.0f;
		}
	}

	private void GoToSleep() {
		Debug.Log("I'm tired... going to sleep...");
		isAsleep = true;
		sleepTimer = 0.0f;
		_orcaAnimator.SetBool("Asleep", true);
		sleepTimeMax = Random.value.Remap(0f, 1f, minimumSleepTime, maximumSleepTime);
		ChangeAlertness(IDLE);
		ModifySpeed(0f);
		_navMeshAgent.destination = transform.position;
	}

	private void WakeUp() {
		if (_orcaAnimator.GetBool("Asleep")) {
			Debug.Log("zzz.... HUH?!?!??!");
			_orcaAnimator.SetBool("Asleep", false);
		}
	}

	public void FinishedWaking() {
		isAsleep = false;

		// Update our speed etc.
		ForceAlertness(ALERT);
	}

	public void SendStimulus(LookerScript.detectionTypes detectionType, uint degree, Transform target = null) {

		if (isAsleep && degree > ALERT) {
			WakeUp();
			Debug.Log("Awoken by a level " + degree);
		}

		// Obviously don't change if we're dead, but also don't change if we're sleeping unless it's above ALERT level
		if (!bDead && !(isAsleep && degree <= ALERT)) {
			if (detectionType == LookerScript.detectionTypes.OBJECT) {
				// If the detection degree is greater than what we are at now, increase it to the input
				if (_alertness <= degree) {
					ChangeAlertness(degree);
					_target = target;

					if (Random.value > 0.4f && !bVoiceCoolingDown) {
						//PlaySound("Scream");
						//PlaySound("AlertCall");
						_orcaAnimator.SetTrigger("Call");
						bVoiceCoolingDown = true;
						Invoke("ResetVoice", 1.5f);
					}
				}

			} else if (detectionType == LookerScript.detectionTypes.PLAYER) {
				if (_alertness <= degree) {
					ChangeAlertness(degree);
					_target = _player;

					// Reset the forgetting of the player's active position
					CancelInvoke();
					Invoke("ForgetPlayer", 3.5f);

					if (Random.value > 0.2f) {
						//PlaySound("ScreamLoud");
						PlaySound("Angry");
					}
				}
			}


			// Sample the target's position once. (Used for suspicious/idle modes to avoid perfect tracking)
			// UPDATE: Using proper navmesh sampling to find the nearest point to our target, allowing for tracking of very high/distant objects.
			if (_target) {
				//_targetPosition = _target.position;

				// Sets Target Position (last known pos) to the nearest point on the navmesh to it
				GetNearestPointToTarget(_target.position, out _targetPosition);
				Debug.DrawLine(transform.position, _targetPosition, Color.red, 1f);
			}

			sleepTimer = 0.0f; // Reset sleepy time timer

			_navMeshAgent.destination = _targetPosition;
			Debug.DrawLine(transform.position, _targetPosition, Color.red, 1f);
		}
	}

	private void ForgetPlayer() {
		_targetPosition = _target.position;
		_target = null;
		Debug.Log("Forgot the player's active location...");
	}

	private bool GetNearestPointToTarget(Vector3 sourcePos, out Vector3 result) {
		NavMeshHit navHit;
		if (NavMesh.SamplePosition(sourcePos, out navHit, 50f, NavMesh.AllAreas)) {
			result = navHit.position;
			return true;
		} else {
			result = sourcePos;
			return false;
		}
	}

	private void ChangeAlertness(uint alertness) {
		ForceAlertness(alertness);

		switch (_alertness) {
			case ALERT:
				_resetTimer = Mathf.Max(_resetTimer, _detectionReset);
				//_navMeshAgent.speed = detectionRunSpeed;
				break;
			case SUSPICIOUS:
				_resetTimer = Mathf.Max(_resetTimer, _suspicionReset);
				//_navMeshAgent.speed = suspiciousWanderSpeed;
				break;
			default:
				_target = null;
				_resetTimer = Mathf.Max(_resetTimer, _idleReset);
				//_navMeshAgent.speed = idleWanderSpeed;
				break;
		}
	}

	private void ForceAlertness(uint alertness) {
		_alertness = (uint)Mathf.Min(alertness, ALERT);

		switch (_alertness) {
			case ALERT:
				ModifySpeed(detectionRunSpeed);
				break;
			case SUSPICIOUS:
				ModifySpeed(suspiciousWanderSpeed);
				break;
			default:
				_target = null;
				ModifySpeed(idleWanderSpeed);
				break;
		}
	}

	private void ModifySpeed(float speed) {
		if (!isAsleep || speed == 0f) {
			//Debug.Log("Speed modified to " + speed);
			_navMeshAgent.speed = speed;
		}
	}

	const float MaxBreathTime = 6.0f;
	float BreathInterval = MaxBreathTime;
	float breathTimer = 0.0f;

	private void Bark() {
		_orcaAnimator.SetTrigger("Bark");
	}

	private void Call() {
		_orcaAnimator.SetTrigger("Call");
	}

	void Update() {
		//Debug.DrawLine(transform.position, _navMeshAgent.destination, Color.red, 0.05f);

		if (!bDead && !movementLocked) {

			breathTimer += Time.deltaTime;
			if (breathTimer > BreathInterval) {
				PlaySound(_alertness > SUSPICIOUS ? "Clicking" : "Breathe");
				breathTimer = 0.0f;
				BreathInterval = Random.value.Remap(0.0f, 1.0f, MaxBreathTime / 2f, MaxBreathTime);

				if (!isAsleep) {
					if (Random.value > 0.2f) {
						if (Random.value > 0.5f) {
							Invoke("Call", Random.value);
						} else {
							Invoke("Bark", Random.value);
						}
					}
				}
			}

			if (_hasFireScript) {
				if (_fs._isBurning) {
					//Damage(12f * Time.deltaTime);
					Damage(10f * Time.deltaTime);
					_paintSoundTimer += Time.deltaTime;
					if (_paintSoundTimer > _painSoundTime) {
						PlaySound("Pain");
						_painSoundTime = PainSoundTimeMax + (Random.value * 2.0f);
						_paintSoundTimer = 0.0f;
					}
				}
			}

			if (!isAsleep) {
				if (_alertness > IDLE) {

					// Transition down the alertness states
					_resetTimer -= Time.deltaTime;
					//Debug.Log(_resetTimer);

					/*if (!_target && (_targetPosition - transform.position).magnitude < 1.0f) {
						_resetTimer = 0.0f;
					}*/

					// If we're alert and our timer goes below the suspicion-alert boundary, go to Suspicious
					if (_alertness >= ALERT) {
						if (_resetTimer < _suspicionReset) {
							Debug.Log("Alertness degraded to Suspicious");
							ForceAlertness(SUSPICIOUS);
							_target = null;
						} else {
							//Actively track our target if we're alert
							if (_target) {
								_navMeshAgent.destination = _target.position;
							} else {
								_navMeshAgent.destination = _targetPosition;
							}
						}
					}
					// If we're suspicious and our timer goes to 0 (idle reset), go to Idle
					else {
						if (_resetTimer < _idleReset) {
							Debug.Log("Alertness degraded to Idle");
							ForceAlertness(IDLE);
							_target = null;
						}

						if ((transform.position - _targetPosition).magnitude < 5.0f) {
							if ((_wanderTimer += Time.deltaTime) >= _wanderTimeMax) {
								Wander();
							}
						}

						float dist = (_navMeshAgent.destination - transform.position).magnitude;
						ModifySpeed(dist > idleWanderSlowMaxDistance ? suspiciousWanderSpeed : suspiciousWanderSpeed * (dist / suspiciousWanderSpeed));
					}
				} else {
					sleepTimer += Time.deltaTime;
					if (sleepTimer > sleepTimeMax) {
						GoToSleep();
					}

					if ((_wanderTimer += Time.deltaTime) >= _wanderTimeMax) {
						/*_targetPosition = RandomNavSphere(transform.position, _wanderRadius * ((Random.value + 0.5f) * 3f), NavMesh.AllAreas); //-1
						_navMeshAgent.destination = _targetPosition;
						_wanderTimer = 0f;
						//Debug.Log("Wandering somewhere new! " + newPos);
						_wanderTimeMax = _idleChangePosTime + ((Random.value - 0.5f) * 4f);

						CalculatePath(_targetPosition);*/
						Wander();
					}

					//Slow down as we approach the target (makes legs look less silly)
					//if (!_target) {
					float dist = (_navMeshAgent.destination - transform.position).magnitude;
					ModifySpeed(dist > idleWanderSlowMaxDistance ? idleWanderSpeed : idleWanderSpeed * (dist / idleWanderSlowMaxDistance));
					//}
				}

				/*if (_objectMemory.Count > 0) {
					if ((_memoryTimer += Time.deltaTime) >= _forgetTime) {
						if (_objectMemory.Count > 0) {
							Debug.Log("I forgot about " + _objectMemory.Dequeue() + "...");
						}
						_memoryTimer = 0.0f;
					}
				}*/

				/*_visionTimer += Time.deltaTime;
				if (_visionTimer >= _visionPoll) {
					_visionTimer = 0.0f;
					PollVision();
				}*/

				// Move toward target constantly if we're Alerted by it
				if (_target && _alertness >= ALERT) {
					_navMeshAgent.destination = _target.position;
					/*if ((_player.position - transform.position).magnitude <= _pounceDist && !_movementLocked && !_pouncing) {
						PounceOnPlayer();
					}*/
				}
			}

			_orcaAnimator.SetFloat("moveSpeed", isAsleep ? 0f : Mathf.Min(MaxAnimRunSpeed, _navMeshAgent.velocity.magnitude));
			_orcaAnimator.SetFloat("moveBlend", isAsleep ? 0f : Mathf.Min(2f, _navMeshAgent.velocity.magnitude / rawSpeedSprintThreshold));
		}
	}

	private void ResetVoice() {
		bVoiceCoolingDown = false;
	}

	private void Wander() {
		if ((_wanderTimer += Time.deltaTime) >= _wanderTimeMax) {
			_targetPosition = RandomNavSphere(transform.position, _wanderRadius * ((Random.value + 0.5f) * 3f), NavMesh.AllAreas); //-1
			_navMeshAgent.destination = _targetPosition;
			_wanderTimer = 0f;
			//Debug.Log("Wandering somewhere new! " + newPos);
			_wanderTimeMax = _idleChangePosTime + ((Random.value - 0.5f) * 4f);

			CalculatePath(_targetPosition);
		}

	}

	private void CalculatePath(Vector3 objective) {
		NavMesh.CalculatePath(transform.position, objective, NavMesh.AllAreas, _path);
		for (int i = 0; i < _path.corners.Length - 1; i++) {
			Debug.DrawLine(_path.corners[i], _path.corners[i + 1], Color.red, 5.0f);
		}
	}

	public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
		Vector3 randDirection = Random.insideUnitSphere * dist;
		randDirection += origin;

		NavMeshHit navHit;
		NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

		return navHit.position;
	}

	public void FootStep() {
		AudioSource.PlayClipAtPoint(_footstepSounds[Mathf.RoundToInt(Random.value * (_footstepSounds.Length - 1))], transform.position, Random.value.Remap(0f, 1f, 0.3f, 0.5f));
	}

	/*public void Maul() {
		movementLocked = true;
		_navMeshAgent.speed = 0f;
		_orcaAnimator.SetBool("Mauling", true);
		_player.GetComponent<FpsController>().Mauled(transform.position);
	}*/

	public void PlaySound(string snd) {
		switch (snd) {
			case "Scream":
				AudioSource.PlayClipAtPoint(_screamClips[Mathf.RoundToInt(Random.value * (_screamClips.Length - 1))], transform.position, 0.55f);
				break;
			case "Angry":
				audioSource.PlayOneShot(_angryAlertBreathingClips[Mathf.RoundToInt(Random.value * (_angryAlertBreathingClips.Length - 1))]);
				break;
			case "Breathe":
				audioSource.PlayOneShot(_normalBreathingClips[Mathf.RoundToInt(Random.value * (_normalBreathingClips.Length - 1))]);
				break;
			case "Call":
				audioSourceCallBark.PlayOneShot(_callClips[Mathf.RoundToInt(Random.value * (_callClips.Length - 1))], 2.0f);
				break;
			case "Bark":
				audioSourceCallBark.PlayOneShot(_alertClips[Mathf.RoundToInt(Random.value * (_alertClips.Length - 1))], 1.5f);
				// bark
				break;
			case "AlertCall":
				audioSourceCallBark.PlayOneShot(_alertClips[Mathf.RoundToInt(Random.value * (_alertClips.Length - 1))], 2.5f);
				break;
			case "Clicking":
				audioSourceCallBark.PlayOneShot(_clickingClips[Mathf.RoundToInt(Random.value * (_clickingClips.Length - 1))], 1.5f);
				break;
			default:
				Debug.LogError("Invalid sound name");
				break;
		}
	}
}