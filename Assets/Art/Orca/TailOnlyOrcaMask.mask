%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: TailOnlyOrcaMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Mesh
    m_Weight: 0
  - m_Path: OrcaArma
    m_Weight: 0
  - m_Path: OrcaArma/Root
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Neck_00
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Neck_00/DEF_Neck_01
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Neck_00/DEF_Neck_01/DEF_Head
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Neck_00/DEF_Neck_01/DEF_Head/DEF_Jaw
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Neck_00/DEF_Neck_01/DEF_Head/DEF_Jaw/DEF_Jaw_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Shoulder.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Shoulder.L/DEF_Arm.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Shoulder.L/DEF_Arm.L/DEF_Flipper.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Shoulder.L/DEF_Arm.L/DEF_Flipper.L/DEF_Flipper.L_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Shoulder.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Shoulder.R/DEF_Arm.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Shoulder.R/DEF_Arm.R/DEF_Flipper.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Spine1/DEF_Spine2/DEF_Spine3/DEF_Shoulder.R/DEF_Arm.R/DEF_Flipper.R/DEF_Flipper.R_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/Def_Tail
    m_Weight: 1
  - m_Path: OrcaArma/Root/DEF_Pelvis/Def_Tail/Def_Tail.001
    m_Weight: 1
  - m_Path: OrcaArma/Root/DEF_Pelvis/Def_Tail/Def_Tail.001/Def_Tail.002
    m_Weight: 1
  - m_Path: OrcaArma/Root/DEF_Pelvis/Def_Tail/Def_Tail.001/Def_Tail.002/Def_Tail.003
    m_Weight: 1
  - m_Path: OrcaArma/Root/DEF_Pelvis/Def_Tail/Def_Tail.001/Def_Tail.002/Def_Tail.003/Def_Tail.004
    m_Weight: 1
  - m_Path: OrcaArma/Root/DEF_Pelvis/Def_Tail/Def_Tail.001/Def_Tail.002/Def_Tail.003/Def_Tail.004/Def_Tail.005
    m_Weight: 1
  - m_Path: OrcaArma/Root/DEF_Pelvis/Def_Tail/Def_Tail.001/Def_Tail.002/Def_Tail.003/Def_Tail.004/Def_Tail.005/Def_Tail.005_end
    m_Weight: 1
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Thigh.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Thigh.L/DEF_Shin.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Thigh.L/DEF_Shin.L/DEF_Foot.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Thigh.L/DEF_Shin.L/DEF_Foot.L/DEF_Foot.L_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Thigh.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Thigh.R/DEF_Shin.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Thigh.R/DEF_Shin.R/DEF_Foot.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/DEF_Pelvis/DEF_Thigh.R/DEF_Shin.R/DEF_Foot.R/DEF_Foot.R_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L/DEF_Toe1.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L/DEF_Toe1.L/DEF_Toe1.001.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L/DEF_Toe1.L/DEF_Toe1.001.L/DEF_Toe1.001.L_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L/DEF_Toe2.001.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L/DEF_Toe2.001.L/DEF_Toe2.001.L_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L/DEF_Toe3.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L/DEF_Toe3.L/DEF_Toe3.001.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.L/DEF_Toe2.L/DEF_Toe3.L/DEF_Toe3.001.L/DEF_Toe3.001.L_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R/DEF_Toe1.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R/DEF_Toe1.R/DEF_Toe1.001.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R/DEF_Toe1.R/DEF_Toe1.001.R/DEF_Toe1.001.R_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R/DEF_Toe2.001.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R/DEF_Toe2.001.R/DEF_Toe2.001.R_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R/DEF_Toe3.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R/DEF_Toe3.R/DEF_Toe3.001.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_Foot.R/DEF_Toe2.R/DEF_Toe3.R/DEF_Toe3.001.R/DEF_Toe3.001.R_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_KneePole.L
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_KneePole.L/IK_KneePole.L_end
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_KneePole.R
    m_Weight: 0
  - m_Path: OrcaArma/Root/IK_KneePole.R/IK_KneePole.R_end
    m_Weight: 0
