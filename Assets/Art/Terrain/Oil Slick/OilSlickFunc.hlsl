//UNITY_SHADER_NO_UPGRADE
#ifndef MYHLSLINCLUDE_INCLUDED
#define MYHLSLINCLUDE_INCLUDED

void OilFunction_float(float2 uv, out float4 Out) {
	Out = float4(uv.xy, 1.5f, 0.0f);
	for (int i = 0; i < 234; i++) { //234
		Out.xzy = float3(1.3f, 1.0f, 0.78f) * abs(Out.xyz / dot(Out, Out) - float3(1.0f, 1.0f, 0.0f));
	}
}
#endif //MYHLSL_INCLUDED