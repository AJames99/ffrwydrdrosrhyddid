%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: FlareGunHammerMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: arm mesh flare
    m_Weight: 0
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Arm.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Index.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Index.L/Index2.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Index.L/Index2.L/Index3.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Index.L/Index2.L/Index3.L/Index3.L_end
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Middle.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Middle.L/Middle2.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Middle.L/Middle2.L/Middle3.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Middle.L/Middle2.L/Middle3.L/Middle3.L_end
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Pinky.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Pinky.L/Pinky2.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Pinky.L/Pinky2.L/Pinky3.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Pinky.L/Pinky2.L/Pinky3.L/Pinky3.L_end
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Ring.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Ring.L/Ring2.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Ring.L/Ring2.L/Ring3.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Ring.L/Ring2.L/Ring3.L/Ring3.L_end
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Thumb.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Thumb.L/Thumb2.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Thumb.L/Thumb2.L/Thumb3.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/Thumb.L/Thumb2.L/Thumb3.L/Thumb3.L_end
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/WeaponBone.L
    m_Weight: 0
  - m_Path: Armature/Arm.L/Hand.L/WeaponBone.L/WeaponBone.L_end
    m_Weight: 0
  - m_Path: Armature/Arm.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Index.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Index.R/Index2.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Index.R/Index2.R/Index3.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Index.R/Index2.R/Index3.R/Index3.R_end
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Middle.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Middle.R/Middle2.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Middle.R/Middle2.R/Middle3.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Middle.R/Middle2.R/Middle3.R/Middle3.R_end
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Pinky.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Pinky.R/Pinky2.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Pinky.R/Pinky2.R/Pinky3.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Pinky.R/Pinky2.R/Pinky3.R/Pinky3.R_end
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Ring.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Ring.R/Ring2.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Ring.R/Ring2.R/Ring3.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Ring.R/Ring2.R/Ring3.R/Ring3.R_end
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Thumb.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Thumb.R/Thumb2.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Thumb.R/Thumb2.R/Thumb3.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/Thumb.R/Thumb2.R/Thumb3.R/Thumb3.R_end
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/WeaponBone.R
    m_Weight: 0
  - m_Path: Armature/Arm.R/Hand.R/WeaponBone.R/WeaponBone.R_end
    m_Weight: 0
  - m_Path: Armature/Root
    m_Weight: 0
  - m_Path: Armature/Root/CameraBone
    m_Weight: 0
  - m_Path: Armature/Root/CameraBone/CameraBone_end
    m_Weight: 0
  - m_Path: Flare Gun
    m_Weight: 0
  - m_Path: FlareChambered
    m_Weight: 0
  - m_Path: FlareFree
    m_Weight: 0
  - m_Path: FlareGunArmature
    m_Weight: 0
  - m_Path: FlareGunArmature/FG_Grip
    m_Weight: 0
  - m_Path: FlareGunArmature/FG_Grip/FG_Barrel
    m_Weight: 0
  - m_Path: FlareGunArmature/FG_Grip/FG_Barrel/FG_RoundChambered
    m_Weight: 0
  - m_Path: FlareGunArmature/FG_Grip/FG_Barrel/FG_RoundChambered/FG_RoundChambered_end
    m_Weight: 0
  - m_Path: FlareGunArmature/FG_Grip/FG_Hammer
    m_Weight: 1
  - m_Path: FlareGunArmature/FG_Grip/FG_Hammer/FG_Hammer_end
    m_Weight: 1
  - m_Path: FlareGunArmature/FG_Grip/FG_Trigger
    m_Weight: 0
  - m_Path: FlareGunArmature/FG_Grip/FG_Trigger/FG_Trigger_end
    m_Weight: 0
  - m_Path: FleeFlareArmature
    m_Weight: 0
  - m_Path: FleeFlareArmature/FG_RoundFree
    m_Weight: 0
  - m_Path: FleeFlareArmature/FG_RoundFree/FG_RoundFree_end
    m_Weight: 0
