%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: c4_remote_mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Remote_Armature
    m_Weight: 1
  - m_Path: Remote_Armature/Root
    m_Weight: 1
  - m_Path: Remote_Armature/Root/Antenna1
    m_Weight: 0
  - m_Path: Remote_Armature/Root/Antenna1/Antenna2
    m_Weight: 1
  - m_Path: Remote_Armature/Root/Antenna1/Antenna2/Antenna3
    m_Weight: 1
  - m_Path: Remote_Armature/Root/Antenna1/Antenna2/Antenna3/Antenna4
    m_Weight: 1
  - m_Path: Remote_Armature/Root/Button
    m_Weight: 1
  - m_Path: Remote_Armature/Root/Clip
    m_Weight: 0
  - m_Path: Remote_Armature/Root/Lever
    m_Weight: 1
  - m_Path: Remote_Armature/Root/MasterButton
    m_Weight: 1
  - m_Path: Remote_Armature/Root/MasterButtonCap
    m_Weight: 1
  - m_Path: RemoteMesh
    m_Weight: 0
