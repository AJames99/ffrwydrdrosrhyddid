﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSphere {
	private SphereCollider _collider;

	private GameObject _obj;

	public SphereCollider collider {
		set {
			_collider = value;
		}
		get {
			return _collider;
		}
	}

	public GameObject obj {
		set {
			_obj = value;
		}
		get {
			return _obj;
		}
	}

	public Transform transform {
		get {
			return _obj.transform;
		}
	}

	private float _radius;
	public float radius {
		set {
			_radius = value;
			collider.radius = value;
		}
		get {
			return _radius;
		}
	}

	private Vector3 _position;
	public Vector3 position {
		set {
			transform.position = value;
			_position = value;
		}
		get {
			return _position;
		}
	}

	public SoundSphere() {
		obj = new GameObject("Sound Sphere");
		obj.SetActive(false);
		obj.transform.parent = null;
		obj.tag = "DetectableSound";
		obj.transform.position = Vector3.zero;

		collider = obj.AddComponent<SphereCollider>();
		collider.isTrigger = true;
	}

	public void Create(Vector3 position, float radius/*, float lifetime*/) {
		this.radius = radius;
		this.position = position;
		obj.SetActive(true);
	}

	public void Destroy() {
		obj.SetActive(false);
	}
}

public class DetectableSoundScript : MonoBehaviour {
	private static Queue<SoundSphere> soundSpheres = new Queue<SoundSphere>(0);
	private static List<SoundSphere> currentlyActiveSpheres = new List<SoundSphere>(0);

	private SphereCollider movingSoundCollider;

	private const int MaxSpheres = 25;

	// Used to scale up the volume input to a world scale radius
	private const float radiusMultiplier = 15.0f;


	// To stop too many spheres being created on collision objects, refuse to create one if it is too close to the last and too small in comparison to the last
	private Vector3 lastSoundPosition;
	private const float minimumDistanceForNewSphere = 3.0f;
	private float lastRadius;
	private const float minimumRadiusIncreaseForNewSphere = 0.25f;

	void Awake() {

		// Static
		if (soundSpheres.Count <= 0) {
			soundSpheres = new Queue<SoundSphere>(MaxSpheres);
			currentlyActiveSpheres = new List<SoundSphere>(MaxSpheres);

			for (int i = 0; i < MaxSpheres; i++) {
				soundSpheres.Enqueue(new SoundSphere());
			}
		}

		GameObject movingSoundObj = new GameObject("Moving Sound Sphere");
		movingSoundObj.SetActive(false);
		movingSoundObj.transform.parent = this.gameObject.transform;
		movingSoundObj.transform.localPosition = Vector3.zero;
		movingSoundObj.tag = "DetectableSound";
		movingSoundObj.transform.position = Vector3.zero;
		movingSoundCollider = movingSoundObj.AddComponent<SphereCollider>();
		movingSoundCollider.isTrigger = true;
	}

	public void CreateDetectableSound(Vector3 position, float radius, float lifetime) {
		float nearestDist = 99999.0f;

		if (currentlyActiveSpheres.Count > 0) {
			foreach (SoundSphere sphere in currentlyActiveSpheres) {
				nearestDist = ((sphere.position - lastSoundPosition).magnitude < nearestDist) ? (sphere.position - lastSoundPosition).magnitude : nearestDist;
			}
		}

		if ((nearestDist >= minimumDistanceForNewSphere || radius >= lastRadius + minimumRadiusIncreaseForNewSphere) || currentlyActiveSpheres.Count == 0) {

			SoundSphere active = soundSpheres.Dequeue();
			active.Create(position, radius * radiusMultiplier);
			currentlyActiveSpheres.Add(active);
			StartCoroutine(RemoveDetectableSound(active, lifetime));

			lastRadius = radius;
			lastSoundPosition = position;
		}
	}

	public void CreateDetectableMovingSound(float radius, float lifetime, bool isPlayer = false) {
		movingSoundCollider.radius = radius * radiusMultiplier;
		movingSoundCollider.gameObject.SetActive(true);
		movingSoundCollider.transform.localPosition = Vector3.zero;
		movingSoundCollider.tag = isPlayer ? "PlayerDetectableSound" : "DetectableSound";
		Invoke("RemoveMovingSound", lifetime);
	}

	private void RemoveMovingSound() {
		movingSoundCollider.gameObject.SetActive(false);
	}

	IEnumerator RemoveDetectableSound(SoundSphere sphere, float lifetime) {
		yield return new WaitForSeconds(lifetime);
		currentlyActiveSpheres.Remove(sphere);
		sphere.Destroy();
		soundSpheres.Enqueue(sphere);
	}
}
