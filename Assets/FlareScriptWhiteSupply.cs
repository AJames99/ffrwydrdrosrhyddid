﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareScriptWhiteSupply : FlareScript {

	[SerializeField]
	private GameObject parachuteCrate;
	[SerializeField]
	private GameObject parachuteCrateDouble;

	void Start() {
		base.Start();
		Invoke("CallInSupplyCrate", 4.0f);
	}

	private void CallInSupplyCrate() {
		if (Random.value < 0.9f) {
			Instantiate(parachuteCrate, transform.position + (Vector3.up * 50.0f), Quaternion.identity);
		} else {
			Instantiate(parachuteCrateDouble, transform.position + (Vector3.up * 50.0f), Quaternion.identity);
		}
	}
}
