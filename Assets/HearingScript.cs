﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HearingScript : MonoBehaviour {
	public OrcaScript guard;
	private SphereCollider sphereDetector;


	private float _pollRate = 1.0f;
	private float _pollCurrent = 0.0f;

	/// <summary>
	/// The minimum needed radius for the sound to be detected by this hearing script
	/// </summary>
	[SerializeField]
	private float radiusThreshold = 0.0f;

	// The higher the value, the more stimulating/resolving the detection will be
	[SerializeField]
	[Range(1, 3)]
	private uint detectionDegree = 1;

	[SerializeField]
	private bool canTransmitPlayersPosition = false;


	private void Awake() {
		guard = GetComponentInParent<OrcaScript>();
		sphereDetector = GetComponent<SphereCollider>();
	}

	private void Update() {
		if ((_pollCurrent += Time.deltaTime) > _pollRate) {
			sphereDetector.enabled = true;
			_pollCurrent = 0f;
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (canTransmitPlayersPosition) {
			if (other.tag == "PlayerDetectableSound") {
				SphereCollider sc = other.GetComponent<SphereCollider>();
				if (sc.radius >= radiusThreshold) {
					// If the radius of the sphere is many times more than the threshold of this listener, increase the detection degree (div by square to reduce)
					guard.SendStimulus(LookerScript.detectionTypes.PLAYER, detectionDegree + (uint)Mathf.FloorToInt(sc.radius / (radiusThreshold /** radiusThreshold*/ * ((detectionDegree * detectionDegree) / 2f))), other.transform);
					sphereDetector.enabled = false;
					Debug.Log("Player-locating sound detected: " + other.GetInstanceID() + " | of radius: " + sc.radius);
				}
			}
		} else if (other.tag == "DetectableSound" || other.tag == "PlayerDetectableSound") {
			SphereCollider sc = other.GetComponent<SphereCollider>();
			if (sc.radius >= radiusThreshold) {
				// If the radius of the sphere is many times more than the threshold of this listener, increase the detection degree (div by square to reduce)
				guard.SendStimulus(LookerScript.detectionTypes.OBJECT, detectionDegree + (uint)Mathf.FloorToInt(sc.radius / (radiusThreshold /** radiusThreshold*/ * ((detectionDegree * detectionDegree) / 2f))), other.transform);
				sphereDetector.enabled = false;
				Debug.Log("Sound detected: " + other.GetInstanceID() + " | of radius: " + sc.radius);
			}
		}
	}
}
