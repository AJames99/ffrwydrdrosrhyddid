﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareBoxScript : MonoBehaviour, UsableObject {


	Animator _boxAnimator;
	[SerializeField]
	private MeshRenderer _flaresMesh;

	private Rigidbody rb;

	private FlareShellScript.FlareTypes _type;

	public FlareShellScript.FlareTypes type {
		set {
			_type = value;
			RefreshMat();
		}
		get {
			return _type;
		}
	}

	[SerializeField]
	private bool useOverride = false;
	[SerializeField]
	private FlareShellScript.FlareTypes overrideType = FlareShellScript.FlareTypes.RedSignal;


	[SerializeField]
	private MatsList _fm;

	private bool bOpen = false;

	private bool bEmpty = false;

	private CollisionSFX colSFX;

	// Start is called before the first frame update
	void Start() {
		RefreshMat();
		_boxAnimator = GetComponent<Animator>();
		colSFX = GetComponent<CollisionSFX>();
		rb = GetComponent<Rigidbody>();
	}

	void Awake() {
		type = useOverride ? overrideType : type;
	}

	private void RefreshMat() {
		/*switch (_type) {
			case FlareShellScript.FlareTypes.RedSignal:
				_flaresMesh.material = _fm.materials[0];
				break;
			case FlareShellScript.FlareTypes.WhiteIllum:
				_flaresMesh.material = _fm.materials[1];
				break;
			case FlareShellScript.FlareTypes.GreenConc:
				_flaresMesh.material = _fm.materials[2];
				break;
		}*/
		_flaresMesh.material = _fm.materials[(int)_type];
	}
	// Update is called once per frame
	/*void Update() {

	}*/

	public void Use(FpsController user) {
		if (bOpen && !bEmpty) {
			user.GiveAmmo(type, 5);
			_flaresMesh.gameObject.SetActive(false);
			bEmpty = true;
			Debug.Log("Getting ammo outta box");
		} else if (Vector3.Dot(transform.up, Vector3.up) > 0.1f) {
			// If the box is upright...

			if (bOpen && bEmpty) {
				_boxAnimator.SetTrigger("Close");
				bOpen = false;
				Debug.Log("Closing box");
				colSFX.Break();
			} else {
				bOpen = true;
				_boxAnimator.SetTrigger("Open");
				Debug.Log("Opening box");
				colSFX.Break();
			}

			// If the box isn't upright, jostle it!
		} else {
			// Boink in a random direction
			colSFX.PlaySoft(transform.position, 0.4f);
			//rb.AddForceAtPosition(new Vector3(Random.value - 0.5f, 3.0f, Random.value - 0.5f), transform.position + new Vector3(Random.value - 0.5f, 0.0f, Random.value - 0.5f), ForceMode.Impulse);
			/*Quaternion q = Quaternion.FromToRotation(transform.up, Vector3.up);
			rb.AddTorque(q.eulerAngles / 0.5f, ForceMode.VelocityChange); // NEW?
			rb.AddForce(Vector3.up * 9.81f, ForceMode.VelocityChange);*/ // NEW
			Vector3 point = ((transform.position + transform.up).WithY(transform.position.y)).normalized;
			rb.AddForceAtPosition(Vector3.up * 3.0f, point + (Random.insideUnitSphere * 0.05f), ForceMode.VelocityChange);
			Debug.DrawRay(point, Vector3.up, Color.white, 5.0f);
		}
	}
}
