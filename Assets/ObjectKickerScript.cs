﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectKickerScript : MonoBehaviour {

	//public bool canKick = true;

	private FpsController _player;

	private OrcaScript orca;

	private void Awake() {
		orca = GetComponentInParent<OrcaScript>();
	}

	private void OnTriggerStay(Collider other) {
		if (other.attachedRigidbody/* && canKick*/) {
			other.attachedRigidbody.AddForce((other.transform.position - transform.position).normalized * 50.0f, ForceMode.Force);
			//canKick = false;
			//Invoke("ResetKick", 1.0f);
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			if (!_player) {
				_player = other.GetComponentInParent<FpsController>();
			}
			_player.AddForce((other.transform.position - transform.position).normalized * 10f + (Vector3.up * 12.0f));
			//orca.Maul();

			//Debug.Log("MAULED");
		}
	}
	/*private void ResetKick() {
		canKick = true;
	}*/

}
