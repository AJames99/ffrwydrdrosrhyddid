﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/FlareMats", order = 1)]
public class MatsList : ScriptableObject {
	//[SerializeField]
	public Material[] materials;
}

public class FlareShellScript : MonoBehaviour {

	private bool _spent;
	public bool spent {
		set {
			_spent = value;
			UpdateModel();
		}

		get {
			return _spent;
		}
	}

	[HideInInspector]
	public enum FlareTypes {
		RedSignal = 0,
		WhiteIllum = 1,
		GreenConc = 2,
		WhiteSupply = 3
	}

	[SerializeField]
	private Mesh _spentMesh;
	[SerializeField]
	private Mesh _unspentMesh;

	private FlareTypes _type;// = FlareTypes.RedSignal;
	public FlareTypes type {
		get {
			return _type;
		}

		set {
			_type = value;
			UpdateMaterial();
		}
	}
	[SerializeField]
	private MeshRenderer _mr;
	private MeshFilter _mf;

	//public static FlareMats fm;
	public MatsList fm;

	private void UpdateModel() {
		if (!_mf) { _mf = _mr.GetComponent<MeshFilter>(); }
		_mf.sharedMesh = spent ? _spentMesh : _unspentMesh;
	}

	private void UpdateMaterial() {
		/*switch (_type) {
			case FlareTypes.RedSignal:
				_mr.material = materials[0];
				_mr.material = fm.materials[0];
				break;
			case FlareTypes.WhiteIllum:
				_mr.material = materials[1];
				break;
			case FlareTypes.GreenConc:
				_mr.material = materials[2];
				break;
			default:
				_mr.material = materials[0];
				break;
		}*/
		_mr.material = fm.materials[(int)_type];
	}
}
