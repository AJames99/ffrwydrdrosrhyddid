﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReverbVolumeTrigger : MonoBehaviour {
	private AudioReverbZone zone;
	// Start is called before the first frame update
	void Start() {
		zone = GetComponent<AudioReverbZone>();
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player")) {
			zone.enabled = true;
			Debug.Log("Player entered reverb zone");
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.CompareTag("Player")) {
			zone.enabled = false;
			Debug.Log("Player exited reverb zone");
		}
	}
}
