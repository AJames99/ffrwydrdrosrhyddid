﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParachuteScript : MonoBehaviour {

	[SerializeField]
	private LayerMask layerMask;

	[SerializeField]
	private Rigidbody attachedTo;

	private FixedJoint joint;

	private Rigidbody rb;

	private Animator animator;

	private bool deployed = true;

	[SerializeField]
	private MeshRenderer lightMesh;
	[SerializeField]
	private Light light;
	[SerializeField]
	private Transform box;

	private Color colourYellow = new Color(1f, 1f, 0f);
	private Color colourBlue = new Color(0.4f, 0.4f, 1f);
	private bool yellow = true;

	[SerializeField]
	private FireScript box_fs;

	// Start is called before the first frame update
	void Awake() {
		joint = GetComponent<FixedJoint>();
		/*
		if (spring) {
			spring.connectedBody = attachedTo;
		}*/
		rb = GetComponent<Rigidbody>();
		animator = GetComponent<Animator>();
		animator.SetBool("deployed", deployed);
		Invoke("Delete", 35.0f);

		if (light && lightMesh) {
			Invoke("SwitchLights", 1.5f);

			// To be safe
			//lightMesh.material = new Material(lightMat);
		}
		//box_fs = box.GetComponent<FireScript>();
		if (box_fs) {
			Invoke("CheckFire", 2.0f);
		}

		// Red, Pink, Green only (0,1,2)
		box.GetComponentInChildren<CrateScript>().type = (FlareShellScript.FlareTypes)Random.Range(0, 3);
	}

	private void SwitchLights() {
		light.color = yellow ? colourBlue : colourYellow;
		lightMesh.material.SetColor("FlareColor", light.color);
		yellow = !yellow;

		Invoke("SwitchLights", 1.0f);
	}

	private void CheckFire() {
		if (box_fs._isBurning) {
			Delete();
		} else {
			Invoke("CheckFire", 2.0f);
		}
	}


	// Update is called once per frame
	void Update() {
		if (deployed) {
			rb.AddForce(-rb.velocity / (rb.mass * 5f), ForceMode.Force);

			Ray rayStevens = new Ray(box.transform.position + (Vector3.down), Vector3.down);

			//Debug.DrawRay(rayStevens.origin, rayStevens.direction * 4.0f, Color.cyan, 0.1f);

			if (Physics.Raycast(rayStevens, 4.0f, layerMask, QueryTriggerInteraction.Ignore)) {
				CancelInvoke("Delete");
				Delete();
			}

			//rb.AddForce(Vector3.up * 350.0f, ForceMode.Acceleration);
		} else {
			rb.AddForce(Vector3.up * 10.0f, ForceMode.Acceleration);
		}
	}

	/*private void Retract() {
		deployed = false;
		//animator.SetBool("deployed", false);
	}*/

	public void Delete() {
		//Destroy(gameObject);
		deployed = false;
		Destroy(joint);
		Invoke("DestroySelf", 5.0f);
	}

	private void DestroySelf() {
		box.parent = null;
		Destroy(gameObject);
	}
}
