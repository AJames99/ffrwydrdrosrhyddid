﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StingerAudioManager : MonoBehaviour {

	public static StingerAudioManager SAM;

	[SerializeField]
	private AudioSource _violinRapid;
	[SerializeField]
	private AudioSource _violinPlucking;
	[SerializeField]
	private AudioSource _violinJumpscare;
	[SerializeField]
	private float[] _rangesViolinRapid = new float[2] { 8f, 30f };
	[SerializeField]
	private float[] _rangesViolinPlucking = new float[2] { 30f, 60f };
	[SerializeField]
	private float _rangeViolinJumpscare = 3.5f;

	private bool pounced = false;

	// Hard max at 10, probably more efficient than lists
	private Transform[] _enemies = new Transform[10];

	private bool bEnemies = true;

	// Currently only applies to wolf spiders

	// Start is called before the first frame update
	void Start() {
		SpiderScript[] spiders = GameObject.FindObjectsOfType<SpiderScript>();
		for (int i = 0; i < spiders.Length; i++) {
			_enemies[i] = spiders[i].transform;
		}
		bEnemies = spiders.Length > 0;

		_violinRapid.volume = 0f;
		_violinPlucking.volume = 0f;
		_violinJumpscare.volume = 0f;
		_violinRapid.Stop();
		_violinPlucking.Stop();
		_violinJumpscare.Stop();
	}

	private void Awake() {
		if (SAM != null) {
			GameObject.Destroy(SAM);
		} else {
			SAM = this;
		}
		//		DontDestroyOnLoad(this)

	}

	// Call externally when we add a spider
	public void EnemyAdded(Transform enemy) {
		// If we have space...
		for (int i = 0; i < _enemies.Length; i++) {
			if (_enemies[i] == null) {
				_enemies[i] = enemy;
				Debug.Log("Successfully added enemy to stinger list.");
				bEnemies = true;
				return;
			}
		}
	}

	public void EnemyRemoved(Transform enemy) {
		for (int i = 0; i < _enemies.Length; i++) {
			if (_enemies[i]) {
				if (_enemies[i].Equals(enemy)) {
					_enemies[i] = null;
					Debug.Log("Successfully removed enemy from stinger list.");
				}
			}
		}

		bEnemies = false;
		for (int i = 0; i < _enemies.Length; i++) {
			if (_enemies[i]) {
				if (_enemies[i]) {
					bEnemies = true;
					continue;
				}
			}
		}
		if (!bEnemies) {
			SetVolume(0f, _violinPlucking);
			SetVolume(0f, _violinRapid);
		}
	}

	public void JumpScare() {
		if (!_violinJumpscare.isPlaying) {
			SetVolume(0.4f, _violinJumpscare);
		}
		pounced = true;
		SetVolume(0f, _violinPlucking);
		SetVolume(0f, _violinRapid);
	}

	// Update is called once per frame
	void Update() {
		if (bEnemies) {

			float smallestDist = 999f; // Just needs to be arbitrarily high
			for (int i = 0; i < _enemies.Length; i++) {
				if (_enemies[i]) {
					smallestDist = Mathf.Min((transform.position - _enemies[i].position).magnitude, smallestDist);
				}
			}

			if (!pounced) {
				/*if (smallestDist <= _rangeViolinJumpscare) {

					// Jumpscare should only work once (for now...)
					if (!_violinJumpscare.isPlaying) {
						SetVolume(0.4f, _violinJumpscare);
						//_violinJumpscare.volume = 0.35f;
						//_violinJumpscare.PlayDelayed(0.5f);
					}
					pounced = true;
					SetVolume(0f, _violinPlucking);
					SetVolume(0f, _violinRapid);

				} else {*/
				if (smallestDist < _rangesViolinRapid[1] && smallestDist > _rangesViolinRapid[0]) {
					float rapidVol = smallestDist.Remap(_rangesViolinRapid[0], _rangesViolinRapid[1], 1.0f, 0.0f);
					SetVolume(rapidVol, _violinRapid);
				} else {
					// If we're not in the normal range, set it to max or min volume depending.
					SetVolume(smallestDist < _rangesViolinRapid[0] ? 1.0f : 0.0f, _violinRapid);
				}

				if (smallestDist < _rangesViolinPlucking[1] && smallestDist > _rangesViolinPlucking[0]) {
					float pluckVol = smallestDist.Remap(_rangesViolinPlucking[0], _rangesViolinPlucking[1], 1.0f, 0.0f);
					SetVolume(pluckVol, _violinPlucking);
				} else {
					// If we're not in the normal range, set it to max or min volume depending.
					SetVolume(smallestDist < _rangesViolinPlucking[0] ? 1.0f : 0.0f, _violinPlucking);
				}
				//}
			}
		}
		//Debug.Log("Smallest Dist of " + _enemies.Length + " enemies: " + smallestDist);
	}

	private void SetVolume(float inputVol, AudioSource audioSource) {
		if (inputVol > 0f) {
			if (!audioSource.isPlaying) {
				audioSource.Play();
			}
			audioSource.volume = inputVol;
		} else {
			if (audioSource.isPlaying) {
				audioSource.Stop();
			}
			audioSource.volume = 0.0f;
		}
	}
}
