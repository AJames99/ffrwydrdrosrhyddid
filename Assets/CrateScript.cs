﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateScript : MonoBehaviour, UsableObject {

	// Default red flares?
	public FlareShellScript.FlareTypes type {
		set {
			_type = value;
			UpdateType();
		}
		get {
			return _type;
		}
	}

	[SerializeField]
	private FlareBoxScript[] containedBoxes;

	[SerializeField]
	private FlareShellScript.FlareTypes _type;// = FlareShellScript.FlareTypes.RedSignal;

	public MatsList abm;

	[SerializeField]
	private AudioClip[] _crateOpenSounds;


	//private Rigidbody rb;

	[SerializeField]
	private GameObject lid;
	private Rigidbody lidRb;
	private FixedJoint fixedJoint;

	// Start is called before the first frame update
	void Start() {
		lidRb = lid.GetComponent<Rigidbody>();
		fixedJoint = lid.GetComponent<FixedJoint>();
		Random.InitState(System.DateTime.Now.Millisecond);
	}

	void Awake() {
		UpdateType();
	}

	void UpdateType() {
		MeshRenderer mr = lid.GetComponent<MeshRenderer>();
		Material[] list = mr.materials;
		list[1] = abm.materials[(int)type];
		mr.materials = list;

		foreach (FlareBoxScript fbs in containedBoxes) {
			if (fbs) {
				fbs.type = type;
			}
		}

		//Debug.Log("Updated type: " + type + "; " + lid.GetComponent<MeshRenderer>().materials[1].name + ", " + abm.materials[(int)type].name);
	}


	public void Use(FpsController user) {
		// pop
		if (fixedJoint) {
			Destroy(fixedJoint);
			//lidRb.AddForce((lid.transform.forward + (new Vector3(Mathf.Ceil(Random.value - 0.5f), Mathf.Ceil(Random.value - 0.5f), Mathf.Ceil(Random.value - 0.5f)) * 0.2f)) * 4.0f, ForceMode.VelocityChange);
			lidRb.AddForce((lid.transform.rotation * (new Vector3(Mathf.Ceil(Random.value - 0.5f), Mathf.Ceil(Random.value - 0.5f) * 0.1f, Mathf.Ceil(Random.value - 0.5f)) * 0.5f)) * 4.0f, ForceMode.VelocityChange);
			lidRb.AddTorque(new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f) * 5.0f, ForceMode.Impulse);
			AudioSource.PlayClipAtPoint(_crateOpenSounds[Mathf.RoundToInt((Random.value + 0.001f) * (_crateOpenSounds.Length - 1))], lidRb.position, 0.4f);
		}
	}
}
